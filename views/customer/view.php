<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $user app]models\User */

$this->title = $model->id;
$curUser = User::findOne( Yii::$app->user->id );
if ( $curUser->isAdmin() )
{
	$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Customers' ), 'url' => [ 'index' ] ];
}
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="customer-view">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>
		<?= Html::a( Yii::t( 'app', 'Update' ), [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] ) ?>
		<?php
		if ( $model->id != Yii::$app->user->id )
		{
			echo Html::a( Yii::t( 'app', 'Delete' ), [ 'delete', 'id' => $model->id ], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
					'method' => 'post',
				],
			] );
		}
		?>

	</p>

	<?= DetailView::widget( [
		'model' => $model,
		'attributes' => [
			'id',
			[ 'attribute' => 'name', 'value' => $model->user->name ],
			[ 'label' => 'Emails', 'format'  => 'raw', 'value' => $model->user->getEmailsHtml( Yii::$app->request->url ) ],
			[ 'attribute' => 'username', 'value' => $model->user->username ],
			[ 'label' => 'Phones', 'format' => 'raw', 'value' => $model->user->getPhonesHtml( Yii::$app->request->url ) ],
			[ 'label' => 'Video Chats', 'format' => 'raw', 'value' => $model->user->getVideoChatsHtml( Yii::$app->request->url ) ],
			[ 'label' => 'Addresses', 'format' => 'raw', 'value' => $model->user->getAddressesHtml( Yii::$app->request->url ) ],
			[ 'label' => 'Timezone', 'value' => $model->user->timezone ],
			[ 'label' => 'Notes', 'value' => $model->user->notes ],
			[ 'label' => 'Private Notes', 'visible' => $curUser->isAdmin() || $curUser->isTutor(), 'value' => $model->user->notesPrivate ],
			[ 'label' => 'status', 'value' => $model->user->statusString() ],
			'howFound',
			'howFoundOther',
			'emailConfirmed:email',
		],
	] ) ?>

</div>
