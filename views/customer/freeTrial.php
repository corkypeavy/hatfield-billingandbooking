<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\FreeTrialForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Free Trial';
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="site-free">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p><?= Html::a( "I already have an Account", Url::to( [ 'site/login' ] ), [ "class" => "btn btn-lg btn-success" ] ) ?></p>
	<p><?= Html::a( "Make a New Account", Url::to( [ 'site/signup' ] ), [ "class" => "btn btn-lg btn-success" ] ) ?></p>
</div>
