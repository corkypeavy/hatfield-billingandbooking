<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Customers' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="customer-index">

	<h1><?= Html::encode( $this->title ) ?></h1>
	<?= $this->render( '_search', [ 'model' => $searchModel ] ); ?>

	<p>
		<?= Html::a( Yii::t( 'app', 'Create Customer' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
	</p>
	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
//			'filterModel' => $searchModel,
			'columns' => [
				[ 'class' => 'yii\grid\SerialColumn' ],
				'user.name',
				'user.username',
				[ 'label' => 'Emails', 'format' => 'raw', 'value' => function ( $model ) { return $model->user->getEmailsHtml( Yii::$app->request->url ); } ],
				[ 'label' => 'Phones', 'format' => 'raw', 'value' => function ( $model ) { return $model->user->getPhonesHtml( Yii::$app->request->url ); } ],
				[ 'attribute' => 'status', 'value' => function ( $model ) { return $model->user->statusString(); } ],
				[ 'class' => 'yii\grid\ActionColumn' ],
			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
