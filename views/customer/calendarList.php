<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\models\Schedule;
use app\models\helpers\TimeType;
use app\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Calendar List' );
$this->params[ 'breadcrumbs' ][] = $this->title;

<div class="customer-schedule">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= yii2fullcalendar\yii2fullcalendar::widget( [
		'ajaxEvents' => Url::to( [ 'scheduled-list', 'custId' => $cust->id ] ),
		'clientOptions' => [
			'selectable' => false,
			'selectHelper' => false,
			'droppable' => false,
			'editable' => false,
			'eventStartEditable' => false,
			'eventDurationEditable' => false,
			'defaultDate' => date( 'Y-m-d' ),
			'defaultView' => 'listMonth',
			'availableViews' => [ 'listMonth' ],
			'allDaySlot' => false,
			'slotDuration' => '00:15:00',
			'snapDuration' => $type == TimeType::FREE ? '00:30:00' : '01:00:00',
			'eventColor' => 'yellow',
			'eventTextColor' => 'black',
			'selectOverlap' => false,
			'eventOverlap' => false,
			'selectConstraint' => 'businessHours',
			'eventConstraint' => 'businessHours',
/*			'header' => [
				'left' => 'title',
				'center' => 'agendaWeek',
				'right' => $type == TimeType::RECURRING ? '' : 'prev, next'
			], */
			'businessHours' => $tutor->getAvailableTimes( $cust->user->timezone ),
			'selectConstraint' => 'businessHours',
			'timezone' => $cust->user->timezone,
		]
	] ) ?>
</div>
