<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t( 'app', 'Purchase History' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="purchase-history">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'columns' => [
				[ 'attribute' => 'tutorId', 'label' => 'Tutor', 'value' => $model->tutor->user->name ],
				'createdDate',
				[ 'attribute' => 'type', 'value' => function ( $model ) { return $model->typeString(); } ],
				'price',
				'id',
				[ 'attribute' => 'status', 'format' => 'raw', 'value' => function ( $model ) { return $model->statusHtml(); } ],
			],
		] )  ?>
	<?php Pjax::end(); ?>
</div>
