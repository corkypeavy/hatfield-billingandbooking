<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $customer app\models\Customer */
/* @var $user app\models\User */
/* @var $phone app\models\Phone */
/* @var $address app\models\Address */
/* @var $videoChat app\models\VideoChat */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\Timezone;
use app\models\Address;
use app\models\Phone;
use app\models\VideoChat;
use app\models\Referrer;

$this->title = 'Signup';
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="site-signup">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>Please fill out the following fields to signup:</p>

	<?php $form = ActiveForm::begin( [ 'id' => 'form-signup' ] ); ?>

		<?= $form->field( $user, 'name' )->textInput( [ 'autofocus' => true ] ) ?>

		<?= $form->field( $user, 'username' )->textInput() ?>

		<?= $form->field( $user, 'password' )->passwordInput() ?>

		<?= $form->field( $user, 'email' ) ?>

		<?= $form->field( $user, 'timezone' )->dropDownList( Timezone::$timezones ) ?>

		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $phone, 'type' )->dropDownList( Phone::$types ) ?>
			</div>
			<div class="col-lg-2">
				<?= $form->field( $phone, 'countryCode' )->textInput( [ 'maxlength' => true ] ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $phone, 'number' )->textInput() ?>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $address, 'type' )->dropDownList( Address::$types ) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $address, 'street' )->textInput( [ 'maxlength' => true ] ) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3">
				<?= $form->field( $address, 'city' )->textInput() ?>
			</div>
			<div class="col-lg-3">
				<?= $form->field( $address, 'state' )->textInput() ?>
			</div>
			<div class="col-lg-3">
				<?= $form->field( $address, 'postalCode' )->textInput() ?>
			</div>
			<div class="col-lg-3">
				<?= $form->field( $address, 'country' )->textInput() ?>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $videoChat, 'type' )->dropDownList( VideoChat::$types ) ?>
			</div>
			<div class="col-lg-2">
				<?= $form->field( $videoChat, 'accountId' )->textInput() ?>
			</div>
		</div>

		<?= $form->field( $customer, 'howFound' )->dropDownList( ArrayHelper::map( Referrer::find()->where( [ 'canShow' => 1 ] )->all(), 'id', 'name' ), [ 'prompt' => '-- Select One -- ' ] ) ?>

		<?= $form->field( $customer, 'howFoundOther' )->textInput( [ 'maxlength' => true ] ) ?>

		<div class="form-group">
			<?= Html::submitButton( 'Signup', [ 'class' => 'btn btn-primary', 'name' => 'signup-button' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>
</div>
