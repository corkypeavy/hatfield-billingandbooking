<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TutorSearch */
/* @var $userSearch app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Tutors' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="tutor-index">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
//			'filterModel' => $searchModel,
			'columns' => [
				'user.name',
				'user.email',
				'bio',
				[ 'attribute' => 'photo', 'format' => 'raw', 'value' => function ( $model ) {
					if ( !empty ( $model->photo ) )
					{
						return '<img src="data:image;base64,' . base64_encode( $model->photo ).'" height="50" />'; 
					}
					else
					{
						return '';
					}
				} ],
				'user.timezone',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{view} {select}',
					'buttons' => [
						'view' => function ( $url, $model, $key ) {
							return Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', '#', [
								'value' => Url::to( [ 'view-tutor', 'id' => $key ] ),
								'title' => 'View Tutor',
								'class' => 'showModalButton' ] );
						},
						'select' => function ( $url, $model, $key ) {
							return Html::a( 'Select', [ 'schedule', 'tutorId' => $key ], [ 'title' => Yii::t( 'app', 'Select' ) ] );
						},
					],
				],

			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
