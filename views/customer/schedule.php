<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\models\Schedule;
use app\models\helpers\TimeType;
use app\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Schedule' );
$this->params[ 'breadcrumbs' ][] = $this->title;

$DragJS = <<<EOF
/* initialize the external events
-----------------------------------------------------------------*/
$( '#external-events .fc-event' ).each( function() {
	// store data so the calendar knows to render an event upon drop
/*	$( this ).data( 'event', {
		title: $.trim( $( this ).text() ), // use the element's text as the event title
		stick: true, // maintain when user navigates (see docs on the renderEvent method)
		duration: '00:45'
	} ); */
	// make the event draggable using jQuery UI
	$( this ).draggable( {
		zIndex: 999,
		revert: true,      // will cause the event to go back to its
		revertDuration: 0  //  original position after the drag
	} );
} );
EOF;
$this->registerJs( $DragJS );
?>
<div class="customer-schedule">

<?php
$JSSelectRecurring = <<<EOF
function ( start, end )
{
/*
	var eventData;
	if ( title )
	{
		eventData = {
			title: 'Recurring',
			start: start,
			end: end
		};

		$( '#w0' ).fullCalendar( 'renderEvent', eventData, true );
	}
*/
	$( '#w0' ).fullCalendar( 'unselect' );
}
EOF;

$JSSelectFree = <<<EOF
function ( start, end )
{
	var duration = moment.duration( end - start )

	if ( duration.asMinutes() > 30 )
	{
		alert( 'The limit for a free trial is 1/2 hour.' );
		end = moment( start );
		end.add( 1, 'h' );
	}

	var eventData;
	var available = Number( $( '#available' ).val() );
	var nearestHoursForChange = Number( $( '#nearestHoursForChange' ).val() );
	var futureBookingLimit = Number( $( '#futureBookingLimit' ).val() );
	var offset = Number( $( '#custTimeZoneOffset' ).val() );
	var calendar = $( '#w0' ).fullCalendar( 'getCalendar' );
	var now = moment.utc().utcOffset( offset );
	var now2 = moment( now ); // clones the moment

	if ( start < now.add( nearestHoursForChange, 'h' ) )
	{
		alert( 'Must schedule at least ' + nearestHoursForChange + ' hours ahead of time.' );
	}

	else if ( start > now.add( futureBookingLimit, 'd' ) )
	{
		alert( 'Must schedule within ' + futureBookingLimit + ' days.' );
	}

	else if ( available == 0 )
	{
		alert( 'You are out of availble time.' );
	}

	else
	{
		eventData = {
			title: 'Free Time',
			start: start,
			end: end,
			durationEditable: false
		};

		available--;
		$( '#available' ).val( available );
		$( '#w0' ).fullCalendar( 'renderEvent', eventData, true );
	}

	$( '#w0' ).fullCalendar( 'unselect' );
}
EOF;

$JSSelectFlex = <<<EOF
function ( start, end )
{
	var duration = moment.duration( end - start )

	if ( duration.asHours() > 1 || ( duration.hours() == 1 && duration.minutes() > 0 ) )
	{
		alert( 'The limit is 1 hour.  If you would like, you can schedule a new time immediately following the current one.' );
		end = moment( start );
		end.add( 1, 'h' );
	}

	var eventData;
	var available = Number( $( '#available' ).val() );
	var nearestHoursForChange = Number( $( '#nearestHoursForChange' ).val() );
	var futureBookingLimit = Number( $( '#futureBookingLimit' ).val() );
	var offset = Number( $( '#custTimeZoneOffset' ).val() );
	var calendar = $( '#w0' ).fullCalendar( 'getCalendar' );
	var now = moment.utc().utcOffset( offset );
	var now2 = moment( now ); // clones the moment

	if ( start < now.add( nearestHoursForChange, 'h' ) )
	{
		alert( 'Must schedule at least ' + nearestHoursForChange + ' hours ahead of time.' );
	}

	else if ( start > now.add( futureBookingLimit, 'd' ) )
	{
		alert( 'Must schedule within ' + futureBookingLimit + ' days.' );
	}

	else if ( available == 0 )
	{
		alert( 'You are out of availble time.' );
	}

	else
	{
		eventData = {
			title: 'Flex Time',
			start: start,
			end: end,
			durationEditable: false
		};

		available--;
		$( '#available' ).val( available );
		$( '#w0' ).fullCalendar( 'renderEvent', eventData, true );
	}

	$( '#w0' ).fullCalendar( 'unselect' );
}
EOF;

$JSDropEvent = <<<EOF
function ( date )
{
//alert( "Dropped on " + date.format() );
	var data = $( this ).attr( 'data-event' );
	var start = data.indexOf( 'subId' )+ 7; // add 7 to get past 'subId":'
	var end = data.indexOf( '}' ); // subId must be the last thing in the JSON string
	var subId = data.substr( start, end - start );

	// if the remove is set to 1, we just need to remove it and go on
	if ( $( '#remove' + subId ) && $( '#remove' + subId ).val() > 0 )
	{
		$( this ).remove();
		return;
	} // if ( $( '#remove' + subId ) && $( '#remove' + subId ).val() > 0 )

	if ( $( '#available' + subId ).length > 0 )
	{
		var available = $( '#available' + subId ).val();
		var duration = 0;

		if ( $( this ).attr( 'data-event' ).indexOf( "00:45" ) > 0 )
		{
			duration = 40;
		}
		else if ( $( this ).attr( 'data-event' ).indexOf( "01:00" ) > 0 )
		{
			duration = 55;
		}
		else
		{
			return false;
		}

		available -= duration;
		$( '#available' + subId ).val( available );

		if ( available < 55 )
		{
			// delete the 55 item
			if ( $( '#long' + subId ).length > 0 )
			{
				$( '#long' + subId ).remove();
			}

			if ( available < 40 )
			{
				if ( $( '#short' + subId ).length > 0 )
				{
					$( '#short' + subId ).remove();
				}
			}
		}
	}
}
EOF;

$JSReceiveEvent = <<<EOF
function ( event )
{
	$.ajax( {
		url: "check-conflicts",
		data: {
				"start" : event.start.format(),
				"end" : event.end.format(),
			},
		type: "POST",
		success: function ( data, status, request ) {
			if ( data.indexOf( 'success' ) < 0 )
			{
				alert( 'problem' );
			}
		},
		error: function ( e ) {
			$( '#result' ).html( "Update Error - " + e.responseText );
		}
	} );
}
EOF;

$JSOverlap = <<<EOF
function ( event )
{
	return false;
}
EOF;

$JSSelectAllow = <<<EOF
function ( selInfo )
{
	//selInfo.start
	//selInfo.end
	//selInfo.resourceId - if using a Resource View
	return true;
}
EOF;

$JSMyEventsError = <<<EOF
function ()
{
	alert( 'error getting your currently scheduled classes' );
}
EOF;


$JSTutorEventsError = <<<EOF
function ()
{
	alert( 'error getting tutor's currently scheduled classes' );
}
EOF;

$JSEventResize = <<<EOF
function ( event, delta, revertFunc, jsEvent, ui, view )
{
	var duration = moment.duration( event.end - event.start )

	if ( duration.asHours() > 1 || ( duration.hours() == 1 && duration.minutes() > 0 ) )
	{
		revertFunc();
		alert( 'The limit is 1 hour.  If you would like, you can schedule a new time immediately following the current one.' );
	}
}
EOF;

$JSEventRender = <<<EOF
function ( event, element )
{
	element.append( "<span class='closeon'>X</span>" );
	element.find( ".closeon" ).click( function() {
		$( '#w0' ).fullCalendar( 'removeEvents', event._id );
		var available = Number( $( '#available' ).val() );
		available++;
		$( '#available' ).val( available );
	} );
}
EOF;

$JSEventClick = <<<EOF
function( calEvent, jsEvent, view )
{
	if ( confirm( 'Would you like to cancel this event?' ) == true )
	{
		$.ajax( {
			url: "cancel",
			data: {
					"id" : event.id,
				},
			type: "POST",
			success: function ( data, status, request ) {
				if ( data.indexOf( 'success' ) < 0 )
				{
					alert( 'problem' );
				}
				else
				{
					$( '#w0' ).fullCalendar( 'unselect' );
				}
			},
			error: function ( e ) {
				$( '#result' ).html( "Update Error - " + e.responseText );
			}
		} );
	}
//	alert( 'Event: ' + calEvent.title );
//	alert( 'Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY );
//	alert( 'View: ' + view.name );
//	// change the border color just for fun
//	$( this ).css( 'border-color', 'red' );
}
EOF;
?>
<script>
function reset()
{
	location.href = location.href;
/*
	$.ajax( {
		url: "schedule?tutorId=<?= $tutor->id ?>&type=<?= $type ?>&reset=true",
		data: "",
		type: "POST",
		success: function ( response ) {
			window.location.href = "schedule?tutorId=<?= $tutor->id ?>";
		}
	} );
*/
}

function save()
{
	$( '#result' ).html( '' );

	var events = $( '#w0' ).fullCalendar( 'clientEvents' );
	var arr = {};

	// if no events to save
	if ( events.length == 0 )
	{
		alert( 'No events to save.' );
		return;
	} // if ( events.length == 0 )

	// loop thru events
	for ( var i = 0; i < events.length; i++ )
	{
		// if we are scheduleing recurring events
		if ( <?= $type === TimeType::RECURRING ? 'true' : 'false' ?> )
		{
			arr[ i ] = {
				start: events[ i ].start.format( 'hh::mm' ),
				end: events[ i ].end.format( 'hh::mm' ),
				color: events[ i ].color,
				dow: events[ i ].start.day(),
				subId: events[ i ].subId,
				alreadyScheduled: events[ i ].alreadyScheduled
			};
		} // if ( <?= $type == TimeType::RECURRING ?> )

		// else we're scheduling flex/makeup/free time
		else
		{
			arr[ i ] = {
				start: events[ i ].start.format(),
				end: events[ i ].end.format(),
				color: events[ i ].color
			};
		} // else
	} // for ( var i = 0; i < events.length; i++ )
	$.ajax( {
		url: "schedule?tutorId=<?= $tutor->id ?>&type=<?= $type ?>",
		data: arr,
		type: "POST",
		success: function ( data, status, request ) {
			if ( data.indexOf( 'success' ) >= 0 )
			{
				location.href = 'scheduled?tutorId=<?= $tutor->id ?>&type=<?= $type ?>';
//				$( '#result' ).html( '<p style="color : green">Schedule saved successfully</p>' );
			}
			else
			{
				$( '#result' ).html( '<p style="color : red">' + data.replace( '\n', '<br />' ) + '</p>' );
			}
		},
		error: function ( e ) {
			$( '#result' ).html( "Update Error - " + e.responseText );
		}
	} );
}
</script>
	<h1><?= Html::encode( $this->title ) ?></h1>
	<?= Html::button( 'Save', [ 'onclick' => 'javascript:save()' ] ) ?>
	<?= Html::button( 'Reset', [ 'onclick' => 'javascript:reset()' ] ) ?>
	<?= Html::hiddenInput( 'nearestHoursForChange', Settings::getNearestHoursToChange(), [ 'id' => 'nearestHoursForChange' ] ) ?>
	<?= Html::hiddenInput( 'futureBookingLimit', Settings::getFutureBookingLimit(), [ 'id' => 'futureBookingLimit' ] ) ?>
	<?= Html::hiddenInput( 'custTimeZoneOffset', $cust->timeZoneOffset, [ 'id' => 'custTimeZoneOffset' ] )?>
	<br />
	<div id="result"></div>
	<br />

	<?php
		if ( $type == TimeType::RECURRING )
		{
			$subscriptions = $cust->getAvailableSubscriptions( $tutor->id );

			echo '<div class="row">';

			// loop thru all the subscriptions for this tutor
			foreach ( $subscriptions as $sub )
			{
				$available = $sub->minsAvailable;

				// if there are minutes available to schedule 
				if ( $available >= 40 )
				{
					echo '<div class="col-lg-2">Subscription #' . $sub->id . '<br/><div id="external-events">';
					$classSize = -1;

					// if it is evenly divisible by 40
					if ( $available % 40 == 0 )
					{
						$classSize = 40;
					} // if ( $available % 40 == 0 )

					// else if it is evenly divisible by 55
					else if ( $available % 55 == 0 )
					{
						$classSize = 55;
					} // else if ( $available % 55 == 0 )

					// if we set the class size
					if ( $classSize > 0 )
					{
						echo Html::hiddenInput( 'remove' . $sub->id, 1, [ 'id' => 'remove' . $sub->id ] );
						$durationStr = ( $classSize == 40 ? '00:45' : '01:00' );

						$classes = $available / $classSize;

						// loop thru the number for 40 minute classes
						for ( $i = 0; $i < $classes; $i++ )
						{
							echo '<div class="fc-event ui-draggable ui-draggable-handle" data-event=\'{"title":"Recurring","duration":"' . $durationStr . '","subId":' . $sub->id . '}\'> ' . $classSize . '-Minute (Drag Me)</div>'; 
						} // for ( $i = 0; $i < $classess; $i++ )
					} // if ( $classSize > 0 )

					// else it is a random amount
					else
					{
						echo Html::hiddenInput( 'remove' . $sub->id, 0, [ 'id' => 'remove' . $sub->id ] );
						echo Html::hiddenInput( 'available' . $sub->id, $available, [ 'id' => 'available' . $sub->id ] );

						// if we have at least 40 minutes available
						if ( $available >= 40 )
						{
							echo '<div id="short' . $sub->id .'" data-event=\'{"title":"Recurring","duration":"00:45","subId":' . $sub->id . '}\' class="fc-event ui-draggable ui-draggable-handle"> 40-Minute (Drag Me)</div>';

							// if we have over 55 minutes available
							if ( $available >= 55 )
							{
								echo '<div id="long' . $sub->id .'" data-event=\'{"title":"Recurring","duration":"01:00","subId":' . $sub->id . '}\' class="fc-event ui-draggable ui-draggable-handle"> 55-Minute (Drag Me)</div>';
							} // if ( $available >= 55 )
						} // if ( $available >= 40 )
					} // else

					echo '</div></div>';
				} // if ( $available >= 40 )
			} // foreach ( $subscriptions as $sub )

			echo '</div>';
		}
		else if ( $type == TimeType::FLEX )
		{
			echo Html::hiddenInput( 'available', $cust->getAvailableFlex( $tutor->id ), [ 'id' => 'available' ] );
		}
		else if ( $type == TimeType::FREE )
		{
			echo Html::hiddenInput( 'available', $cust->getAvailableFree( $tutor->id ), [ 'id' => 'available' ] );
		}
	?>
	<?php //= \yii2fullcalendar\yii2fullcalendar::widget( [ 'events' => $events ] ); ?>
	<?= yii2fullcalendar\yii2fullcalendar::widget( [
		'ajaxEvents' => Url::to( [ 'scheduled-events', 'custId' => $cust->id, 'tutorId' => $tutor->id, 'type' => $type ] ),
		'clientOptions' => [
			'selectable' => true,
			'selectHelper' => true,
			'droppable' => true,
			'editable' => true,
			'eventStartEditable' => true,
			'eventDurationEditable' => true,
			'drop' => new JSExpression( $JSDropEvent ),
			'eventReceive' => new JSExpression( $JSReceiveEvent ),
			'selectAllow' => new JSExpression( $JSSelectAllow ),
			'select' => ( $type == TimeType::RECURRING
								? new JSExpression( $JSSelectRecurring )
								: ( $type == TimeType::FLEX ? new JSExpression( $JSSelectFlex ) : new JSExpression( $JSSelectFree ) ) ),
			'eventResize' => new JSExpression( $JSEventResize ),
			'eventRender' => new JSExpression( $JSEventRender ),
			'eventClick' => new JSExpression( $JSEventClick ),
			'defaultDate' => date( 'Y-m-d' ),
			'defaultView' => 'agendaWeek',
			'availableViews' => [ 'agendaWeek' ],
			'allDaySlot' => false,
			'slotDuration' => '00:15:00',
			'snapDuration' => $type == TimeType::FREE ? '00:30:00' : '01:00:00',
			'eventColor' => 'yellow',
			'eventTextColor' => 'black',
			'selectOverlap' => false,
			'eventOverlap' => false,
			'selectConstraint' => 'businessHours',
			'eventConstraint' => 'businessHours',
			'header' => [
				'left' => 'title',
				'center' => 'agendaWeek',
				'right' => $type == TimeType::RECURRING ? '' : 'prev, next'
			],
			'businessHours' => $tutor->getAvailableTimes( $cust->user->timezone ),
			'selectConstraint' => 'businessHours',
			'timezone' => $cust->user->timezone,
		]
	] ) ?>
</div>
