<?php

use yii\helpers\Html;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t( 'app', 'Update {modelClass}: ', [ 'modelClass' => 'Customer' ] ) . $model->id;

$curUser = User::findOne( Yii::$app->user->id );
if ( $curUser->isAdmin() )
{
	$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Customers' ), 'url' => [ 'index' ] ];
}
$this->params[ 'breadcrumbs' ][] = [ 'label' => $model->id, 'url' => [ 'view', 'id' => $model->id ] ];
$this->params[ 'breadcrumbs' ][] = Yii::t( 'app', 'Update' );
?>
<div class="customer-update">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= $this->render( '_form', [ 'model' => $model, 'user' => $user ] ) ?>

</div>
