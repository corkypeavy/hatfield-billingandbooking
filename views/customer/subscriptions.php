<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Subscription;

use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t( 'app', 'Subscriptions' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="subscription-index">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'columns' => [
				[ 'attribute' => 'tutorId', 'label' => 'Tutor', 'value' => $model->tutor->user->name ],
				'startDate:date',
				'months',
				'minsPerWeek',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{pause}',
					'buttons' => [
						'pause' => function ( $url, $model ) {
							if ( $model->status == Subscription::STATUS_ACTIVE )
								return Html::a( '<span class="glyphicon glyphicon-pause"></span>', [ 'subscription/pause', 'id' => $model->id ], [ 'title' => 'Pause' ] );
							else if ( $model->status == Subscription::STATUS_PAUSED )
								return Html::a( '<span class="glyphicon glyphicon-play"></span>', [ 'subscription/unpause', 'id' => $model->id ], [ 'title' => 'Unpause' ] );
							else if  ( $model-status == Subscription::STATUS_INACTIVE )
								return Html::a( '<span class="glyphicon glyphicon-refresh"></span>', [ 'subscription/renew', 'id' => $model->id ], [ 'title' => 'Renew' ] );
						},
					],
				],
			],
		] )  ?>
	<?php Pjax::end(); ?>
</div>
