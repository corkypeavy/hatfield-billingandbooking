<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\models\Schedule;
use app\models\helpers\TimeType;
use app\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Schedule' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="customer-schedule">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= yii2fullcalendar\yii2fullcalendar::widget( [
		'ajaxEvents' => Url::to( [ 'tutor-scheduled', 'tutorId' => $tutor->id ] ),
		'clientOptions' => [
			'selectable' => false,
			'droppable' => false,
			'editable' => false,
			'eventStartEditable' => false,
			'eventDurationEditable' => false,
			'defaultDate' => date( 'Y-m-d' ),
			'defaultView' => 'agendaWeek',
			'availableViews' => [ 'agendaWeek' ],
			'allDaySlot' => false,
			'slotDuration' => '00:15:00',
			'eventColor' => 'pink',
			'eventTextColor' => 'black',
			'selectOverlap' => false,
			'eventOverlap' => false,
			'selectConstraint' => 'businessHours',
			'eventConstraint' => 'businessHours',
			'header' => [
				'left' => 'title',
				'center' => 'agendaWeek',
				'right' => 'prev, next'
			],
			'businessHours' => $tutor->getAvailableTimes( $cust->user->timezone ),
			'selectConstraint' => 'businessHours',
			'timezone' => $cust->user->timezone,
		]
	] ) ?>
</div>
