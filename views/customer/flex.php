<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t( 'app', 'Buy Flex Time' );
$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Select Tutor' ), 'url' => [ 'tutor' ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>

<div class="flex-form">
	<div class="row">
		<div class="col-lg-4">
			<?php $form = ActiveForm::begin(); ?>
				<?= $form->field( $model, 'hours' )->textInput( [ 'onkeyup' => 'calcFlexPrice( this.value )', 'autofocus' => true ] ) ?>
				<br />
				<p>Your cost for <?= Html::label( '0', null, [ 'id' => 'hoursRepeat' ] ) ?> hours is : <?= Html::label( '$00.00', null, [ 'id' => 'totalCost' ] ) ?></p>
				<br />
				<div class="bt-drop-in-wrapper">
					<div id="bt-drop-in"></div>
				</div>
				<div class="form-group">
					<?= Html::submitButton( Yii::t( 'app', 'Make Purchase' ), [ 'class' => 'btn btn-primary' ] ) ?>
				</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<h3>Pricing</h3>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<label>Maximum Price per Hour: </label>
	</div>
	<div class="col-lg-2">
		$<?= Html::label( round( $tutor->maxPPHFlex, 2 ) , null, [ 'id' => 'maxPPHFlex' ] ) ?>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<label>Minimum Price per Hour: </label>
	</div>
	<div class="col-lg-2">
		$<?= Html::label( round( $tutor->minPPHFlex, 2 ) , null, [ 'id' => 'minPPHFlex' ] ) ?>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<label>Minimum Hours for Discounts to Start: </label>
	</div>
	<div class="col-lg-2">
		<?= Html::label( $tutor->minDiscountHoursFlex , null, [ 'id' => 'minHoursForDiscount' ] ) ?>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<label>Hours at which Maximum Discounts Reached: </label>
	</div>
	<div class="col-lg-2">
		<?= Html::label( $tutor->maxDiscountHoursFlex , null, [ 'id' => 'maxHoursForDiscount' ] ) ?>
	</div>
</div>
<script src="https://js.braintreegateway.com/js/braintree-2.27.0.min.js"></script>
<script>
var clientToken = "<?= Yii::$app->braintree->call( 'ClientToken', 'generate', [] ) ?>";
braintree.setup( clientToken, "dropin", { container: "bt-drop-in" } );
</script>
<script>
function calcFlexPrice( hours )
{
	$( '#hoursRepeat' ).html( hours );
	var maxPPH = Number( $( '#maxPPHFlex' ).html() );
	var minPPH = Number( $( '#minPPHFlex' ).html() );
	var minHoursForDiscount = Number( $( '#minHoursForDiscount' ).html() );
	var maxHoursForDiscount = Number( $( '#maxHoursForDiscount' ).html() );

	// if we don't reach the minimum value for discount, use max price
	if ( hours < minHoursForDiscount )
	{
		var price = Math.round( maxPPH * hours * 100 ) / 100
		$( '#totalCost' ).html( '$' + price );
	} // if ( hours < minHoursForDiscount )

	// else we exceed the maximum discount hours, use min price
	else if ( hours >= maxHoursForDiscount )
	{
		var price = Math.round( minPPH * hours * 100 ) / 100;
		$( '#totalCost' ).html( '$' + price );
	} // else if ( hours >= maxHoursForDiscount )

	// else somewhere in the middle
	else
	{
		var slope = ( maxPPH - minPPH ) / ( minHoursForDiscount - maxHoursForDiscount );

		// slope = (y2 - y1) / (x2 - x1)
		// let (x1, y1) = ( minHoursForDiscount, maxPPH )  and   x2 = hours
		// so slope = (y2 - maxPPH) / ( hours - minHoursForDiscount )
		// so slope * ( hours - minHoursForDiscount ) = y2 - maxPPH
		// so y2 = slope * ( hours - minHoursForDiscount ) + maxPPH
		var pph = slope * ( hours - minHoursForDiscount ) + maxPPH;
		var price = Math.round( pph * hours * 100 ) / 100;
		$( '#totalCost' ).html( '$' + price );
	} // else
}
</script>
