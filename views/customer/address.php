<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Address;
?>
<div class="address-form">

	<?php $form = ActiveForm::begin( [ 'options' => [ 'id' => 'address' ] ] ); ?>

			<div class="row">
				<div class="col-lg-2">
					<?= $form->field( $model, 'type' )->dropDownList( Address::$types ) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= $form->field( $model, 'street' )->textInput( [ 'maxlength' => true ] ) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3">
					<?= $form->field( $model, 'city' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'state' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'postalCode' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'country' )->textInput() ?>
				</div>
			</div>

		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Add' ), [ 'class' => 'btn btn-success' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
