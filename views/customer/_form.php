<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Timezone;
use app\models\Address;
use app\models\Phone;
use app\models\VideoChat;
use app\models\Referrer;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">
	<?php $form = ActiveForm::begin(); ?>
		<?= $form->field( $user, 'name' )->textInput( [ 'autofocus' => true ] ) ?>

		<?= $form->field( $user, 'username' )->textInput() ?>

		<?= $form->field( $user, 'password' )->passwordInput() ?>

		<?php if ( $model->isNewRecord ) : ?>
			<?= $form->field( $user, 'email' )->textInput() ?>
		<?php else : ?>
			<div class="row">
				<div class="col-lg-4">
					<br /><b>Emails</b><br />
					<?= $user->getEmailsHtml( Yii::$app->request->url ) ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( $model->isNewRecord ) : ?>
			<div class="row">
				<div class="col-lg-2">
					<?= $form->field( $phone, 'type' )->dropDownList( Phone::$types ) ?>
				</div>
				<div class="col-lg-2">
					<?= $form->field( $phone, 'countryCode' )->textInput( [ 'maxlength' => true ] ) ?>
				</div>
				<div class="col-lg-4">
					<?= $form->field( $phone, 'number' )->textInput() ?>
				</div>
			</div>
		<?php else : ?>
			<div class="row">
				<div class="col-lg-4">
					<br /><b>Phones</b><br />
					<?= $user->getPhonesHtml( Yii::$app->request->url ) ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( $model->isNewRecord ) : ?>
			<div class="row">
				<div class="col-lg-2">
					<?= $form->field( $address, 'type' )->dropDownList( Address::$types ) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<?= $form->field( $address, 'street' )->textInput( [ 'maxlength' => true ] ) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3">
					<?= $form->field( $address, 'city' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $address, 'state' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $address, 'postalCode' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $address, 'country' )->textInput() ?>
				</div>
			</div>
		<?php else : ?>
			<div class="row">
				<div class="col-lg-4">
					<br /><b>Addresses</b><br />
					<?= $user->getAddressesHtml( Yii::$app->request->url ) ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( $model->isNewRecord ) : ?>
			<div class="row">
				<div class="col-lg-2">
					<?= $form->field( $videoChat, 'type' )->dropDownList( VideoChat::$types ) ?>
				</div>
				<div class="col-lg-2">
					<?= $form->field( $videoChat, 'accountId' )->textInput() ?>
				</div>
			</div>
		<?php else : ?>
			<div class="row">
				<div class="col-lg-2">
					<br /><b>Video Chats</b><br />
					<?= $user->getVideoChatsHtml( Yii::$app->request->url ) ?>
				</div>
			</div>
		<?php endif; ?>		

		<?= $form->field( $user, 'timezone' )->dropDownList( Timezone::$timezones ) ?>

		<?= $form->field( $model, 'howFound' )->dropDownList( ArrayHelper::map( Referrer::find()->where( [ 'canShow' => 1 ] )->all(), 'id', 'name' ), [ 'prompt' => '-- Select One -- ' ] ) ?>

		<?= $form->field( $model, 'howFoundOther' )->textInput( [ 'maxlength' => true ] ) ?>

		<?= $form->field( $user, 'notes' )->textArea() ?>

		<?php if ( $user->isAdmin() || $user->isTutor() ) : ?>
			<?= $form->field( $user, 'notesPrivate' )->textArea() ?>
		<?php endif; ?>

		<div class="form-group">
			<?= Html::submitButton( $model->isNewRecord ? Yii::t( 'app', 'Create' ) : Yii::t( 'app', 'Update' ), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
