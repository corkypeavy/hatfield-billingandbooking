<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
?>
<div class="payment-view">
	<?= DetailView::widget( [
		'model' => $model,
		'attributes' => [
			'id',
			'transactionId',
			'datetimeUTC:datetime',
			'amount',
		],
	] ) ?>
</div>