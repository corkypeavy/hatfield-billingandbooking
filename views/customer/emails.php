<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use app\models\User;

$this->title = Yii::t( 'app', 'Customer Emails' );
$this->params[ 'breadcrumbs' ][] = $this->title;
$curUser = User::findOne( Yii::$app->user->id );
?>
<div class="customer-emails">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php $form = ActiveForm::begin( [ 'action' => [ 'emails' ], 'method' => 'get' ] ); ?>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'customer' )->listBox( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_STUDENT ] )->all(), 'id', 'name' ), [ 'prompt' => 'All Students' ] ) ?>
			</div>
			<div class="col-lg-4">
				<?php
					// if the user is admin
					if ( $curUser->isAdmin() )
					{
						echo $form->field( $searchModel, 'who' )->radioList( [
							0 => 'Have ever made a purchase',
							1 => 'Have purchased recently',
							2 => 'Signed up but never purchased',
							3 => 'Signed up recently but never purchased',
							4 => 'All who have signed up',
							5 => 'All who have signed up recently'
						] );
					}
					// else if the user must be a tutor to get here
					else
					{
						echo $form->field( $searchModel, 'who' )->radioList( [
							0 => 'Have ever made a purchase from you',
							1 => 'Have purchased recently from you'
						] );
					}
				?>
			</div>
			<?php if ( $curUser->isAdmin() ) : ?>
				<div class="col-lg-4">
					<?= $form->field( $searchModel, 'tutor' )->listBox( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_TUTOR ] )->all(), 'id', 'name' ), [ 'prompt' => 'All Tutors' ] ) ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'show' )->radioList( [ 0 => 'Name and Email', 1 =>'Email Only' ] ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'recentMonths' )->textInput() ?>
			</div>
		</div>
		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Search' ), [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::resetButton( Yii::t( 'app', 'Reset' ), [ 'class' => 'btn btn-default' ] ) ?>
		</div>
	<?php ActiveForm::end(); ?>

	<?= ListView::widget( [
		'dataProvider' => $dataProvider,
		'layout' => '{items}',
		'itemView' => function ( $model, $key, $index, $widget ) {
			if ( $searchModel->show == '0' )
			{
				return $model->user->name . ' &lt;' . $model->user->email . '&gt;, ';
			}
			else
			{
				return $model->user->email . ', ';
			}
		}
	] ) ?>
</div>
