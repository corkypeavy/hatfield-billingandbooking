<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tutor */
use yii\helpers\VarDumper;
?>
<div class="tutor-view">

	<?= DetailView::widget( [
		'model' => $model,
		'attributes' => [
			[ 'attribute' => 'name', 'value' => $model->user->name ],
			'bio',
			[ 'label' => 'Video Chats', 'format' => 'raw', 'value' => $model->user->getVideoChatsHtml() ],
			[ 'label' => 'Timezone', 'value' => $model->user->timezone ],
			[
				'attribute' => 'photo',
				'format' => 'raw',
				'value' => ( empty( $model->photo ) ? '' : '<img src="data:image;base64,' . base64_encode( $model->photo ).'" height="200" />' )
			],
		],
	] ) ?>
</div>
