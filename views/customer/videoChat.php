<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\VideoChat;
?>
<div class="video-chat-form">

	<?php $form = ActiveForm::begin( [ 'options' => [ 'id' => 'videoChat' ] ] ); ?>

		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $model, 'type' )->dropDownList( VideoChat::$types, [ 'autofocus' => true ] ) ?>
			</div>
			<div class="col-lg-3">
				<?= $form->field( $model, 'accountId' )->textInput() ?>
			</div>
		</div>

		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Add' ), [ 'class' => 'btn btn-success' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
