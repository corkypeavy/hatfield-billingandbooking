<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Purchases' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="purchase-index">

	<div class="body-content">
		<h1><?= Html::encode( $this->title ) ?></h1>
		<div class="row">
			<div class="col-lg-10">
				<p><?= Html::a( "Purchase Flex Time - hours can be used any time",
									 Url::to( [ 'tutor', 'type' => 'flex' ] ),
									 [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Purchase Recurring Time - hours where you study on a regular weekly schedule (more affordable)",
									 Url::to( [ 'tutor', 'type' => 'recurring' ] ),
									 [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Custom Purchase - Translation, Tip/Gift, etc.",
									  Url::to( [ 'custom' ] ),
									  [ "class" => "btn btn-default" ] ) ?></p>
			</div>
		</div>
	</div>
</div>
