<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $user app\models\User */

$this->title = 'Booking and Billing';

$balance = $model->getOutstandingBalance();
?>
<div class="customer-tasks">

	<h1>Welcome, <?= $model->user->name ?></h1>
	<br /><br />

	<div class="body-content">
		<?php if ( $balance > 0.0 ) : ?>
			<div class="row">
				<p><?= Html::a( "Pay Outstanding Balance of $" . $balance, Url::to( [ 'customer/purchases' ] ) ) ?></p> 
			</div>
		<?php endif; ?>
		<div class="row">
			<h3>You have the following recurring subscriptions:</h3>
			<?= $model->getRecurringSubscriptionsHtml() ?>
			<hr />
			<h3>Your flex time balances:</h3>
			<?= $model->getFlexBalancesHtml() ?>
			<hr />
		</div>
		<div class="row">
			<div class="col-lg-4">
				<p><?= Html::a( "Make Purchase", Url::to( [ 'customer/purchase' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Billing History", Url::to( [ 'customer/history' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Manage Subscriptions", Url::to( [ 'customer/subscriptions' ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
			<div class="col-lg-4">
				<p><?= Html::a( "Future Appointments List", Url::to( [ 'customer/appointments' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Calendar", Url::to( [ 'customer/calendar' ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
			<div class="col-lg-4">
				<p><?= Html::a( "My Account", Url::to( [ 'customer/view', 'id' => $model->id ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Schedule Trial with New Tutor", Url::to( [ 'customer/free-trial' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Get Tutor Contact Info", Url::to( [ 'tutor/contact-info' ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
		</div>
	</div>
</div>
