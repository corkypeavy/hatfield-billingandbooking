<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t( 'app', 'Unpaid Invoices' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="invoice-index">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'columns' => [
				[ 'attribute' => 'tutorId', 'label' => 'Tutor', 'value' => $model->tutor->user->name ],
				'createdDate',
				[ 'attribute' => 'type', 'value' => function ( $model ) { return $model->typeString(); } ],
				'price',
				'id',
				[ 'attribute' => 'status', 'format' => 'raw', 'value' => function ( $model ) { return $model->statusHtml(); } ],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{pay}',
					'buttons' => [
						'pay' => function ( $url, $model ) {
							return  Html::a( '<span class="glyphicon glyphicon-usd"></span>', [ 'invoice/pay', 'id' => $model->id ], [ 'title' => 'Pay' ] );
						},
					],
				],
			],
		] )  ?>
	<?php Pjax::end(); ?>
</div>
