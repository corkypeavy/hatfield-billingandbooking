<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TutorSearch */
/* @var $userSearch app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Select Tutor' );
// if this is not a free trial
if ( !strpos( Yii::$app->request->url, 'free-trial' ) )
{
	$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Purchase Types' ), 'url' => [ 'choose' ] ];
} // if ( !strstr( Yii::$app->request->url, 'free-trial' ) )

$this->params[ 'breadcrumbs' ][] = $this->title;

use yii\helpers\VarDumper;
?>
<div class="tutor-index">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'columns' => [
				'user.name',
				'user.email',
				'bio',
				[ 'attribute' => 'photo', 'format' => 'raw', 'value' => function ( $model ) {
					if ( !empty ( $model->photo ) )
					{
						return '<img src="data:image;base64,' . base64_encode( $model->photo ).'" height="50" />'; 
					}
					else
					{
						return '';
					}
				} ],
				'user.timezone',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{view} {available} {select}',
					'buttons' => [
						'view' => function ( $url, $model, $key ) {
							return Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', '#', [
								'value' => Url::to( [ 'view-tutor', 'id' => $key ] ),
								'title' => 'View Tutor',
								'class' => 'showModalButton' ] );
						},
						'available' => function ( $url, $model, $key ) {
							return Html::a( '<span class="glyphicon glyphicon-clock"></span>', '#', [
								'value' => Url::to( [ 'view-available', 'id' => $key ] ),
								'title' => 'View Available',
								'class' => 'showModalButton' ] );
						},
						'select' => function ( $url, $model, $key ) use ( $type ) {
							return Html::a( 'Select', [ $type, 'tutorId' => $key ], [ 'title' => Yii::t( 'app', 'Select' ) ] );
						},
					],
				],
			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
