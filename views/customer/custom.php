<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomPurchase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-form">
	<?php $form = ActiveForm::begin(); ?>
		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $model, 'amount' )->textInput() ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $model, 'tutorId' )->dropDownList( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_TUTOR ] )->all(), 'id', 'name' ), [ 'prompt' => '-- No Tutor --' ] ) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<?= $form->field( $model, 'description' )->textArea() ?>
			</div>
		</div>
		<div class="bt-drop-in-wrapper">
			<div id="bt-drop-in"></div>
		</div>
		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Pay' ), [ 'class' => 'btn btn-success' ] ) ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
<script src="https://js.braintreegateway.com/js/braintree-2.27.0.min.js"></script>
<script>
var clientToken = "<?= Yii::$app->braintree->call( 'ClientToken', 'generate', [] ) ?>";
braintree.setup( clientToken, "dropin", { container: "bt-drop-in" } );
</script>
