<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t( 'app', 'Purchase Recurring' );
$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Select Tutor' ), 'url' => [ 'tutor' ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>

<div class="recurring-form">
	<div class="row">
		<div class="col-lg-6">
			<?= Html::hiddenInput( 'maxPPM', $tutor->maxPPMRecurring, [ 'id' => 'maxPPM' ] ) ?>
			<?= Html::hiddenInput( 'minPPM', $tutor->minPPMRecurring, [ 'id' => 'minPPM' ] ) ?>
			<?= Html::hiddenInput( 'minDiscMins', $tutor->minDiscountMinsRecurring, [ 'id' => 'minDiscMins' ] ) ?>
			<?= Html::hiddenInput( 'maxDiscMins', $tutor->maxDiscountMinsRecurring, [ 'id' => 'maxDiscMins' ] ) ?>
			<?php $form = ActiveForm::begin(); ?>
				<?= $form->field( $model, 'minsPerWeek' )->textInput( [ 'onkeyup' => 'calcCosts()', 'autofocus' => true ] ) ?>

				<br />

				<p>Cost Per Month: <?= Html::label( '$0', NULL, [ 'id' => 'costPerMonth' ] ) ?></p>
				<p>Cost Per Hour: <?= Html::label( '$0', NULL, [ 'id' => 'costPerHour' ] ) ?> (for an "average" month)</p>
				<p><?php //= Html::label( '0', NULL, [ 'id' => 'hoursPerMonth' ] ) ?> hours in an average month.</p>
				<p>Months vary in length. An average month has 4.35 weeks</p>

				<br />

				<?= $form->field( $model, 'startDate' )->widget( DatePicker::className(), [ 'options' => [ 'class' => 'form-control' ], 'clientOptions' => [ 'maxDate' => 30, 'minDate' => 0 ], 'dateFormat' => 'yyyy/MM/dd' ] ) ?>

				<?= $form->field( $model, 'months' )->textInput() ?>
				<p>You can always change this or any aspect of your subscription later.</p>
				<div class="bt-drop-in-wrapper">
					<div id="bt-drop-in"></div>
				</div>
				<div class="form-group">
					<?= Html::submitButton( Yii::t( 'app', 'Make Purchase' ), [ 'class' => 'btn btn-primary' ] ) ?>
				</div>
			<?php ActiveForm::end(); ?>
		</div>
		<div class="col-lg-6">
			<h3>Automatically calculate Total Minutes Per Week</h3>
			<p><?= Html::button( '2 sessions 40 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 2, 40 );',
				] ) ?></p>
			<p><?= Html::button( '3 sessions 40 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 3, 40 );',
				] ) ?></p>
			<p><?= Html::button( '4 sessions 40 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 4, 40 );',
				] ) ?></p>
			<p><?= Html::button( '5 sessions 40 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 5, 40 );',
				] ) ?></p>
			<p><?= Html::button( '2 sessions 55 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 2, 55 );',
				] ) ?></p>
			<p><?= Html::button( '3 sessions 55 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 3, 55 );',
				] ) ?></p>
			<p><?= Html::button( '4 sessions 55 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 4, 55 );',
				] ) ?></p>
			<p><?= Html::button( '5 sessions 55 minutes each per week', [
					'class' => 'btn btn-success',
					'onclick' => 'autoCalcTime( 5, 55 );',
				] ) ?></p>
		</div>
	</div>
</div>
<script src="https://js.braintreegateway.com/js/braintree-2.27.0.min.js"></script>
<script>
var clientToken = "<?= Yii::$app->braintree->call( 'ClientToken', 'generate', [] ) ?>";
braintree.setup( clientToken, "dropin", { container: "bt-drop-in" } );
</script>

<script src="/js/calculations.js"></script>
<script>
function autoCalcTime( sessions, minutes )
{
	$( '#subscription-minsperweek' ).val( sessions * minutes );
	calcCosts();
}

function calcCosts()
{
	var minsPerWeek = Number( $( '#subscription-minsperweek' ).val() );
	var minsPerMonth = minsPerWeek * 4.35;
	var pricePerMonth = calcRecurringPrice( minsPerWeek,
														 Number( $( '#minPPM' ).val() ),
														 Number( $( '#maxPPM' ).val() ),
														 Number( $( '#minDiscMins' ).val() ),
														 Number( $( '#maxDiscMins' ).val() ) );
	if ( pricePerMonth > 0 )
	{
		$( '#costPerMonth' ).html( pricePerMonth );
		$( '#costPerHour' ).html( pricePerMonth / minsPerMonth * 60 );		 
	}
}
/*
$( function() {
	$( document ).on( 'change', '#subscription-minsPerWeek', function() {
		calcCosts();
	} );
} );
*/
</script>