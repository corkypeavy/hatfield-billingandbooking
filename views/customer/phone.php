<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Phone;
?>
<div class="phone-form">

	<?php $form = ActiveForm::begin( [ 'options' => [ 'id' => 'phone' ] ] ); ?>

		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $model, 'type' )->dropDownList( Phone::$types, [ 'autofocus' => true ] ) ?>
			</div>
			<div class="col-lg-2">
				<?= $form->field( $model, 'countryCode' )->textInput( [ 'maxlength' => true ] ) ?>
			</div>
			<div class="col-lg-3">
				<?= $form->field( $model, 'number' )->textInput() ?>
			</div>
		</div>

		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Add' ), [ 'class' => 'btn btn-success' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
