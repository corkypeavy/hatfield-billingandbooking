<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Settings;
use app\models\helpers\PurchaseType;

/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $form yii\widgets\ActiveForm */

use app\models\Purchase;
use app\models\User;
?>

<div class="purchase-form">

	<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<?= $form->field( $model, 'customerId' )->dropDownList( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_STUDENT ] )->all(), 'id', 'name' ), [ 'prompt' => 'Select a Customer' ] ) ?>

				<?php
					$curUser = User::findOne( Yii::$app->user-id );

					if ( $curUser->isAdmin() )
					{
						echo $form->field( $model, 'tutorId' )->dropDownList( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_TUTOR ] )->all(), 'id', 'name' ), [ 'prompt' => 'Select a Tutor', 'onchange' => 'tutorChanged( this.value )' ] );
						echo Html::hiddenInput( 'maxPPMRecurring', Settings::getHighestPricePerMinuteRecurring(), [ 'id' => 'maxPPMRecurring' ] );
						echo Html::hiddenInput( 'minPPMRecurring', Settings::getLowestPricePerMinuteRecurring(), [ 'id' => 'minPPMRecurring' ] );
						echo Html::hiddenInput( 'minMinsForDiscount', Settings::getMinimumMinutesForDiscountsRecurring(), [ 'id' => 'minMinsForDiscount' ] );
						echo Html::hiddenInput( 'maxMinsForDiscount', Settings::getMaximumMinutesForDiscountsRecurring(), [ 'id' => 'maxMinsForDiscount' ] );
						echo Html::hiddenInput( 'maxPPHFlex', Settings::getHighestPricePerHourFlex(), [ 'id' => 'maxPPHFlex' ] );
						echo Html::hiddenInput( 'minPPHFlex', Settings::getLowestPricePerHourFlex(), [ 'id' => 'minPPHFlex' ] );
						echo Html::hiddenInput( 'minHoursForDiscount', Settings::getMinimumHoursForDiscountsFlex(), [ 'id' => 'minHoursForDiscount' ] );
						echo Html::hiddenInput( 'maxHoursForDiscount', Settings::getMaximumHoursForDiscountsFlex(), [ 'id' => 'maxHoursForDiscount' ] );
					}
					else
					{
						$tutor = Tutor::findOne( $curUser->id );
						echo Html::hiddenInput( 'maxPPMRecurring', $tutor->maxPPMRecurring, [ 'id' => 'maxPPMRecurring' ] );
						echo Html::hiddenInput( 'minPPMRecurring', $tutor->minPPMRecurring, [ 'id' => 'minPPMRecurring' ] );
						echo Html::hiddenInput( 'minMinsForDiscount', $tutor->minDiscountMinsRecurring, [ 'id' => 'minMinsForDiscount' ] );
						echo Html::hiddenInput( 'maxMinsForDiscount', $tutor->maxDiscountMinsRecurring, [ 'id' => 'maxMinsForDiscount' ] );
						echo Html::hiddenInput( 'maxPPHFlex', $tutor->maxPPHFlex, [ 'id' => 'maxPPHFlex' ] );
						echo Html::hiddenInput( 'minPPHFlex', $tutor->minPPHFlex, [ 'id' => 'minPPHFlex' ] );
						echo Html::hiddenInput( 'minHoursForDiscount', $tutor->minDiscountHoursFlex, [ 'id' => 'minHoursForDiscount' ] );
						echo Html::hiddenInput( 'maxHoursForDiscount', $tutor->maxDiscountHoursFlex, [ 'id' => 'maxHoursForDiscount' ] );
					}
				?>
				<?= $form->field( $model, 'type' )->dropDownList( [
						PurchaseType::RECURRING => 'Recurring',
						PurchaseType::FLEX => 'Flex',
						PurchaseType::OTHER => 'Custom'
					],
					[
						'onchange' => 'typeChanged()'
					]
				) ?>

				<?= $form->field( $model, 'description')->textArea( [ 'maxlength' => true ] ) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4">
				<div id="recurring" style="display:<?= $model->type == PurchaseType::RECURRING ? 'block' : 'none' ?>">
					<?= $form->field( $subscription, 'minsPerWeek' )->textInput( [ 'onkeyup' => 'calcRecurringPrice( this.value )' ] ) ?>
					<?php //= $form->field( $model, 'price' )->label() ?>
					<?= Html::label( 'Price', [ 'class' => 'control-label' ] ) ?>
					<?= Html::textInput( 'priceRecurring', null, [ 'class' => 'form-control', 'id' => 'priceRecurring', 'readonly' => true ] ) ?>
					<?= $form->field( $subscription, 'startDate' )->widget( DatePicker::classname(), [
						'options' => [ 'class' => 'form-control' ],
						'dateFormat' => 'yyyy-MM-dd',
					] ) ?>
					<?= $form->field( $subscription, 'months' )->textInput() ?>
				</div>
				<div id="flex" style="display:<?= $model->type == PurchaseType::FLEX ? 'block' : 'none' ?>">
					<?= $form->field( $model, 'quantity' )->textInput( [ 'onkeyup' => 'calcFlexPrice( this.value )' ] ) ?>
					<?= Html::label( 'Price', [ 'class' => 'control-label' ] ) ?>
					<?= Html::textInput( 'priceFlex', null, [ 'class' => 'form-control', 'id' => 'priceFlex', 'readonly' => true ] ) ?>
				</div>
				<div id="other" style="display:<?= $model->type == PurchaseType::OTHER ? 'block' : 'none' ?>">
					<?= Html::label( 'Price', [ 'class' => 'control-label' ] ) ?>
					<?= Html::textInput( 'priceOther', null, [ 'class' => 'form-control' ] ) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-offset-4 col-lg-2">
				<br />
				<label><?= Html::checkbox( 'sendNow', false ) ?> Send Invoice Now </label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-offset-5 col-lg-2">
				<div class="form-group">
					<br />
					<?= Html::submitButton( $model->isNewRecord ? Yii::t( 'app', 'Create' ) : Yii::t( 'app', 'Update' ), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
				</div>
			</div>
		</div>

	<?php ActiveForm::end(); ?>

</div>
<script src="/js/calculations.js" />
<script>
function tutorChanged( id )
{
	$.get( "/tutor/prices?id=" + id, function ( data, status ) {
		if ( status == 'success' )
		{
			$( '#maxPPMRecurring' ).val( data[ 'maxPPMRecurring' ] );
			$( '#minPPMRecurring' ).val( data[ 'minPPMRecurring' ] );
			$( '#minMinsForDiscount' ).val( data[ 'minMinsForDiscount' ] );
			$( '#maxMinsForDiscount' ).val( data[ 'maxMinsForDiscount' ] );
			$( '#maxPPHFlex' ).val( data[ 'maxPPHFlex' ] );
			$( '#minPPHFlex' ).val( data[ 'minPPHFlex' ] );
			$( '#minHoursForDiscount' ).val( data[ 'minHoursForDiscount' ] );
			$( '#maxHoursForDiscount' ).val( data[ 'maxHoursForDiscount' ] );

			$recPrice = calcRecurringPrice( Number( $( '#subscription-minsperweek' ).val() ), Number( data[ 'minPPMRecurring' ] ),
														Number( data[ 'maxPPMRecurring' ] ), Number( data[ 'minMinsForDiscount' ] ),
														Number( data[ 'maxMinsForDiscount' ] ) );
			$( '#priceRecurring' ).val( $recPrice );

			$flexPrice = calcFlexPrice( Number( $( '#purchase-quantity' ).val() ), Number( data[ 'minPPHFlex' ] ),
													Number( data[ 'maxPPHFlex' ] ), Number( data[ 'minHoursForDiscount' ] ),
													Number( data[ 'maxHoursForDiscount' ] ) );
			$( '#priceFlex' ).val( $flexPrice );
		}
		else
		{
			alert( 'error retrieving tutor pricing data' );
		}
	} );
}

function typeChanged()
{
	if ( document.forms[ 'w0' ][ 'Purchase[type]' ].value == <?= PurchaseType::RECURRING ?> )
	{
		$( '#flex' ).hide();
		$( '#other' ).hide();
		$( '#recurring' ).show();
	}
	else if ( document.forms[ 'w0' ][ 'Purchase[type]' ].value == <?= PurchaseType::FLEX ?> )
	{
		$( '#recurring' ).hide();
		$( '#other' ).hide();
		$( '#flex' ).show();
	}
	else
	{
		$( '#recurring' ).hide();
		$( '#flex' ).hide();
		$( '#other' ).show();
	}
}
</script>