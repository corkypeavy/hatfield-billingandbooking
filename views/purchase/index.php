<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Purchases' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="purchase-index">

	<h1><?= Html::encode( $this->title ) ?></h1>
	<?php
	echo $this->render( '_search', [ 'model' => $searchModel, 'curUser' => $curUser ] );
	?>
	<hr />

	<p><?= Html::a( Yii::t( 'app', 'Create Purchase' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?></p>
	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
//			'filterModel' => $searchModel,
			'columns' => [
				[ 'attribute' => 'customerId', 'label' => 'Customer', 'value' => $model->customer->user->name ],
				'createdDttmUTC',
				[ 'attribute' => 'type', 'format' => 'raw', 'value' => function ( $model ) { return $model->typeString(); } ],
				'totalPrice',
				'id',
				[ 'attribute' => 'status', 'format' => 'raw', 'value' => function ( $model ) { return $model->statusHtml(); } ],
				[ 'class' => 'yii\grid\ActionColumn' ],
			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
