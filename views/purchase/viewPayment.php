<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<div class="payment-view">

    <?= DetailView::widget( [
        'model' => $model,
        'attributes' => [
            'id',
            'amount',
            'transactionId',
            'datetimeUTC',
        ],
    ] ) ?>

</div>
