<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="purchase-search">

	<?php $form = ActiveForm::begin( [ 'action' => [ 'index' ], 'method' => 'get' ] ); ?>

		<div class="row">
			<div class="col-lg-6">
				<?= $form->field( $model, 'dateRange' )->radioList(
					[
						0 => 'Last Month',
						1 => 'This Month',
						2 => 'Custom'
					],
					[
						'onclick' => 'dateRangeChange()'
					] ) ?>
				<?= $form->field( $model, 'start' )->widget( DatePicker::classname(), [
					'options' => [ 'class' => 'form-control', 'disabled' => ( $model->dateRange != 2 ) ],
					'dateFormat' => 'yyyy-MM-dd',
					'clientOptions' => [
						'defaultDate' => $model->start->format( 'Y-m-d' )
					]
				] ) ?>

				<?= $form->field( $model, 'end' )->widget( DatePicker::classname(), [
					'options' => [ 'class' => 'form-control', 'disabled' => ( $model->dateRange != 2 ) ],
					'dateFormat' => 'yyyy-MM-dd',
					'clientOptions' => [
						'defaultDate' => $model->end->format( 'Y-m-d' )
					]
				] ) ?>
			</div>

			<div class="col-lg-3">
				<?= $form->field( $model, 'customerId' )->listBox( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_STUDENT ] )->all(), 'id', 'name' ), [ 'prompt' => 'All Students' ] ) ?>
			</div>
			<div class="col-lg-3">
				<?php
					if ( $curUser->isAdmin() )
					{
						echo $form->field( $model, 'tutorId' )->listBox( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_TUTOR ] )->all(), 'id', 'name' ), [ 'prompt' => 'All Tutors' ] );
					}
					else
					{
						echo $form->field( $model, 'tutorId' )->hiddenInput();
						echo $curUser->name;
					}
				?>
			</div>
		</div>

		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Search' ), [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::resetButton( Yii::t( 'app', 'Reset' ), [ 'class' => 'btn btn-default' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
<script>
function dateRangeChange()
{
	if ( document.forms[ 'w0' ][ 'PurchaseSearch[dateRange]' ].value == 0 )
	{
		// set appropriate values
		curDate = new Date();
		curYear = curDate.getFullYear();
		curMonth = curDate.getMonth();
		endDate = new Date( curYear, curMonth, 0 );

		// if it is january we need to change the year too
		if ( curMonth == 0 )
		{
			curYear--;
			curMonth = 11;
		}
		else
		{
			curMonth--;
		}

		startDate = new Date( curYear, curMonth, 1 );

		$( '#purchasesearch-start' ).val( curYear + '-' + ( curMonth + 1 ) + '-01' );
		$( '#purchasesearch-end' ).val( curYear + '-' + ( curMonth + 1 ) + '-' + endDate.getDate() );
		$( '#purchasesearch-start' ).prop( 'disabled', true );
		$( '#purchasesearch-end' ).prop( 'disabled', true );
	}
	else if ( document.forms[ 'w0' ][ 'PurchaseSearch[dateRange]' ].value == 1 )
	{
		// set appropriate values
		curDate = new Date();
		curYear = curDate.getFullYear();
		curMonth = curDate.getMonth();
		startDate = new Date( curYear, curMonth, 1 );
		endDate = new Date( curYear, curMonth + 1, 0 );

		$( '#purchasesearch-start' ).val( curYear + '-' + ( curMonth + 1 ) + '-01' );
		$( '#purchasesearch-end' ).val( curYear + '-' + ( curMonth + 1 ) + '-' + endDate.getDate() );
		$( '#purchasesearch-start' ).prop( 'disabled', true );
		$( '#purchasesearch-end' ).prop( 'disabled', true );
	}
	else 
	{
		curDate = new Date();
		curYear = curDate.getFullYear();
		curMonth = curDate.getMonth();

		$( '#purchasesearch-start' ).val( curYear + '-' + ( curMonth + 1 ) + '-' + curDate.getDate() );
		$( '#purchasesearch-end' ).val( curYear + '-' + ( curMonth + 1 ) + '-' + curDate.getDate() );
		$( '#purchasesearch-start' ).prop( 'disabled', false );
		$( '#purchasesearch-end' ).prop( 'disabled', false );
	}
}
</script>
