<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pay */
/* @var $form yii\widgets\ActiveForm */

$model->amount = $purchase->totalPrice;
?>

<?= DetailView::widget( [
	'model' => $purchase,
	'attributes' => [
		[ 'attribute' => 'id', 'label' => 'Purchase #' ],
		[ 'attribute' => 'tutorId', 'label' => 'Tutor', 'value' => $purchase->tutor->user->name ],
		[ 'attribute' => 'type', 'value' => $purchase->typeString() ],
		'quantity',
		'totalPrice:currency',
		[ 'attribute' => 'status', 'value' => $purchase->statusString() ],
		'notes',
	],
] ) ?>

<div class="purchase-form">
	<?php $form = ActiveForm::begin(); ?>
		<div class="bt-drop-in-wrapper">
			<div id="bt-drop-in"></div>
		</div>
		<div class="row">
			<div class="col-lg-2">
				<?= $form->field( $model, 'amount' )->hiddenInput() ?>
				<h2><?= Html::label( $model->amount, [ 'class' => 'control-label' ] ) ?></h2>
			</div>
		</div>
		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Pay' ), [ 'class' => 'btn btn-success' ] ) ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
<script src="https://js.braintreegateway.com/js/braintree-2.27.0.min.js"></script>
<script>
//var checkout = new Demo( { formID: 'payment-form' } );
var clientToken = "<?= Yii::$app->braintree->call( 'ClientToken', 'generate', [] ) ?>";
braintree.setup( clientToken, "dropin", { container: "bt-drop-in" } );
</script>
