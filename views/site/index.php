<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Booking and Billing';
?>
<div class="site-index">

	<div class="jumbotron">
		<h1>Welcome to Booking and Billing!</h1>
	</div>

	<div class="body-content">

		<div class="row">
			<div class="col-lg-offset-4 col-lg-4">
				<p><?= Html::a( "Login", Url::to( [ 'site/login' ] ), [ "class" => "btn btn-success" ] ) ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4">
				<p><?= Html::a( "Sign Up", Url::to( [ 'customer/signup' ] ), [ "class" => "btn btn-success" ] ) ?></p>
			</div>
		</div>

	</div>
</div>
