<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
/* @var $this yii\web\View */

$this->title = 'Booking and Billing';
$id = Yii::$app->user->id;
$curUser == User::findOne( $id );
?>
<div class="site-index">

	<div class="jumbotron">
		<h1>Welcome to Booking and Billing!</h1>
	</div>

	<div class="body-content">
		<div class="row">
			<div class="col-lg-4">
				<p><?= Html::a( "Tutor Accounts", Url::to( [ 'tutor/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Tutor Email Addresses", Url::to( [ 'tutor/emails' ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
			<div class="col-lg-4">
				<p><?= Html::a( "Student Accounts", Url::to( [ 'customer/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Student Email Addresses", Url::to( [ 'customer/emails' ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
			<div class="col-lg-4">
				<?php if ( $curUser->type == User::TYPE_SUPER_ADMIN ) : ?>
					<p><?= Html::a( "Admin Accounts", Url::to( [ 'user/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<?php endif; ?>
				<p><?= Html::a( "Calendar", Url::to( [ 'admin/calendar' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Invoices and Payments", Url::to( [ 'purchase/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Global Settings", Url::to( [ 'settings/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?php //= Html::a( "Prices and Offerings", Url::to( [ 'price/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Referrers", Url::to( [ 'referrer/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "My Account", Url::to( [ 'user/view', 'id' => $id ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
		</div>
	</div>
</div>
