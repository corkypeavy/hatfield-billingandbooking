<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Dunning;

use yii\helpers\VarDumper;

/* @var $this yii\web\View */

$this->title = 'Dunning Table';
$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Settings' ), 'url' => [ 'settings/index' ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;

$nextStep = Dunning::find()->max( 'step' ) + 1;
?>

<h1><?= Html::encode( $this->title ) ?></h1>
<hr />
<div id="dunning">
	<div class="row">
		<div class="col-lg-2">Step</div>
		<div class="col-lg-2">Days</div>
		<div class="col-lg-4">Message</div>
	</div>
	<?php
	$duns = Dunning::find()->orderBy( 'step ASC' )->all();

	foreach ( $duns as $d )
	{
		echo '<div id="row' . $d->step . '" class="row">' .
					'<div class="col-lg-2">' . $d->step . '</div>' .
					'<div id="days' . $d->step . '" class="col-lg-2">' . $d->days . '</div>' .
					'<div id="message' . $d->step . '" class="col-lg-4">' . $d->message . '</div>' .
				'</div>';
	}
	?>
</div>
<div><button id="removeLast" <?= count( $duns ) == 0 ? 'style="display:none"' : '' ?> onclick="removeLast()">Remove Last</button></div>
<hr />
<div class="dunning">
	<div class="row">
		<div class="col-lg-2">
			<!-- <label>Step</label> --><div id="step"><?= $nextStep ?></div>
		</div>
		<div class="col-lg-2">
			<!-- <label>Days</label> --><div><input id="days" type="number" min="1" step="1" /></div>
		</div>
		<div class="col-lg-4">
			<!-- <label>Message</label> --><div><textarea id="message" rows="10" cols="50" ></textarea></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-2 col-lg-2">
			<button id="Add" onclick="add()">Add</button>&nbsp;
			<button id="Save" onclick="save()">Save</button>
		</div>
	</div>
	<div class="row">
		<div id="result" />
	</div>
</div>

<script>
function save()
{
	var arr = {};
	var done = false;
	var i = 1;
	
	while ( !done )
	{
		if ( $( '#row' + i ).length > 0 )
		{
			arr[ i - 1 ] = {
				step: i,
				days: $( '#days' + i ).html(),
				message: htmlEncode( $( '#message' + i ).html() )
			};
			i++;
		}
		else
		{
			done = true;
		}
	}

	$.ajax( {
		url: "dunning",
		data: arr,
		type: "POST",
		success: function ( data, status, request ) {
			if ( data.indexOf( 'success' ) >= 0 )
			{
alert( 'success' );
				location.href = 'index';
//				$( '#result' ).html( '<p style="color : green">Schedule saved successfully</p>' );
			}
			else
			{
alert( 'not success' );
				$( '#result' ).html( '<p style="color : red">' + data.replace( '\n', '<br />' ) + '</p>' );
			}
		},
		error: function ( e ) {
			$( '#result' ).html( "Update Error - " + e.responseText );
		}
	} );
}

function add()
{
	if ( isNaN( parseInt( $( '#days' ).val() ) ) )
	{
		$( '#days' ).focus();
		return;
	}
	if ( $( '#message' ).val().length == 0 )
	{
		$( '#message' ).focus();
		return;
	}

	var step = Number( $( '#step' ).text() );
	var days = Number( $( '#days' ).val() );
	var msg = htmlEncode( $( '#message' ).val() );

	$( '#dunning' ).append( '<div id="row' + step + '" class="row">' +
										'<div class="step col-lg-2">' + step + '</div>' +
										'<div id="days' + step + '" class="col-lg-2">' + days + '</div>' +
										'<div id="message' + step + '" class="col-lg-4">' + msg + '</div>' +
									'</div>' );
/*	
must add jquery-ui before using drag/drop
	$( '#row' + step ).draggable( {
		revert: true,      // immediately snap back to original position
		revertDuration: 0  //
	} ); */
	step++;
	$( '#step' ).text( step );
	$( '#days' ).val( '' );
	$( '#message' ).val( '' );
	$( '#removeLast' ).show();
	$( '#days' ).focus();
}

function removeLast()
{
	var step = Number( $( '#step' ).text() ) - 1;

	$( '#row' + step ).remove();
	$( '#step' ).text( step );

	if ( step == 1 )
	{
		$( '#removeLast' ).hide();
	}
}

function htmlEncode( value )
{
  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
  //then grab the encoded contents back out.  The div never exists on the page.
  return $( '<div/>' ).text( value ).html();
}

function htmlDecode( value )
{
  return $('<div/>').html( value ).text();
}
</script>