<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\models\User;
use app\models\Tutor;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Available Times (Weekly)' );
$tutor = Tutor::findOne( $tutorId );
$this->params[ 'breadcrumbs' ][] = [ 'label' => $tutor->user->name, 'url' => [ 'view', 'id' => $tutor->id ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;

$curUser = User::findOne( Yii::$app->user->id );

$DragJS = <<<EOF
/* initialize the external events
-----------------------------------------------------------------*/
$( '#external-events .fc-event' ).each( function() {
    // store data so the calendar knows to render an event upon drop
    $( this ).data( 'event', {
        title: $.trim( $( this ).text() ), // use the element's text as the event title
        stick: true // maintain when user navigates (see docs on the renderEvent method)
    } );
    // make the event draggable using jQuery UI
    $( this ).draggable( {
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
    } );
} );
EOF;
$this->registerJs( $DragJS );
?>
<div class="customer-schedule">

<?php
$JSCode = <<<EOF
function ( start, end )
{
//	var title = prompt( 'Event Title:' );
	var eventData;
//	if ( title )
//	{
		eventData = {
			title: '',//title,
			start: start,
			end: end
		};
		$( '#w0' ).fullCalendar( 'renderEvent', eventData, true );
//	}
	$( '#w0' ).fullCalendar( 'unselect' );
}
EOF;

$JSDropEvent = <<<EOF
function ( date )
{
//	alert( "Dropped on " + date.format() );
	if ( $( '#drop-remove' ).is( ':checked' ) )
	{
		// if so, remove the element from the "Draggable Events" list
		$( this ).remove();
	}
}
EOF;

$JSEventClick = <<<EOF
function( calEvent, jsEvent, view )
{
//	alert( 'Event: ' + calEvent.title );
//	alert( 'Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY );
//	alert( 'View: ' + view.name );
	// change the border color just for fun
	$( this ).css( 'border-color', 'red' );
}
EOF;
?>
<script>
function reset()
{
	$.ajax( {
		url: "available?id=<?= $tutorId ?>&reset=true",
		data: "",
		type: "POST",
		success: function ( response ) {
			window.location.href = "available?id=<?= $tutorId ?>";
		}
	} );
}

function update()
{
	$( '#result' ).html( '' );

	var events = $( '#w0' ).fullCalendar( 'clientEvents' );
	var arr = {};

	for ( var i = 0; i < events.length; i++ )
	{
		arr[ i ] = { start: events[ i ].start.format(), end: events[ i ].end.format() };
	}

	$.ajax( {
		url: "available?id=<?= $tutorId ?>",
		data: arr,
		type: "POST",
		success: function ( response ) {
			$( '#result' ).html( '<p style="color : green">Updated Available Times Successfully</p>' );
		},
		error: function ( e ) {
			$( '#result' ).html( "Updated Error - " + e.responseText );
		}
	} );
}
</script>

	<h1><?= Html::encode( $this->title ) ?></h1>
	<?php if ( $curUser->isTutor() ) : ?>
		<?= Html::button( 'Update', [ 'onclick' => 'javascript:update()' ] ) ?>
		<?= Html::button( 'Reset', [ 'onclick' => 'javascript:reset()' ] ) ?>
	<?php endif; ?>
	<br />
	<div id="result"></div>
	<br />
	<?= Html::hiddenInput( 'tutorId', $tutorId ) ?>
	<?php
		if ( $curUser->isAdmin() )
		{
			echo yii2fullcalendar\yii2fullcalendar::widget( [
//				'ajaxEvents' => Url::to( [ 'jsoncalendar' ] ),
				'events' => $events,
				'clientOptions' => [
//					'selectable' => true,
//					'selectHelper' => true,
//					'droppable' => true,
//					'editable' => true,
//					'drop' => new JSExpression( $JSDropEvent ),
//					'select' => new JSExpression( $JSCode ),
//					'eventClick' => new JSExpression( $JSEventClick ),
					'defaultDate' => date( 'Y-m-d' ),
					'defaultView' => 'agendaWeek',
					'availableViews' => [ 'agendaWeek' ],
					'allDaySlot' => false,
					'slotDuration' => '00:15:00',
					'snapDuration' => '00:30:00',
//					'selectOverlap' => false,
//					'eventOverlap' => false,
					'header' => [
						'left' => '',
						'center' => '',
						'right' => ''
					],
					'timezone' => $tutor->timezone
				]
			] );
		}
		else
		{
			echo yii2fullcalendar\yii2fullcalendar::widget( [
//				'ajaxEvents' => Url::to( [ 'jsoncalendar' ] ),
				'events' => $events,
				'clientOptions' => [
					'selectable' => true,
					'selectHelper' => true,
					'droppable' => true,
					'editable' => true,
					'drop' => new JSExpression( $JSDropEvent ),
					'select' => new JSExpression( $JSCode ),
					'eventClick' => new JSExpression( $JSEventClick ),
					'defaultDate' => date( 'Y-m-d' ),
					'defaultView' => 'agendaWeek',
					'availableViews' => [ 'agendaWeek' ],
					'allDaySlot' => false,
					'slotDuration' => '00:15:00',
					'snapDuration' => '00:30:00',
					'selectOverlap' => false,
					'eventOverlap' => false,
					'header' => [
						'left' => '',
						'center' => '',
						'right' => ''
					],
					'timezone' => 'local'
				]
			] );
		}
	?>
</div>

