<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\User;
use app\models\Timezone;
use app\models\Phone;
use app\models\VideoChat;

/* @var $this yii\web\View */
/* @var $model app\models\Tutor */
/* @var $user app\models\User */
/* @var @phone app\models\Phone */				// NULL if Update
/* @var @videoChat app\models\VideoChat */	// NULL if update
/* @var $form yii\widgets\ActiveForm */

$curUser = User::findOne( Yii::$app->user->id );
?>

<div class="tutor-form">

	<?php $form = ActiveForm::begin( [ 'options' => [ 'enctype' => 'multipart/form-data' ] ] ); ?>

		<?= $form->field( $user, 'name' )->textInput() ?>

		<?= $form->field( $user, 'email' )->textInput() ?>

		<?= $form->field( $user, 'username' )->textInput() ?>

		<?= $form->field( $user, 'password' )->passwordInput() ?>

		<!-- if we are an admin creating a new record -->
		<?php if ( $model->isNewRecord ) : ?>
			<div class="row">
				<div class="col-lg-2">
					<?= $form->field( $phone, 'type' )->dropDownList( Phone::$types ) ?>
				</div>
				<div class="col-lg-2">
					<?= $form->field( $phone, 'countryCode' )->textInput( [ 'maxlength' => true ] ) ?>
				</div>
				<div class="col-lg-4">
					<?= $form->field( $phone, 'number' )->textInput() ?>
				</div>
			</div>
		<!-- else we are either an admin or the tutor himself  -->
		<?php else : ?>
			<?= $user->getPhonesHtml( Yii::$app->request->url ) ?>
		<?php endif; ?>		

		<?= $form->field( $model, 'imageFile' )->fileInput() ?>
		<?php
		// if the photo has a value, let's display it
		if ( !empty( $model->photo ) )
		{
			echo '<img src="data:image;base64,' . base64_encode( $model->photo ).'" height="200" />';
		} //if ( !empty( $model->photo ) )
		?>

		<?= $form->field( $model, 'bio' )->textArea( [ 'maxlength' => true ] ) ?>

		<?= $form->field( $model, 'address' )->textArea( [ 'maxlength' => true ] ) ?>

		<!-- if we are an admin creating a new record -->
		<?php if ( $model->isNewRecord ) : ?>
			<div class="row">
				<div class="col-lg-2">
					<?= $form->field( $videoChat, 'type' )->dropDownList( VideoChat::$types ) ?>
				</div>
				<div class="col-lg-2">
					<?= $form->field( $videoChat, 'accountId' )->textInput() ?>
				</div>
			</div>
		<!-- else we are either an admin or the tutor himself  -->
		<?php else : ?>
			<?= $user->getVideoChatsHtml( Yii::$app->request->url ) ?>
		<?php endif; ?>		

		<?= $form->field( $user, 'timezone' )->dropDownList( Timezone::$timezones ) ?>

		<?= $form->field( $user, 'notes' )->textArea() ?>

		<!-- if the current user is an admin -->
		<?php if ( $curUser->isAdmin() ) : ?>
			<?= $form->field( $user, 'notesPrivate' )->textArea() ?>
			<?= $form->field( $model, 'canSetOwnPrice' )->checkBox() ?>
			<?= $form->field( $user, 'status' )->dropDownList( [ User::STATUS_ACTIVE => 'Active', User::STATUS_INACTIVE => 'Inactive' ] ) ?>
		<?php endif; ?>

		<!-- if the current user is an admin or it is a tutor can has the ability to set their own price -->
		<?php if ( $curUser->isAdmin() || $model->canSetOwnPrice ) : ?>
			<div class="row">
				<div class="col-lg-3">
					<?= $form->field( $model, 'maxPPMRecurring' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'minPPMRecurring' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'maxDiscountMinsRecurring' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'minDiscountMinsRecurring' )->textInput() ?>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-3">
					<?= $form->field( $model, 'maxPPHFlex' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'minPPHFlex' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'maxDiscountHoursFlex' )->textInput() ?>
				</div>
				<div class="col-lg-3">
					<?= $form->field( $model, 'minDiscountHoursFlex' )->textInput() ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="form-group">
			<?= Html::submitButton( $model->isNewRecord ? Yii::t( 'app', 'Create' ) : Yii::t( 'app', 'Update' ), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
