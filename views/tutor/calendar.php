<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\models\Schedule;
use app\models\helpers\TimeType;
use app\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Schedule' );
$this->params[ 'breadcrumbs' ][] = $this->title;

$DragJS = <<<EOF
/* initialize the external events
-----------------------------------------------------------------*/
$( '#external-events .fc-event' ).each( function() {
	// store data so the calendar knows to render an event upon drop
/*	$( this ).data( 'event', {
		title: $.trim( $( this ).text() ), // use the element's text as the event title
		stick: true, // maintain when user navigates (see docs on the renderEvent method)
		duration: '00:45'
	} ); */
	// make the event draggable using jQuery UI
	$( this ).draggable( {
		zIndex: 999,
		revert: true,      // will cause the event to go back to its
		revertDuration: 0  //  original position after the drag
	} );
} );
EOF;
$this->registerJs( $DragJS );
?>
<div class="customer-schedule">

<?php




$JSOverlap = <<<EOF
function ( event )
{
	return false;
}
EOF;

$JSSelectAllow = <<<EOF
function ( selInfo )
{
	//selInfo.start
	//selInfo.end
	//selInfo.resourceId - if using a Resource View
	return true;
}
EOF;

$JSMyEventsError = <<<EOF
function ()
{
	alert( 'error getting your currently scheduled classes' );
}
EOF;


$JSTutorEventsError = <<<EOF
function ()
{
	alert( 'error getting tutor's currently scheduled classes' );
}
EOF;

$JSEventRender = <<<EOF
function ( event, element )
{
	element.append( "<span class='closeon'>X</span>" );
	element.find( ".closeon" ).click( function() {
		$( '#w0' ).fullCalendar( 'removeEvents', event._id );
		var available = Number( $( '#available' ).val() );
		available++;
		$( '#available' ).val( available );
	} );
}
EOF;

$JSEventClick = <<<EOF
function( calEvent, jsEvent, view )
{
	if ( confirm( 'Would you like to cancel this event?' ) == true )
	{
		$.ajax( {
			url: "cancel",
			data: {
					"id" : event.id,
				},
			type: "POST",
			success: function ( data, status, request ) {
				if ( data.indexOf( 'success' ) < 0 )
				{
					alert( 'problem' );
				}
				else
				{
					$( '#w0' ).fullCalendar( 'unselect' );
				}
			},
			error: function ( e ) {
				$( '#result' ).html( "Update Error - " + e.responseText );
			}
		} );
	}
//	alert( 'Event: ' + calEvent.title );
//	alert( 'Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY );
//	alert( 'View: ' + view.name );
//	// change the border color just for fun
//	$( this ).css( 'border-color', 'red' );
}
EOF;
?>
	<h1><?= Html::encode( $this->title ) ?></h1>
	<br />
	<?= yii2fullcalendar\yii2fullcalendar::widget( [
		'ajaxEvents' => Url::to( [ 'scheduled-events' ] ),
		'clientOptions' => [
			'selectable' => false,
			'selectHelper' => false,
			'droppable' => false,
			'editable' => false,
			'eventStartEditable' => false,
			'eventDurationEditable' => false,
//			'drop' => new JSExpression( $JSDropEvent ),
//			'eventReceive' => new JSExpression( $JSReceiveEvent ),
//			'selectAllow' => new JSExpression( $JSSelectAllow ),
//			'select' => ( $type == TimeType::RECURRING
//								? new JSExpression( $JSSelectRecurring )
//								: ( $type == TimeType::FLEX ? new JSExpression( $JSSelectFlex ) : new JSExpression( $JSSelectFree ) ) ),
//			'eventResize' => new JSExpression( $JSEventResize ),
			'eventRender' => new JSExpression( $JSEventRender ),
			'eventClick' => new JSExpression( $JSEventClick ),
			'defaultDate' => date( 'Y-m-d' ),
			'defaultView' => 'agendaWeek',
			'availableViews' => [ 'agendaWeek' ],
			'allDaySlot' => false,
			'slotDuration' => '00:15:00',
//			'snapDuration' => $type == TimeType::FREE ? '00:30:00' : '01:00:00',
			'eventColor' => 'yellow',
			'eventTextColor' => 'black',
			'selectOverlap' => false,
			'eventOverlap' => false,
			'selectConstraint' => 'businessHours',
			'eventConstraint' => 'businessHours',
			'header' => [
				'left' => 'title',
				'center' => 'agendaWeek',
				'right' => $type == TimeType::RECURRING ? '' : 'prev, next'
			],
			'businessHours' => $tutor->getAvailableTimes( $tutor->user->timezone ),
			'selectConstraint' => 'businessHours',
			'timezone' => $tutor->user->timezone,
		]
	] ) ?>
</div>
