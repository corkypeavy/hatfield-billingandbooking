<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tutor */
/* @var $user app\models\User */
/* @var $phone app\models\Phone */
/* @var $videoChat app\models\VideoChat */

$this->title = Yii::t( 'app', 'Create Tutor' );
$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Tutors' ), 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="tutor-create">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= $this->render( '_form', [ 'model' => $model, 'user' => $user, 'phone' => $phone, 'videoChat' => $videoChat ] ) ?>
</div>
