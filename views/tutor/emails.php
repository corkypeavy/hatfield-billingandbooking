<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use app\models\User;

$this->title = Yii::t( 'app', 'Tutor Emails' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="tutor-emails">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php $form = ActiveForm::begin( [ 'action' => [ 'emails' ], 'method' => 'get' ] ); ?>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'tutor' )->listBox( ArrayHelper::map( User::find()->where( [ 'type' => User::TYPE_TUTOR ] )->all(), 'id', 'name' ), [ 'prompt' => 'All Tutors' ] ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'salesAmt' )->textInput() ?>
				<?= $form->field( $searchModel, 'sales' )->radioList( [ 0 => 'Any', 1 => 'Greater than', 2 => 'Less than' ] ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'startMonths' )->textInput() ?>
				<?= $form->field( $searchModel, 'start' )->radioList( [ 0 => 'Any', 1 => 'Greater than', 2 => 'Less than' ] ) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $searchModel, 'show' )->radioList( [ 0 => 'Name and Email', 1 =>'Email Only' ] ) ?>
			</div>
		</div>
		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Search' ), [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::resetButton( Yii::t( 'app', 'Reset' ), [ 'class' => 'btn btn-default' ] ) ?>
		</div>
	<?php ActiveForm::end(); ?>

	<?= ListView::widget( [
		'dataProvider' => $dataProvider,
		'layout' => '{items}',
		'itemView' => function ( $model, $key, $index, $widget ) {
			if ( $searchModel->show == '0' )
			{
				return $model->user->name . ' &lt;' . $model->user->email . '&gt;, ';
			}
			else
			{
				return $model->user->email . ', ';
			}
		}
	] ) ?>
</div>
