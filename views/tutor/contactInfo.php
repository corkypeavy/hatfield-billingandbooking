<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TutorSearch */
/* @var $userSearch app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Tutors' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="tutor-index">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'columns' => [
				[ 'class' => 'yii\grid\SerialColumn' ],
				'user.name',
				'user.email',
				[ 'label' => 'Phones', 'format' => 'raw', 'value' => function ( $model ) { return $model->user->getPhonesHtml( null, false ); } ],
				//'address',
				'user.timezone',
				[ 'class' => 'yii\grid\ActionColumn' ],
			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
