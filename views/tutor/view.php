<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Tutor */

$this->title = $model->id;

$curUser = User::findOne( Yii::$app->user->id );
if ( $curUser->isAdmin() )
{
	$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Tutors' ), 'url' => [ 'index' ] ];
}
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="tutor-view">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>
		<?= Html::a( Yii::t( 'app', 'Update' ), [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] ) ?>
		<?= Html::a( Yii::t( 'app', 'Available Times' ), [ 'available', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] ) ?>
		<?php
		if ( $model->id != $curUser->id )
		{
			Html::a( Yii::t( 'app', 'Delete' ), [ 'delete', 'id' => $model->id ], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
					'method' => 'post',
				],
			] );
		}
		?>
	</p>

	<?php if ( Yii::$app->session->hasFlash( 'phoneNotDeleted' ) ): ?>
		<p>
			<h3 class="error">Cannot delete the only phone number.</h3>
		</p>
	<?php endif; ?>

	<?= DetailView::widget( [
		'model' => $model,
		'attributes' => [
			'id',
			[ 'attribute' => 'name', 'value' => $model->user->name ],
			[ 'attribute' => 'email', 'value' => $model->user->email ],
			[ 'attribute' => 'username', 'value' => $model->user->username ],
			[ 'label' => 'Phones', 'format' => 'raw', 'value' => $model->user->getPhonesHtml( Yii::$app->request->url ) ],
			'bio',
			'address',
			[ 'label' => 'Video Chats', 'format' => 'raw', 'value' => $model->user->getVideoChatsHtml( Yii::$app->request->url ) ],
			[ 'label' => 'Timezone', 'value' => $model->user->timezone ],
			[
				'attribute' => 'photo',
				'format' => 'raw',
				'value' => ( empty( $model->photo ) ? '' : '<img src="data:image;base64,' . base64_encode( $model->photo ).'" height="200" />' )
			],
			[ 'label' => 'Notes', 'value' => $model->user->notes ],
			[ 'label' => 'Private Notes', 'visible' => $curUser->isAdmin(), 'value' => $model->user->notesPrivate ],
			[ 'attribute' => 'canSetOwnPrice', 'visible' => $curUser->isAdmin(), 'value' => $model->canSetOwnPrice ? 'Yes' : 'No' ],
		],
	] ) ?>
</div>
