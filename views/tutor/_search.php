<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TutorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tutor-search">

	<?php $form = ActiveForm::begin( [ 'action' => [ 'index' ], 'method' => 'get' ] ); ?>

		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $model, 'name' ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $model, 'email' ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $model, 'username' ) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field( $model, 'phone' ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $model, 'status' ) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field( $model, 'timezone' ) ?>
			</div>
		</div>
		<div class="form-group">
			<?= Html::submitButton( Yii::t( 'app', 'Search' ), [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::resetButton( Yii::t( 'app', 'Reset' ), [ 'class' => 'btn btn-default' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
