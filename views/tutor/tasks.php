<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\AvailableTime;

/* @var $this yii\web\View */

$this->title = 'Booking and Billing';
?>
<div class="site-index">

	<div class="jumbotron">
		<h1>Welcome, <?= $model->user->name ?></h1>
	</div>

	<div class="body-content">
		<div class="row">
			<div class="col-lg-4">
				<?php if ( !AvailableTime::find()->where( [ 'tutorId' => $model->id ] )->exists() ) : ?>
					<p><?= Html::a( "No Available Time is currently set.  Set Now", Url::to( [ 'tutor/available' ] ) ) ?></p>
				<?php endif; ?>
				<p><?= Html::a( "Calendar", Url::to( [ 'tutor/calendar', 'id' => $model->id ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Invoices and Payments", Url::to( [ 'purchase/index', 'tutorId' => $model->id ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Student Email Addresses", Url::to( [ 'customer/emails' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "Student Accounts", Url::to( [ 'customer/index' ] ), [ "class" => "btn btn-default" ] ) ?></p>
				<p><?= Html::a( "My Account", Url::to( [ 'tutor/view', 'id' => $model->id ] ), [ "class" => "btn btn-default" ] ) ?></p>
			</div>
		</div>
	</div>
</div>
