<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ReferrerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Referrers' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="referrer-index">
	<h1><?= Html::encode( $this->title ) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a( Yii::t( 'app', 'Create Referrer' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
	</p>
	<?php Pjax::begin(); ?>
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			//'filterModel' => $searchModel,
			'columns' => [
				[ 'class' => 'yii\grid\SerialColumn' ],
				'name',
				[ 'attribute' => 'canShow', 'value' => function ( $model ) { return $model->canShow ? 'Yes' : 'No'; } ],
				[ 'class' => 'yii\grid\ActionColumn' ],
			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
