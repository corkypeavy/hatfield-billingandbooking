<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Referrer */

$this->title = Yii::t( 'app', 'Create Referrer' );
$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Referrers' ), 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="referrer-create">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= $this->render( '_form', [ 'model' => $model ] ) ?>
</div>
