<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tutor */

$this->title = Yii::t( 'app', 'Create Admin User');
$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Admin Accounts' ), 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="user-create">
    <h1><?= Html::encode( $this->title ) ?></h1>

    <?= $this->render( '_form', [ 'model' => $model ] ) ?>
</div>
