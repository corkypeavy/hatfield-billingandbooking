<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t( 'app', 'Admin Accounts' );
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="user-index">

	<h1><?= Html::encode( $this->title ) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a( Yii::t( 'app', 'Create Admin User' ), [ 'user/create' ], [ 'class' => 'btn btn-success' ] ) ?>
	</p>
	<?php Pjax::begin(); ?>
		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
//				'id',
				'username',
				'name',
				'email',
				[ 'attribute' => 'type', 'value' => function ( $model ) { return $model->typeString(); } ],
				[ 'attribute' => 'status', 'value' => function ( $model ) { return $model->statusString(); } ],
				[
					'class' => 'yii\grid\ActionColumn',
					'buttons' => [
						'update' => function ( $url, $model ) {
							return $model->status != User::STATUS_DELETED && ( !$model->isSuperAdmin() || $model->id == Yii::$app->user->id ) ?  Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t( 'app', 'Update' ),
							] ) : '';
						},
						'delete' => function ( $url, $model ) {
							return !$model->isSuperAdmin() && $model->status != User::STATUS_DELETED ? Html::a( '<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t( 'app', 'Delete' ),
							] ) : '';
						},
					],
				],
			],
		] ); ?>
	<?php Pjax::end(); ?>
</div>
