<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$curUser = User::findOne( Yii::$app->user->id );
?>

<div class="user-form">
	<?php $form = ActiveForm::begin(); ?>

		<?php //= $form->field( $model, 'id' )->label() ?>

		<?= $form->field( $model, 'username' )->textInput() ?>

		<?= $form->field( $model, 'name' )->textInput() ?>

		<?= $form->field( $model, 'email' )->textInput() ?>

		<?= $form->field( $model, 'password' )->passwordInput() ?>

		<!-- if the user is a super admin, he can upgrade an admin to super admin -->
		<?php if ( $model->type == User::TYPE_ADMIN ) : ?>
			<?= $form->field( $model, 'type' )->dropDownList( [ User::TYPE_SUPER_ADMIN => 'Super Admin', User::TYPE_ADMIN => 'Admin' ] ) ?>
		<?php endif; ?>
		
		<?php
		if ( !$model->isNewRecord && $curUser->type == TYPE_SUPER_ADMIN )
		{
			echo $form->field( $model, 'status' )->dropDownList( [ User::STATUS_ACTIVE => 'Active', User::STATUS_INACTIVE => 'Inactive' ] );
		}
		?>
		<div class="form-group">
			<?= Html::submitButton( $model->isNewRecord ? Yii::t( 'app', 'Create' ) : Yii::t( 'app', 'Update' ), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
		</div>

	<?php ActiveForm::end(); ?>
</div>
