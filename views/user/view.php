<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$curUser = User::findOne( Yii::$app->user->id );
if ( Yii::$app->user-id != $model->id )
{
	$this->params[ 'breadcrumbs' ][] = [ 'label' => Yii::t( 'app', 'Admin Accounts' ), 'url' => [ 'index' ] ];
}
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="user-view">

	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>
		<?php
			if ( $model->id == Yii::$app->user->id || ( $curUser->isSuperAdmin() && !$model->isSuperAdmin() ) )
			{
				echo Html::a( Yii::t( 'app', 'Update' ), [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ] );
			}
			if ( $model->id != Yii::$app->user->id && $model->type != User::TYPE_SUPER_ADMIN )
			{
				echo Html::a( Yii::t( 'app', 'Delete' ), [ 'delete', 'id' => $model->id ], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t( 'app', 'Are you sure you want to delete this item?' ),
						'method' => 'post',
					],
				] );
			}
		?>
    </p>

    <?= DetailView::widget( [
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'name',
            'email',
            [ 'label' => 'Status', 'value' => $model->statusString() ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ] ) ?>

</div>
