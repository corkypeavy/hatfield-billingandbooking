<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property integer $customerId
 * @property integer $tutorId
 * @property integer $type
 * @property integer $quantity
 * @property string $price
 * @property integer $status
 * @property integer $paymentId
 * @property integer $subscriptionId
 * @property string $description
 *
 * @property Customer $customer
 * @property Payment $payment
 * @property Subscription $subscription
 * @property Tutor $tutor
 */
class Invoice extends \yii\db\ActiveRecord
{
	const STATUS_UNPAID = 0;
	const STATUS_FREE = 2;
	const STATUS_CANCELLED = 5;
	const STATUS_DELETED = 7;
	const STATUS_PAID = 10;

	public function statusString()
	{
		switch ( $this->status )
		{
			case self::STATUS_UNPAID:
				return 'Unpaid';

			case self::STATUS_FREE:
				return 'Free';

			case self::STATUS_CANCELLED:
				return 'Cancelled';

			case self::STATUS_DELETED:
				return 'Deleted';

			case self::STATUS_PAID:
				return 'Paid';

			default:
				return 'Invalid';
		}
	}

	const TYPE_RECURRING = 0;
	const TYPE_FLEX = 1;
	const TYPE_OTHER = 2;

	public function typeString()
	{
		switch ( $this->type )
		{
			case self::TYPE_RECURRING:
				return 'Recurring';

			case self::TYPE_FLEX:
				return 'Flex';

			case self::TYPE_OTHER:
				return 'Other';

			default:
				return 'Invalid';
		}
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'invoice';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'customerId', 'tutorId', 'type', 'quantity', 'price', 'status', 'description' ], 'required' ],
			[ [ 'customerId', 'tutorId', 'type', 'status', 'paymentId', 'subscriptionId' ], 'integer' ],
			[ [ 'price', 'quantity' ], 'number' ],
			[ [ 'description' ], 'string', 'max' => 250 ],
			[ [ 'customerId' ], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => [ 'customerId' => 'id' ] ],
			[ [ 'paymentId' ], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => [ 'paymentId' => 'id' ] ],
			[ [ 'subscriptionId' ], 'exist', 'skipOnError' => true, 'targetClass' => Subscription::className(), 'targetAttribute' => [ 'subscriptionId' => 'id' ] ],
			[ [ 'tutorId' ], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => [ 'tutorId' => 'id' ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID'),
			'createdDate' => Yii::t( 'app', 'Created Date' ),
			'customerId' => Yii::t( 'app', 'Customer ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'quantity' => Yii::t( 'app', 'Quantity' ),
			'price' => Yii::t( 'app', 'Price' ),
			'status' => Yii::t( 'app', 'Status' ),
			'paymentId' => Yii::t( 'app', 'Payment ID' ),
			'subscriptionId' => Yii::t( 'app', 'Subscription ID' ),
			'description' => Yii::t( 'app', 'Description' ),
		];
	}

	public function statusHtml()
	{
		switch ( $this->status )
		{
			case self::STATUS_UNPAID:
				return '<span style="color:red">Unpaid</span>';

			case self::STATUS_FREE:
				return 'Free';

			case self::STATUS_PAID:
				return Html::a( 'Paid', '#', [
					'value' => Url::to( [ 'view-payment', 'paymentId' => $this->paymentId ] ),
					'title' => 'Payment',
					'class' => 'showModalButton'
				] );

			case self::STATUS_DELETED:
				return 'Deleted';

			case self::STATUS_CANCELLED:
				return 'Cancelled';

			default:
				return 'Invalid';
		}
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne( Customer::className(), [ 'id' => 'customerId' ] );
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPayment()
	{
		return $this->hasOne( Payment::className(), [ 'id' => 'paymentId' ] );
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSubscription()
	{
		return $this->hasOne( Subscription::className(), [ 'id' => 'subscriptionId' ] );
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTutor()
	{
		return $this->hasOne( Tutor::className(), [ 'id' => 'tutorId' ] );
	}

	/**
	 * @inheritdoc
	 * @return InvoiceQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new InvoiceQuery( get_called_class() );
	}
}
