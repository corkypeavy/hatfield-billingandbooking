<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videoChat".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $type
 * @property string $accountId
 * @property integer $isPrimary
 */
class VideoChat extends \yii\db\ActiveRecord
{
	public static $types = [
		'Skype' => 'Skype',
		'ooVoo' => 'ooVoo',
		'FaceTime' => 'FaceTime'
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'videoChat';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'userId', 'accountId' ], 'required' ],
			[ [ 'userId', 'isPrimary' ], 'integer' ],
			[ [ 'type' ], 'string', 'max' => 20 ],
			[ [ 'accountId' ], 'string', 'max' => 50 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'userId' => Yii::t( 'app', 'User ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'accountId' => Yii::t( 'app', 'Account ID' ),
			'isPrimary' => Yii::t( 'app', 'Is Primary' ),
		];
	}

	/**
	 * @inheritdoc
	 * @return VideoChatQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new VideoChatQuery( get_called_class() );
	}
}
