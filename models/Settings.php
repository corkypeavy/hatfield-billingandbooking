<?php

namespace app\models;

use Yii;

use yii\helpers\VarDumper;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $value
 */
class Settings extends \yii\db\ActiveRecord
{
	private static $_settings = null;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'settings';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'name', 'value' ], 'required' ],
			[ [ 'type' ], 'string', 'max' => 20 ],
			[ [ 'name' ], 'string', 'max' => 50 ],
			[ [ 'value' ], 'string', 'max' => 250 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'name' => Yii::t( 'app', 'Name' ),
			'type' => Yii::t( 'app', 'Type' ),
			'value' => Yii::t( 'app', 'Value' ),
		];
	}

	/**
	 * @inheritdoc
	 * @return SettingsQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new SettingsQuery( get_called_class() );
	}

	protected static function loadSettings()
	{
		static::$_settings = array();

		$rows = static::find()->all();

		foreach ( $rows as $row )
		{
			switch ( $row->type )
			{
				case 'Number':
					static::$_settings[ $row->code ] = ( float )$row->value;
					break;

				case 'Boolean':
					static::$_settings[ $row->code ] = ( strcasecmp( $row->value, 'false' ) == 0 || $row->value == 0 ) ? false : true;
					break;

				default: // Text or other
					static::$_settings[ $row->code ] = $row->value;
					break;
			}
		}
	}

	public static function getNearestHoursToChange()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'NearestHoursToChange' ];
	}

	public static function getFutureBookingLimit()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'FutureBookingLimit' ];
	}

	public static function getStoreName()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'StoreName' ];
	}

	public static function getTimezone()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'Timezone' ];
	}

	public static function getMaxPauseDays()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'MaxPauseDays' ];
	}

	public static function getDelayBeforeChangeNotice()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'DelayBeforeChangeNotice' ];
	}

	public static function getMakeupAllowance()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'MakeupAllowance' ];
	}

	public static function getCronJobMinutes()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'CronJobMinutes' ];
	}

	public static function getHighestPricePerMinuteRecurring()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'HighestPricePerMinuteRecurring' ];
	}

	public static function getLowestPricePerMinuteRecurring()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'LowestPricePerMinuteRecurring' ];
	}

	public static function getMinimumMinutesForDiscountsRecurring()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'MinimumMinutesForDiscountsRecurring' ];
	}

	public static function getMaximumMinutesForDiscountsRecurring()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'MaximumMinutesForDiscountsRecurring' ];
	}

	public static function getHighestPricePerHourFlex()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'HighestPricePerHourFlex' ];
	}

	public static function getLowestPricePerHourFlex()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'LowestPricePerHourFlex' ];
	}

	public static function getMinimumHoursForDiscountsFlex()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'MinimumHoursForDiscountsFlex' ];
}

	public static function getMaximumHoursForDiscountsFlex()
	{
		if ( !isset( static::$_settings ) )
		{
			static::loadSettings();
		}

		return static::$_settings[ 'MaximumHoursForDiscountsFlex' ];
	}
}
