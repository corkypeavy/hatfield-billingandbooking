<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Invoice;

use yii\helpers\VarDumper;

/**
 * InvoiceSearch represents the model behind the search form about `app\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
	public $dateRange = 0;
	public $start;
	public $end;

	protected function setDateValues()
	{
		switch ( $this->dateRange )
		{
			case 0:
				$date = new \DateTime();
				$year = $date->format( 'Y' );
				$month = $date->format( 'm' );

				// if the month is january, we also need to change the year
				if ( $month == 1 )
				{
					$year = intval( $year ) - 1;
					$month = 12;
				} // if ( intval( $month ) === 1 )

				// else we just need to subract 1
				else
				{
					$month--;
				} // else

				// set to the first day of previous month
				$date->setDate( $year, $month, 1 );
				$this->start = $date;

				// this returns the last day of the month for the given month
				$lastDay = $date->format( 't' );

				$date = new \DateTime();
				$date->setDate( $year, $month, $lastDay );
				$this->end = $date;		
				break;

			case 1:
				$date = new \DateTime();
				$year = $date->format( 'Y' );
				$month = $date->format( 'm' );

				// set to the first day of previous month
				$date->setDate( $year, $month, 1 );
				$this->start = $date;

				// this returns the last day of the month for the given month
				$lastDay = $date->format( 't' );

				$date = new \DateTime();
				$date->setDate( $year, $month, $lastDay );
				$this->end = $date;		
				break;

			case 2:
				$this->start = new \DateTime( $this->start );
				$this->end = new \DateTime( $this->end );
				break;
			
			default:
				throw new Exception( 'Invalid Date Range' );
				break;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'id', 'customerId', 'tutorId', 'type', 'status', 'paymentId', 'subscriptionId', 'dateRange' ], 'integer' ],
			[ [ 'price', 'quantity' ], 'number' ],
			[ [ 'description', 'start', 'end' ], 'safe' ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function searchUnpaid( $custId )
	{
		$query = Invoice::find()->where( [ 'customerId' => $custId, 'status' => Invoice::STATUS_UNPAID ] );
		return new ActiveDataProvider( [ 'query' => $query ] );
	}

	public function searchHistory( $custId )
	{
		$query = Invoice::find()->where( [ 'customerId' => $custId ] );
		return new ActiveDataProvider( [ 'query' => $query ] );
	}

	public function searchInvoices( $params )
	{
		$query = Invoice::find();
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );
		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere( [
			'customerId' => $this->customerId,
			'tutorId' => $this->tutorId
		] );

		$this->setDateValues();

		$query->andFilterWhere( [
			'between', 'createdDate', $this->start->format( 'Y-m-d' ), $this->end->format( 'Y-m-d' )
		] );

		return $dataProvider;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params )
	{
		$query = Invoice::find();

		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere( [
			'id' => $this->id,
			'customerId' => $this->customerId,
			'tutorId' => $this->tutorId,
			'type' => $this->type,
			'quantity' => $this->quantity,
			'price' => $this->price,
			'status' => $this->status,
			'paymentId' => $this->paymentId,
			'subscriptionId' => $this->subscriptionId,
		] );

		$query->andFilterWhere( [ 'like', 'description', $this->description ] );

		return $dataProvider;
	}
}
