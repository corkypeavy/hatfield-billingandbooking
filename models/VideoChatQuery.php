<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VideoChat]].
 *
 * @see VideoChat
 */
class VideoChatQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return VideoChat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VideoChat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
