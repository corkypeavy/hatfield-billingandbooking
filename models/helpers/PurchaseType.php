<?php

namespace app\models\helpers;

use Yii;

class PurchaseType
{
	const RECURRING = 0;
	const FLEX = 1;
	const OTHER = 2;
	const FREE = 3;

	public static function toString( $val )
	{
		switch ( $val )
		{
			case self::RECURRING:
				return 'Recurring';

			case self::FLEX:
				return 'Flex Time';

			case self::FREE:
				return 'Free Trial';

			case self::OTHER:
				return 'Custom';

			default:
				return 'Invalid';
		}
	}
}