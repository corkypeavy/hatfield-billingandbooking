<?php

namespace app\models\helpers;

use Yii;

class TimeType
{
	const RECURRING = 0;
	const FLEX = 1;
	const FREE = 2;
	const MAKEUP = 3;

	public static function toString( $val )
	{
		switch ( $val )
		{
			case self::RECURRING:
				return 'Recurring';

			case self::FLEX:
				return 'Flex Time';

			case self::FREE:
				return 'Free Time';

			case self::MAKEUP:
				return 'Makeup Time';

			default:
				return 'Invalid';
		}
	}
}