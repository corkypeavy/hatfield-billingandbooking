<?php

namespace app\models\helpers;

use Yii;

class PurchaseStatus
{
	const UNPAID = 0;
	const FREE = 2;
	const CANCELLED = 5;
	const DELETED = 7;
	const PAID = 10;

	public static function toString( $val )
	{
		switch ( $val )
		{
			case self::UNPAID:
				return 'Recurring';

			case self::FREE:
				return 'Free';

			case self::CANCELLED:
				return 'Cancelled';

			case self::DELETED:
				return 'Deleted';

			case self::PAID:
				return 'Paid';

			default:
				return 'Invalid';
		}
	}
}