<?php
namespace app\models\helpers;

use Yii;
use \yii2fullcalendar\models\Event;

class ScheduleEvent extends Event
{
	// holds the subscription id for recurring events
	public $subId;

	// this attribute is set to true if it has been retrieved from the DB
	public $alreadyScheduled;
}
