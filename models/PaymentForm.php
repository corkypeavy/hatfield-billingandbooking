<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "subscription".
 *
 * @property integer amount
 * @property string name
 * @property string $number
 * @property string $ccv
 * @property integer expirationMonth
 * @property integer expirationYear
 *
 */
class PaymentForm extends Model
{
	public $amount;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'amount' ], 'required' ],
			[ [ 'amount' ], 'number' ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'amount' => Yii::t( 'app', 'Amount' ),
		];
	}
}
