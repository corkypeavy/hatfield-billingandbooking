<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;
use app\models\Purchase;
use app\models\Phone;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerSearch extends Customer
{
	public $customer = '';
	public $recentMonths = 1;
	public $show = 0;
	public $who = 0;
	public $tutor = '';
	public $phone;
	public $timezone;
	public $name;
	public $username;
	public $email;
	public $status;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'id', 'emailConfirmed', 'recentMonths' ], 'integer' ],
			[ [ 'howFound', 'howFoundOther', 'customer', 'who', 'tutor', 'phone', 'timezone', 'name', 'username', 'email', 'status' ], 'safe' ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function searchEmail( $params )
	{
		$query = Customer::find()->joinWith( 'user' );
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );
		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		if ( !empty( $this->customer ) )
		{
			$query->where( [ 'customer.id' => $this->customer ] );
			return $dataProvider;
		}

		$date = new \DateTime();
		$monthsAgo = new \DateInterval( 'P' . $this->recentMonths . 'M' );
		$recentDate = $date->sub( $monthsAgo );

		switch ( $this->who )
		{

			case 0: // 'Have ever made a purchase' ('from you' if user is a tutor)
				$query->andFilterWhere( [ 'in', 'user.id', Purchase::find()->select( 'customerId' ) ] );
				break;

			case 1: // 'Have purchased recently' ('from you' if user is a tutor)
				$query->andFilterWhere( [ 'in', 'user.id', Purchase::find()->select( 'customerId' )->where( [ '>', 'createdDate', $recentDate->format( 'Y-m-d' ) ] ) ] );
				break;

			case 2: // 'Signed up but never purchased',
				$query->andFilterWhere( [ 'not in', 'user.id', Purchase::find()->select( 'customerId' ) ]  );
				break;

			case 3: // 'Signed up recently but never purchased',
				$query->andFilterWhere( [ 'not in', 'user.id', Purchase::find()->select( 'customerId' ) ] );
				$query->andFilterWhere( [ '>', 'user.created_at', strtotime( $recentDate->format( 'Y-m-d H:i:s' ) ) ] );
				break;

			case 4: // 'All who have signed up',
				// nothing to add (all active customers will be included)
				break;

			case 5: // 'All who have signed up recently'
				$query->andFilterWhere( [ '>' , 'user.created_at', strtotime( $recentDate->format( 'Y-m-d H:i:s' ) ) ] );
				break;

			default:
				// do nothing
				break;
		}

		$curUser = User::findOne( Yii::$app->user->id );

		// if the current user is a tutor then all searches should include this tutor
		if ( $curUser->isTutor() )
		{
			$this->tutor = $curUser->id;
		} // if ( $curUser->isTutor() )

		// if a tutor is selected
		if ( !empty( $this->tutor ) )
		{
			$query->andFilterWhere( [ 'in', 'user.id', Purchase::find()->joinWith( 'tutor' )->select( 'purchase.customerId' )->where( [ 'tutorId' => $this->tutor ] ) ] );
		} // if ( !empty( $this->tutor ) )

		return $dataProvider;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params )
	{
		$query = Customer::find()->joinWith( 'user' );

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere( [
			'id' => $this->id,
//			'emailConfirmed' => $this->emailConfirmed,
			'user.status' => $this->getStatusFromText( $this->status )
		] );

		$query->andFilterWhere( [ 'like', 'howFound', $this->howFound ] )
//			->andFilterWhere( [ 'like', 'howFoundOther', $this->howFoundOther ] )
			->andFilterWhere( [ 'like', 'user.name', $this->name ] )
			->andFilterWhere( [ 'like', 'user.timezone', $this->timezone ] )
			->andFilterWhere( [ 'like', 'user.username', $this->username ] )
			->andFilterWhere( [ 'like', 'user.email', $this->email ] );

		if ( !empty( $this->phone ) )
		{
			$query->andFilterWhere( [ 'in', 'user.id', Phone::find()->select( 'userId as id' )->where( [ 'like', 'number', $this->phone ] ) ] );
		}

		return $dataProvider;
	}

	protected function getStatusFromText( $status )
	{
		if ( !empty( $status ) )
		{
			if ( strncasecmp( $status, "Active", strlen( $status ) ) == 0 )
			{
				return User::STATUS_ACTIVE;
			}
			else if ( strncasecmp( $status, "Inactive", strlen( $status ) ) == 0 )
			{
				return User::STATUS_INACTIVE;
			}
			else if ( strncasecmp( $status, "Deleted", strlen( $status ) ) == 0 )
			{
				return User::STATUS_DELETED;
			}
		}
	}
}
