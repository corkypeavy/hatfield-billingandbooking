<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\helpers\PurchaseStatus;
use app\models\helpers\PurchaseType;

/**
 * This is the model class for table "purchase".
 *
 * @property integer $id
 * @property integer $customerId
 * @property integer $itemId
 * @property integer $tutorId
 * @property string $quantity
 * @property string $totalPrice
 * @property string $dateCreated
 * @property string $datePaid
 * @property string $notes
 * @property integer $status
 * @property string $dunningStepComplete
 * @property integer $hasViewed
 */
class Purchase extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'purchase';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'customerId', 'quantity', 'totalPrice', 'createdDttmUTC', 'notes', 'type' ], 'required' ],
			[ [ 'customerId', 'productId', 'tutorId', 'quantity', 'dunningStepComplete', 'hasViewed', 'status', 'type', 'paymentId' ], 'integer' ],
			[ [ 'totalPrice' ], 'number' ],
			[ [ 'createdDttmUTC', 'paidDttmUTC' ], 'safe' ],
			[ [ 'notes' ], 'string', 'max' => 250 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'customerId' => Yii::t( 'app', 'Customer ID' ),
			'itemId' => Yii::t( 'app', 'Item ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'quantity' => Yii::t( 'app', 'Quantity' ),
			'totalPrice' => Yii::t( 'app', 'Total Price' ),
			'createdDttmUTC' => Yii::t( 'app', 'Date Created' ),
			'paidDttmUTC' => Yii::t( 'app', 'Date Paid' ),
			'notes' => Yii::t( 'app', 'Notes' ),
			'status' => Yii::t( 'app', 'Status' ),
			'dunningStepComplete' => Yii::t( 'app', 'Dunning Step Complete' ),
			'hasViewed' => Yii::t( 'app', 'Has Viewed' ),
			'type' => Yii::t( 'app', 'Type' ),
			'paymentId' => Yii::t( 'app', 'Payment ID' ),
		];
	}

	public function statusHtml()
	{
		switch ( $this->status )
		{
			case PurchaseStatus::UNPAID:
				return '<span style="color:red">Unpaid</span>';

			case PurchaseStatus::FREE:
				return 'Free';

			case PurchaseStatus::PAID:
				return Html::a( 'Paid', '#', [
					'value' => Url::to( [ 'view-payment', 'paymentId' => $this->paymentId ] ),
					'title' => 'Payment',
					'class' => 'showModalButton'
				] );

			case PurchaseStatus::DELETED:
				return 'Deleted';

			case PurchaseStatus::CANCELLED:
				return 'Cancelled';

			default:
				return 'Invalid';
		}
	}

	public function statusString()
	{
		return PurchaseStatus::toString( $this->status );
	}

	public function typeString()
	{
		return PurchaseType::toString( $this->type );
	}

	/**
	 * returns the customer associated with this purchase
	 */
	public function getCustomer()
	{
		return $this->hasOne( Customer::classname(), [ 'id' => 'customerId' ] );
	}

	/**
	 * returns the customer associated with this purchase
	 */
	public function getTutor()
	{
		return $this->hasOne( Tutor::classname(), [ 'id' => 'tutorId' ] );
	}

	/**
	 * Returns the product record associated with this purchase
	 */
	public function getProduct()
	{
		return $this->hasOne( Product::classname(), [ 'id' => 'productId' ] );
	}

	/**
	 * @inheritdoc
	 * @return PurchaseQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new PurchaseQuery( get_called_class() );
	}
}
