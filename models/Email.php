<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $type
 * @property integer $address
 */
class Email extends \yii\db\ActiveRecord
{
	public static $types = [
		'Personal' => 'Personal',
		'Work' => 'Work',
		'Other' => 'Other'
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'email';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'userId', 'type', 'address' ], 'required' ],
			[ [ 'userId' ], 'integer' ],
			[ [ 'type' ], 'string', 'max' => 15 ],
			[ [ 'address' ], 'string', 'max' => 255 ],
			[ [ 'address' ], 'email' ]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'userId' => Yii::t( 'app', 'User ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'address' => Yii::t( 'app', 'Address' ),
		];
	}

	/**
	 * @inheritdoc
	 * @return EmailQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new EmailQuery( get_called_class() );
	}
}
