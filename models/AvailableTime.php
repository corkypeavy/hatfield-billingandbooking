<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "availableTime".
 *
 * @property integer $id
 * @property integer $tutorId
 * @property integer $dayOfWeek
 * @property string $startTime
 * @property string $endTime
 *
 * @property Tutor $tutor
 */
class AvailableTime extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'availableTime';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'tutorId', 'dayOfWeek', 'startTime', 'endTime' ], 'required' ],
			[ [ 'tutorId', 'dayOfWeek' ], 'integer' ],
			[ [ 'startTime', 'endTime' ], 'safe' ],
			[ [ 'tutorId' ], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => [ 'tutorId' => 'id' ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'dayOfWeek' => Yii::t( 'app', 'Day Of Week' ),
			'startTime' => Yii::t( 'app', 'Start Time' ),
			'endTime' => Yii::t( 'app', 'End Time' ),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTutor()
	{
		return $this->hasOne( Tutor::className(), [ 'id' => 'tutorId' ] );
	}

	/**
	 * @inheritdoc
	 * @return AvailableTimeQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new AvailableTimeQuery( get_called_class() );
	}
}
