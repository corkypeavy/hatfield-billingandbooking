<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

use yii\helpers\VarDumper;

/**
 * This is the model class for table "tutor".
 *
 * @property integer $id
 * @property string $bio
 * @property string $address
 * @property resource $photo
 * @property boolean $canSetOwnPrice
 */
class Tutor extends \yii\db\ActiveRecord
{
	/**
	 * @var UploadedFile
	 */
	public $imageFile;


	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return 'tutor';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'id', 'maxDiscountMinsRecurring', 'minDiscountMinsRecurring', 'minDiscountHoursFlex', 'maxDiscountHoursFlex',
				'maxPPMRecurring', 'minPPMRecurring', 'maxPPHFlex', 'maxPPHFlex' ], 'required' ],
			[ [ 'id', 'maxDiscountMinsRecurring', 'minDiscountMinsRecurring', 'maxDiscountHoursFlex', 'minDiscountHoursFlex' ], 'integer' ],
			[ [ 'photo' ], 'safe' ],
			[ [ 'maxPPMRecurring', 'minPPMRecurring', 'maxPPHFlex', 'maxPPHFlex' ], 'number' ],
			[ [ 'bio', 'address' ], 'string', 'max' => 255 ],
			[ [ 'canSetOwnPrice' ], 'safe' ],
			[ [ 'imageFile' ], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif' ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'bio' => Yii::t( 'app', 'Bio' ),
			'address' => Yii::t( 'app', 'Address' ),
			'photo' => Yii::t( 'app', 'Photo' ),
			'canSetOwnPrice' => Yii::t( 'app', 'Can set their own price?' ),
			'imageFile' => Yii::t( 'app', 'Photo' ),
			'maxPPMRecurring' => Yii::t( 'app', 'Maximum Price/Min (Recurring)' ),
			'minPPMRecurring' => Yii::t( 'app', 'Minimum Price/Min (Recuring)' ),
			'maxDiscountMinsRecurring' => Yii::t( 'app', 'Maximum Minutes for Discounts (Recurring)' ),
			'minDiscountMinsRecurring' => Yii::t( 'app', 'Minimum Minutes for Discounts (Recurring)' ),
			'maxPPHFlex' => Yii::t( 'app', 'Maximum Price/Hr (Flex)' ),
			'minPPHFlex' => Yii::t( 'app', 'Minimum Price/Hr (Flex)' ),
			'maxDiscountHoursFlex' => Yii::t( 'app', 'Maximum Hours for Discounts (Flex)' ),
			'minDiscountHoursFlex' => Yii::t( 'app', 'Minimum Hours for Discounts (Flex)' ),
		];
	}

	/**
	 * Returns the user record associated with this tutor
	 */
	public function getUser()
	{
		return $this->hasOne( User::classname(), [ 'id' => 'id' ] );
	}

	/**
	 * Saves the uploaded image into the photo field
	 */
	public function upload()
	{
		if ( $this->validate() )
		{
			$this->photo = file_get_contents( $this->imageFile->tempName );
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * @inheritdoc
	 * @return TutorQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new TutorQuery( get_called_class() );
	}

	public function getRecurringPricesString()
	{
		return '{ minutes:0, price:0.05 }, { minutes:80, price:0.045 }, { minutes:120, price:0.044 }';
	}

	public function loadDefaultPrices()
	{
		$this->maxPPMRecurring = Settings::getHighestPricePerMinuteRecurring();
		$this->minPPMRecurring = Settings::getLowestPricePerMinuteRecurring();
		$this->maxDiscountMinsRecurring = Settings::getMaximumMinutesForDiscountsRecurring();
		$this->minDiscountMinsRecurring = Settings::getMinimumMinutesForDiscountsRecurring();
		
		$this->maxPPHFlex = Settings::getHighestPricePerHourFlex();
		$this->minPPHFlex = Settings::getLowestPricePerHourFlex();
		$this->maxDiscountHoursFlex = Settings::getMaximumHoursForDiscountsFlex();
		$this->minDiscountHoursFlex = Settings::getMinimumHoursForDiscountsFlex();
	}

	public function getAvailableTimes( $timezone )
	{
		$availableTimes = array();
		$times = AvailableTime::find()->where( [ 'tutorId' => $this->id ] )->all();
		$custTZ = new \DateTimeZone( $timezone );
		$tutorTZ = new \DateTimeZone( $this->user->timezone );

		// loop through all the times from the db
		foreach ( $times as $time )
		{
			$newTime = array();

			// get the current date/time of the tutor
			$curDate = new \DateTime( "now", $tutorTZ );

			// convert the tutor's current day of week to a number
			$curDOW = $this->convertDOWToNum( $curDate->format( 'l' ) );

			// get the difference between the day of week of the available time and the current day of week
			$diff = $time->dayOfWeek - $curDOW;
			$interval = new \DateInterval( 'P' . abs( $diff ) . 'D' );

			// if the difference negative, we need to invert the interval
			if ( $diff < 0 )
			{
				$interval->invert = true;
			} // if ( $diff < 0 )

			// calculate the new start/end dates
			$newStartDate = $curDate->add( $interval );
			$newEndDate = new \DateTime( $curDate->format( 'Y-m-d H:i:s' ), $tutorTZ );

			// set the time of the new start/end dates
			$arr = explode( ':', $time->startTime );
			$newStartDate->setTime( intval( $arr[ 0 ] ), intval( $arr[ 1 ] ) );
			$arr = explode( ':', $time->endTime );
			$newEndDate->setTime( intval( $arr[ 0 ] ), intval( $arr[ 1 ] ) );

			// we now need to convert to customer's timezone
			$newStartDate->setTimezone( $custTZ );
			$newEndDate->setTimezone( $custTZ );

			$newTime[ 'dow' ] = array( [ $this->convertDOWToNum( $newStartDate->format( 'l' ) ) ] );
			$newTime[ 'start' ] = $newStartDate->format( 'H:i' );
			$newTime[ 'end' ] = $newEndDate->format( 'H:i' );
			$availableTimes[] = $newTime;
		} // foreach ( $times as $time )

		return $availableTimes;
	}	

	private function convertDOWToNum( $dow )
	{
		switch ( $dow )
		{
			case 'Sunday':
				return 0;

			case 'Monday':
				return 1;

			case 'Tuesday':
				return 2;

			case 'Wednesday':
				return 3;

			case 'Thursday':
				return 4;

			case 'Friday':
				return 5;

			case 'Saturday':
				return 6;

			default:
				throw new \Exception;
		}
	}
}
