<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $type
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $postalCode
 * @property string $country
 */
class Address extends \yii\db\ActiveRecord
{
	public static $types = [
		'Mailing' => 'Mailing',
		'Shipping' => 'Shipping',
		'Physical' => 'Physical',
		'Work' => 'Work',
		'Other' => 'Other'
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'address';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'userId', 'type', 'street', 'city', 'state', 'postalCode', 'country' ], 'required' ],
			[ [ 'userId' ], 'integer' ],
			[ [ 'type' ], 'string', 'max' => 10 ],
			[ [ 'street' ], 'string', 'max' => 30 ],
			[ [ 'city', 'state' ], 'string', 'max' => 20 ],
			[ [ 'postalCode' ], 'string', 'max' => 15 ],
			[ [ 'country' ], 'string', 'max' => 50 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'userId' => Yii::t( 'app', 'User ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'street' => Yii::t( 'app', 'Street' ),
			'city' => Yii::t( 'app', 'City' ),
			'state' => Yii::t( 'app', 'State' ),
			'postalCode' => Yii::t( 'app', 'Postal Code' ),
			'country' => Yii::t( 'app', 'Country' ),
		];
	}

	/**
	 * @inheritdoc
	 * @return AddressQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new AddressQuery( get_called_class() );
	}
}
