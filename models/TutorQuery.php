<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Tutor]].
 *
 * @see Tutor
 */
class TutorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Tutor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tutor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
