<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $id
 * @property integer $customerId
 * @property integer $tutorId
 * @property integer $minsPerWeek
 * @property string $startDate
 * @property integer $months
 * @property integer $status
 *
 * @property Customer $customer
 * @property Tutor $tutor
 */
class Subscription extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE = 0;
	const STATUS_DELETED = 1;
	const STATUS_PAUSED = 2;
	const STATUS_INACTIVE = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'subscription';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[ [ 'customerId', 'tutorId', 'minsPerWeek', 'minsAvailable', 'startDate' ], 'required' ],
			[ [ 'customerId', 'tutorId', 'minsPerWeek', 'minsAvailable', 'months', 'status' ], 'integer' ],
			[ [ 'startDate', 'pauseDttmUTC' ], 'safe' ],
			[ [ 'customerId' ], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => [ 'customerId' => 'id' ] ],
			[ [ 'tutorId' ], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => [ 'tutorId' => 'id' ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'customerId' => Yii::t( 'app', 'Customer ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'minsPerWeek' => Yii::t( 'app', 'Mins Per Week' ),
			'minsAvailable' => Yii::t( 'app', 'Mins Available' ),
			'startDate' => Yii::t( 'app', 'Start Date' ),
			'months' => Yii::t( 'app', 'Months' ),
			'status' => Yii::t( 'app', 'Status' ),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne( Customer::className(), [ 'id' => 'customerId' ] );
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTutor()
	{
		return $this->hasOne( Tutor::className(), [ 'id' => 'tutorId' ] );
	}

	/**
	 * @inheritdoc
	 * @return SubscriptionQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new SubscriptionQuery( get_called_class() );
	}

	public static function GetExpired()
	{
		//TODO: need to implement
		return null;
	}

	public static function GetInvoicesToSend()
	{
		//TODO: need to implement
		return null;
	}

	public function calcTotalPrice()
	{
		//TODO: need to implement
		return 0.0;
	}
}
