<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phone".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $type
 * @property string $countryCode
 * @property string $number
 */
class Phone extends \yii\db\ActiveRecord
{
	public static $types = [
		'Mobile' => 'Mobile',
		'Home' => 'Home',
		'Work' => 'Work',
		'Other' => 'Other'
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'phone';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'userId', 'number' ], 'required' ],
			[ [ 'userId' ], 'integer' ],
			[ [ 'type' ], 'string', 'max' => 10 ],
			[ [ 'countryCode' ], 'string', 'max' => 6 ],
			[ [ 'number' ], 'string', 'max' => 20 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'userId' => Yii::t( 'app', 'User ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'countryCode' => Yii::t( 'app', 'Country Code' ),
			'number' => Yii::t( 'app', 'Number' ),
		];
	}

	/**
	 * @inheritdoc
	 * @return PhoneQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new PhoneQuery( get_called_class() );
	}
}
