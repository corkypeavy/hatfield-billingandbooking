<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property string $transactionId
 * @property string $date
 * @property string $amount
 *
 * @property Purchase[] $purchases
 */
class Payment extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'payment';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'transactionId', 'datetimeUTC', 'amount' ], 'required' ],
			[ [ 'datetimeUTC' ], 'safe' ],
			[ [ 'amount' ], 'number' ],
			[ [ 'transactionId' ], 'string', 'max' => 20 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'transactionId' => Yii::t( 'app', 'Transaction ID' ),
			'datetimeUTC' => Yii::t( 'app', 'Date' ),
			'amount' => Yii::t( 'app', 'Amount' ),
		];
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getPurchases()
	{
		return $this->hasMany( Purchase::className(), [ 'paymentId' => 'id' ] );
	}

	/**
	* @inheritdoc
	* @return PaymentQuery the active query used by this AR class.
	*/
	public static function find()
	{
		return new PaymentQuery( get_called_class() );
	}
}
