<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventoryTransaction".
 *
 * @property integer $id
 * @property integer $inventoryId
 * @property integer $amount
 * @property integer $isAdded
 * @property integer $purchaseId
 * @property integer $reason
 */
class InventoryTransaction extends \yii\db\ActiveRecord
{
	const REASON_PURCHASE = 0;
	const REASON_SCHEDULED = 1;
	const REASON_FREE = 2;
	const REASON_MAKEUP = 3;
	const REASON_DELETED = 4;
	const REASON_CANCELLED = 5;

	public function getReasonString()
	{
		switch ( $this->reason )
		{
			case self::REASON_PURCHASE:
				return 'Purchase';

			case self::REASON_SCHEDULED:
				return 'Scheduled';

			case self::REASON_FREE:
				return 'Free';

			case self::REASON_MAKEUP:
				return 'Makeup';

			case self::REASON_DELETED:
				return 'Deleted';

			case self::REASON_CANCELLED:
				return 'Cancelled';

			default:
				return 'Invalid';
		}
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'inventoryTransaction';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'inventoryId', 'amount', 'isAdded', 'reason' ], 'required' ],
			[ [ 'inventoryId', 'amount', 'reason', 'purchaseId' ], 'integer' ],
			[ [ 'isAdded' ], 'boolean' ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'inventoryId' => Yii::t( 'app', 'Inventory ID' ),
			'amount' => Yii::t( 'app', 'Amount' ),
			'isAdded' => Yii::t( 'app', 'Is Added' ),
			'purchaseId' => Yii::t( 'app', 'Purchase ID' ),
			'reason' => Yii::t( 'app', 'Reason' ),
		];
	}

	/**
	 * @inheritdoc
	 * @return InventoryTransactionQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new InventoryTransactionQuery( get_called_class() );
	}
}
