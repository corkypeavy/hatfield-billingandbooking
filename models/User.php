<?php
namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\Phone;
use app\models\VideoChat;
use app\models\Email;
use app\models\Address;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string name
 * @property integer type
 * @property string timezone
 * @property string $notes
 * @property string $notesPrivate
 */
class User extends ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
	const STATUS_INACTIVE = 1;
	const STATUS_ACTIVE = 10;

	const TYPE_SUPER_ADMIN = 0;
	const TYPE_ADMIN = 1;
	const TYPE_TUTOR = 5;
	const TYPE_STUDENT = 10;

	public $password;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%user}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'username', 'email', 'name', 'password', 'status', 'type' ], 'safe' ],
			[ [ 'notes', 'notesPrivate' ], 'string', 'max' => 250 ],
			[ [ 'timezone' ], 'string', 'max' => 35 ],
			[ 'email', 'email' ],
			[ 'status', 'default', 'value' => self::STATUS_ACTIVE ],
			[ 'status', 'in', 'range' => [ self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'username' => Yii::t( 'app', 'Username' ),
			'auth_key' => Yii::t( 'app', 'Authorization Key' ),
			'password_hash' => Yii::t( 'app', 'Password Hash' ),
			'password_reset_token' => Yii::t( 'app', 'Password Reset Token' ),
			'email' => Yii::t( 'app', 'Email' ),
			'status' => Yii::t( 'app', 'Status' ),
			'created_at' => Yii::t( 'app', 'Created At' ),
			'updated_at' => Yii::t( 'app', 'Updated At' ),
			'name' => Yii::t( 'app', 'Name' ),
			'type' => Yii::t( 'app', 'Type' ),
			'timezone' => Yii::t( 'app', 'Timezone' ),
			'notes' => Yii::t( 'app', 'Notes' ),
			'notesPrivate' => Yii::t( 'app', 'Notes Private' ),
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity( $id )
	{
		return static::findOne( [ 'id' => $id, 'status' => self::STATUS_ACTIVE ] );
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken( $token, $type = null )
	{
		throw new NotSupportedException( '"findIdentityByAccessToken" is not implemented.' );
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername( $username )
	{
		return static::findOne( [ 'username' => $username, 'status' => self::STATUS_ACTIVE ] );
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken( $token )
	{
		if ( !static::isPasswordResetTokenValid( $token ) )
		{
			return null;
		}

		return static::findOne( [ 'password_reset_token' => $token, 'status' => self::STATUS_ACTIVE ] );
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return bool
	 */
	public static function isPasswordResetTokenValid( $token )
	{
		if ( empty( $token ) )
		{
			return false;
		}

		$timestamp = ( int ) substr( $token, strrpos( $token, '_' ) + 1 );
		$expire = Yii::$app->params[ 'user.passwordResetTokenExpire' ];
		return $timestamp + $expire >= time();
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey( $authKey )
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword( $password )
	{
		return Yii::$app->security->validatePassword( $password, $this->password_hash );
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword( $password )
	{
		$this->password_hash = Yii::$app->security->generatePasswordHash( $password );
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}

	public function isSuperAdmin()
	{
		return ( $this->type === TYPE_SUPER_ADMIN );
	}

	/**
	 * Checks to see if the current user is an admin user
	 *
	 * return true if the user is an admin, otherwise false
	 */
	public function isAdmin()
	{
		return ( $this->type === static::TYPE_ADMIN || $this->type === static::TYPE_SUPER_ADMIN );
	}

	/**
	 * Checks to see if the current user is an admin user
	 *
	 * return true if the user is an admin, otherwise false
	 */
	public function isTutor()
	{
		return ( $this->type === self::TYPE_TUTOR );
	}

	/**
	 * Checks to see if the current user is an admin user
	 *
	 * return true if the user is an admin, otherwise false
	 */
	public function isStudent()
	{
		return $this->type === self::TYPE_STUDENT;
	}

	/**
	 * returns a string representing a User's status
	 */
	public function statusString()
	{
		switch ( $this->status )
		{
			case self::STATUS_DELETED:
				return 'Deleted';
			case self::STATUS_INACTIVE:
				return 'Inactive';
			case self::STATUS_ACTIVE:
				return 'Active';
			default:
				return 'Invalid';
		}
	}

	/**
	 * returns a string representing the type of user
	 */
	public function typeString()
	{
		switch ( $this->type )
		{
			case self::TYPE_SUPER_ADMIN:
				return 'Super Admin';
			case self::TYPE_ADMIN:
				return 'Admin';
			case self::TYPE_TUTOR:
				return 'Tutor';
			case self::TYPE_STUDENT:
				return 'Customer';
			default:
				return 'Invalid';
		}
	}

	/**
	 * Returns an array of Phone models
	 */
	public function getPhones()
	{
		return $this->hasMany( Phone::classname(), [ 'userId' => 'id' ] );
	}

	public function getPhonesHtml( $returnUrl = NULL, $editable = true )
	{
		$phones = $this->getPhones()->all();
		$str = '';
		$bAddDeleteIcon = ( count( $phones ) > 1 && $editable );

		foreach ( $phones as $phone )
		{
			$str .= $phone->type . ' - +' . $phone->countryCode . ' ' . $phone->number .
				( $bAddDeleteIcon ? Html::a( '<span class="glyphicon glyphicon-trash"></span>', Url::to( [ 'user/delete-phone', 'id' => $phone->id, 'returnUrl' => $returnUrl ] ) ) : '' )  . '<br />';
		}

		// if this is editable
		if ( $editable )
		{
			$str .= Html::a( 'Add', '#', [
				'value' => Url::to( [ 'add-phone', 'userId' => $this->id, 'returnUrl' => $returnUrl ] ),
				'title' => 'Add Phone',
				'class' => 'showModalButton' ] );
		} // if ( $editable )

		return $str;
	}

	/**
	 * Returns an array of VideoChat models
	 */
	public function getVideoChats()
	{
		return $this->hasMany( VideoChat::classname(), [ 'userId' => 'id' ] );
	}

	public function getVideoChatsHtml( $returnUrl = NULL )
	{
		$videoChats  = $this->getVideoChats()->all();
		$str = '';
		$bAddDeleteIcon = count( $videoChats ) > 1;

		foreach ( $videoChats as $videoChat )
		{
			$str .= $videoChat->type . ' - ' . $videoChat->accountId;

			// if this is empty, then we are not allowing editing of video chats
			if ( $returnUrl != null )
			{
				$str .= ( $bAddDeleteIcon ? Html::a( '<span class="glyphicon glyphicon-trash"></span>', Url::to( [ 'user/delete-video-chat', 'id' => $videoChat->id, 'returnUrl' => $returnUrl ] ) ) : '' ) . '<br />';
			}
		}

		// if this is empty, then we are not allowing editing of video chats
		if ( $returnUrl != null )
		{
			$str .= Html::a( 'Add', '#', [
				'value' => Url::to( [ 'add-video-chat', 'userId' => $this->id, 'returnUrl' => $returnUrl ] ),
				'title' => 'Add Video Chat',
				'class' => 'showModalButton' ] );
		}

		return $str;
	}

	public function getEmails()
	{
		return $this->hasMany( Email::classname(), [ 'userId' => 'id' ] );
	}

	public function getEmailsHtml( $returnUrl = NULL )
	{
		$emails  = $this->getEmails()->all();
		$str = '';
		$bAddDeleteIcon = count( $emails ) > 1;

		foreach ( $emails as $email )
		{
			$str .= $email->type . ' - ' . $email->address . 
			( $bAddDeleteIcon && $email->address != $this->email ? Html::a( '<span class="glyphicon glyphicon-trash"></span>', Url::to( [ 'user/delete-email', 'id' => $email->id, 'returnUrl' => $returnUrl ] ) ) : '' ) . '<br />';
		}

		$str .= Html::a( 'Add', '#', [
			'value' => Url::to( [ 'add-email', 'userId' => $this->id, 'returnUrl' => $returnUrl ] ),
			'title' => 'Add Email',
			'class' => 'showModalButton' ] );

		return $str;
	}

	public function getAddresses()
	{
		return $this->hasMany( Address::classname(), [ 'userId' => 'id' ] );
	}

	public function getAddressesHtml( $returnUrl = NULL )
	{
		$addresses  = $this->getAddresses()->all();
		$str = '';
		$bAddDeleteIcon = count( $addresses ) > 1;
		$i = 0;

		foreach ( $addresses as $address )
		{
			if ( $i > 0 )
			{
				$str .= '<br />';
			}
			else
			{
				$i++;
			}

			$str .= '<b>' . $address->type . '</b><br />'
				. $address->street . '<br />' 
				. $address->city . ', ' . $address->state . ' ' . $address->postalCode . '<br />'
				. $address->country . ' '
				. ( $bAddDeleteIcon ? Html::a( '<span class="glyphicon glyphicon-trash"></span>', Url::to( [ 'user/delete-address', 'id' => $address->id, 'returnUrl' => $returnUrl ] ) ) : '' ) . '<br />';
		}

		$str .= Html::a( 'Add', '#', [
			'value' => Url::to( [ 'add-address', 'userId' => $this->id, 'returnUrl' => $returnUrl ] ),
			'title' => 'Add Address',
			'class' => 'showModalButton' ] );

		return $str;
	}
}
