<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AvailableTime]].
 *
 * @see AvailableTime
 */
class AvailableTimeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return AvailableTime[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AvailableTime|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
