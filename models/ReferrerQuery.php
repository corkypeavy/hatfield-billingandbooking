<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Referrer]].
 *
 * @see Referrer
 */
class ReferrerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Referrer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Referrer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
