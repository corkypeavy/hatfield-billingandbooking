<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dunning".
 *
 * @property integer $step
 * @property string $message
 * @property integer $days
 */
class Dunning extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'dunning';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'step', 'message', 'days' ], 'required' ],
			[ [ 'step', 'days' ], 'integer' ],
			[ [ 'message' ], 'string', 'max' => 255 ],
			[ [ 'step' ], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'step' => Yii::t( 'app', 'Step' ),
			'message' => Yii::t( 'app', 'Message' ),
			'days' => Yii::t( 'app', 'Days' ),
		];
	}
}
