<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "referrer".
 *
 * @property integer $id
 * @property string $name
 * @property integer $canShow
 */
class Referrer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referrer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['canShow'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'canShow' => Yii::t('app', 'Can Show'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReferrerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReferrerQuery(get_called_class());
    }
}
