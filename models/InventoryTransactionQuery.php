<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[InventoryTransaction]].
 *
 * @see InventoryTransaction
 */
class InventoryTransactionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return InventoryTransaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return InventoryTransaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
