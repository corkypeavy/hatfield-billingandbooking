<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventory".
 *
 * @property integer $id
 * @property integer $customerId
 * @property integer $tutorId
 * @property integer $type
 * @property integer $availableAmount
 *
 * @property Customer $customer
 */
class Inventory extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'inventory';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'customerId', 'tutorId', 'type', 'availableAmount' ], 'required' ],
			[ [ 'customerId', 'tutorId', 'type', 'availableAmount' ], 'integer' ],
			[ [ 'customerId' ], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => [ 'customerId' => 'id' ] ],
			[ [ 'tutorId' ], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => [ 'tutorId' => 'id' ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'customerId' => Yii::t( 'app', 'Customer ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'availableAmount' => Yii::t( 'app', 'Available Amount'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne( Customer::className(), [ 'id' => 'customerId' ] );
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTutor()
	{
		return $this->hasOne( Tutor::className(), [ 'id' => 'tutorId' ] );
	}

	/**
	 * @inheritdoc
	 * @return InventoryQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new InventoryQuery( get_called_class() );
	}
}
