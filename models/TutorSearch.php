<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tutor;
use app\models\helpers\PurchaseType;
use yii\db\Query;
use app\models\Phone;

/**
 * TutorSearch represents the model behind the search form about `app\models\Tutor`.
 */
class TutorSearch extends Tutor
{
	public $phone;
	public $timezone;
	public $name;
	public $username;
	public $email;
	public $status;
	public $start = 0;
	public $sales = 0;
	public $tutor = 0;
	public $salesAmt = 100.0;
	public $startMonths = 1;
	public $show = 0;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'id', 'startMonths' ], 'integer' ],
			[ [ 'salesAmt' ], 'double' ],
			[ [ 'bio', 'address', 'photo', 'timezone', 'phone', 'name', 'username', 'email', 'status', 'start',
				 'sales', 'tutor', 'show' ], 'safe' ],
		];
	}

	public function attributeLabels()
	{
		return [
			'startMonths' => 'months ago',
			'salesAmt' => 'sales last month' 
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function searchEmail( $params )
	{
		$query = Tutor::find()->joinWith( 'user' );

		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		if ( !empty( $this->tutor ) )
		{
			$query->where( [ 'tutor.id' => $this->tutor ] );
			return $dataProvider;
		}

		if ( !empty( $this->sales ) && $this->salesAmt > 0.0 )
		{
			$date = new \DateTime();
			$interval = new \DateInterval( 'P1M' );
			$date->sub( $interval );
			
			if ( $this->sales == '1' )
			{
				$query->andFilterWhere( [
						'in',
						'tutor.id',
						Purchase::find()->select( 'tutorId' )
							->where( [ 'between', 'createdDttmUTC', $date->format( 'Y-m-1 00:00:00' ), $date->format( 'Y-m-t 11:59:59' ) ] )
							->groupBy( 'tutorId' )
							->having( 'SUM( totalPrice ) >= ' . $this->salesAmt ) ] );
			}
			else
			{
				$query->andFilterWhere( [
						'not in',
						'tutor.id',
						Purchase::find()->select( 'tutorId' )
							->where( [ 'between', 'createdDttmUTC', $date->format( 'Y-m-1 00:00:00' ), $date->format( 'Y-m-t 11:59:59' ) ] )
							->groupBy( 'tutorId' )
							->having( 'SUM( totalPrice ) >= ' . $this->salesAmt ) ] );
			}
		}

		if ( !empty( $this->start ) )
		{
			$date = new \DateTime();
			$monthsAgo = new \DateInterval( 'P' . $this->startMonths . 'M' );
			$monthsAgo->invert = 1;
			$searchDate = $date->add( $monthsAgo );

			if ( $this->start == '1' )
			{
				$query->andFilterWhere( [ '<' , 'user.created_at', strtotime( $searchDate->format( 'Y-m-d H:i:s' ) ) ] );
			}
			else
			{
				$query->andFilterWhere( [ '>' , 'user.created_at', strtotime( $searchDate->format( 'Y-m-d H:i:s' ) ) ] );
			}
		}

		return $dataProvider;
	}

	public function searchFree( $user )
	{
		$query = Tutor::find()->joinWith( 'user' );
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$query->andFilterWhere( [ 'user.status' => User::STATUS_ACTIVE ] );
		$query->andFilterWhere( [ 'not in', 'tutor.id', Purchase::find()->select( 'tutorId' )
			->where( [ 'customerId' => $user->id ] )
			->andWhere( [ 'not', [ 'type' => PurchaseType::FREE ] ] ) ] );
		$query->andFilterWhere( [ 'in', 'tutor.id', AvailableTime::find()->select( 'tutorId' ) ] ); 

		return $dataProvider; 
	}

	public function searchCustomerTutors( $custId)
	{
		$query = Tutor::find()->joinWith( 'user' );
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

//		$query->andFilterWhere( [ 'user.status' => User::STATUS_ACTIVE ] );
		$query->andFilterWhere( [ 'in', 'tutor.id', Purchase::find()
																	->select( 'tutorId' )
																	->where( [ 'customerId' => $custId ] ) ] );

		return $dataProvider; 
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params )
	{
		$query = Tutor::find()->joinWith( 'user' );

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere( [
			'id' => $this->id,
			'user.status' => $this->getStatusFromText( $this->status )
		] );

		$query->andFilterWhere( [ 'like', 'user.name', $this->name ] )
			->andFilterWhere( [ 'like', 'user.timezone', $this->timezone ] )
			->andFilterWhere( [ 'like', 'user.username', $this->username ] )
			->andFilterWhere( [ 'like', 'user.email', $this->email ] );
		
		if ( !empty( $this->phone ) )
		{
			$query->andFilterWhere( [ 'in', 'user.id', Phone::find()->select( 'userId as id' )->where( [ 'like', 'number', $this->phone ] ) ] );
		}

		return $dataProvider;
	}

	protected function getStatusFromText( $status )
	{
		if ( !empty( $status ) )
		{
			if ( strncasecmp( $status, "Active", strlen( $status ) ) == 0 )
			{
				return User::STATUS_ACTIVE;
			}
			else if ( strncasecmp( $status, "Inactive", strlen( $status ) ) == 0 )
			{
				return User::STATUS_INACTIVE;
			}
			else if ( strncasecmp( $status, "Deleted", strlen( $status ) ) == 0 )
			{
				return User::STATUS_DELETED;
			}
		}
	}
}
