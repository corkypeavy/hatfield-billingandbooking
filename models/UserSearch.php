<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tutor;
use app\models\Phone;

/**
 * TutorSearch represents the model behind the search form about `app\models\Tutor`.
 */
class UserSearch extends User
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'id', /*'status',*/ 'created_at', 'updated_at', 'type' ], 'integer'],
			[ [ 'notes', 'notesPrivate', 'username', 'email', 'name', 'status' ], 'safe' ],
		];
	}

	/**
	 * @inheritdoc
	 */
/*	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
*/

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params )
	{
		$query = User::find()->where( [ 'in', 'type', [ User::TYPE_SUPER_ADMIN, User::TYPE_ADMIN ] ] );

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere( [
			'id' => $this->id,
			'status' => $this->getStatusFromText( $this->status ),
			'type' => $this->type,
		] );

		$query->andFilterWhere( [ 'like', 'username', $this->username ] )
			->andFilterWhere( [ 'like', 'email', $this->email ] )
			->andFilterWhere( [ 'like', 'name', $this->name ] )
			->andFilterWhere( [ 'like', 'timezone', $this->timezone ] )
			->andFilterWhere( [ 'like', 'notes', $this->notes ] )
			->andFilterWhere( [ 'like', 'notesPrivate', $this->notesPrivate ] );
//			->andFilterWhere( [ 'in', 'id', Phone::find()->select( 'userId' )->where( [ 'like', 'number', $this->phone ] ) ] );

		return $dataProvider;
	}

	protected function getStatusFromText( $status )
	{
		if ( !empty( $status ) )
		{
			if ( strncasecmp( $status, "Active", strlen( $status ) ) == 0 )
			{
				return User::STATUS_ACTIVE;
			}
			else if ( strncasecmp( $status, "Inactive", strlen( $status ) ) == 0 )
			{
				return User::STATUS_INACTIVE;
			}
			else if ( strncasecmp( $status, "Deleted", strlen( $status ) ) == 0 )
			{
				return User::STATUS_DELETED;
			}
		}
	}
}
