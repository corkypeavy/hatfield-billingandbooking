<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Purchase;
use app\models\helpers\PurchaseStatus;

use yii\helpers\VarDumper;

/**
 * PurchaseSearch represents the model behind the search form about `app\models\Purchase`.
 */
class PurchaseSearch extends Purchase
{
	public $dateRange = 0;
	public $start;
	public $end;

	protected function setDateValues()
	{
		switch ( $this->dateRange )
		{
			case 0:
				$date = new \DateTime();
				$year = $date->format( 'Y' );
				$month = $date->format( 'm' );

				// if the month is january, we also need to change the year
				if ( $month == 1 )
				{
					$year = intval( $year ) - 1;
					$month = 12;
				} // if ( intval( $month ) === 1 )

				// else we just need to subract 1
				else
				{
					$month--;
				} // else

				// set to the first day of previous month
				$date->setDate( $year, $month, 1 );
				$this->start = $date;

				// this returns the last day of the month for the given month
				$lastDay = $date->format( 't' );

				$date = new \DateTime();
				$date->setDate( $year, $month, $lastDay );
				$this->end = $date;		
				break;

			case 1:
				$date = new \DateTime();
				$year = $date->format( 'Y' );
				$month = $date->format( 'm' );

				// set to the first day of previous month
				$date->setDate( $year, $month, 1 );
				$this->start = $date;

				// this returns the last day of the month for the given month
				$lastDay = $date->format( 't' );

				$date = new \DateTime();
				$date->setDate( $year, $month, $lastDay );
				$this->end = $date;		
				break;

			case 2:
				$this->start = new \DateTime( $this->start );
				$this->end = new \DateTime( $this->end );
				break;
			
			default:
				throw new Exception( 'Invalid Date Range' );
				break;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'id', 'customerId', 'productId', 'type', 'tutorId', 'quantity', 'status', 'dunningStepComplete', 'hasViewed', 'dateRange' ], 'integer' ],
			[ [ 'totalPrice' ], 'number' ],
			[ [ 'dateCreated', 'datePaid', 'notes', 'start', 'end' ], 'safe' ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function searchUnpaid( $custId )
	{
		$query = Purchase::find()->where( [ 'customerId' => $custId, 'status' => PurchaseStatus::UNPAID ] );
		return new ActiveDataProvider( [ 'query' => $query ] );
	}

	public function searchHistory( $custId )
	{
		$query = Purchase::find()->where( [ 'customerId' => $custId ] );
		return new ActiveDataProvider( [ 'query' => $query ] );
	}

	public function searchPurchases( $params )
	{
		$query = Purchase::find();
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );
		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere( [
			'customerId' => $this->customerId,
			'tutorId' => $this->tutorId
		] );

		$this->setDateValues();

		$query->andFilterWhere( [
			'between', 'createdDttmUTC', $this->start->format( 'Y-m-d h:m' ), $this->end->format( 'Y-m-d h:m' )
		] );

		return $dataProvider;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Purchase::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );

		$this->load( $params );

		if ( !$this->validate() )
		{
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere( [
			'id' => $this->id,
			'customerId' => $this->customerId,
			'type' => $this->type,
			'productId' => $this->productId,
			'tutorId' => $this->tutorId,
			'quantity' => $this->quantity,
			'totalPrice' => $this->totalPrice,
			'createdDttmUTC' => $this->createdDttmUTC,
			'datePaid' => $this->paidDttmUTC,
			'status' => $this->status,
			'dunningStepComplete' => $this->dunningStepComplete,
			'hasViewed' => $this->hasViewed,
			'paymentId' => $this->paymentId
		] );

		$query->andFilterWhere( [ 'like', 'notes', $this->notes ] );

		return $dataProvider;
	}
}
