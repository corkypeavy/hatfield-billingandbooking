<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $tutorId
 * @property integer $type
 * @property string $name
 * @property string $description
 * @property string $pricePerMinute
 * @property integer $purchaseMinutes
 *
 * @property Tutor $tutor
 * @property Purchase[] $purchases
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tutorId', 'type', 'name', 'description', 'pricePerMinute', 'purchaseMinutes'], 'required'],
            [['tutorId', 'type', 'purchaseMinutes'], 'integer'],
            [['pricePerMinute'], 'number'],
            [['name'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 200],
            [['tutorId'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutorId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tutorId' => Yii::t('app', 'Tutor ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'pricePerMinute' => Yii::t('app', 'Price Per Minute'),
            'purchaseMinutes' => Yii::t('app', 'Purchase Minutes'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutor()
    {
        return $this->hasOne(Tutor::className(), ['id' => 'tutorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases()
    {
        return $this->hasMany(Purchase::className(), ['productId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
