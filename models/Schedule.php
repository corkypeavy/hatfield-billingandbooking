<?php

namespace app\models;

use Yii;
use app\models\helpers\TimeType;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property integer $tutorId
 * @property integer $customerId
 * @property integer $type
 * @property string $startDttmUTC
 * @property string $endDttmUTC
 * @property integer $status
 * @property string $notes
 * @property integer $subscriptionId
 *
 * @property Customer $customer
 * @property Tutor $tutor
 */
class Schedule extends \yii\db\ActiveRecord
{
	const STATUS_SCHEDULED = 0;
	const STATUS_CANCELLED = 1;
	const STATUS_COMPLETED = 2;

	public function typeString()
	{
		return TimeType::toString( $this->type );
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'schedule';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'tutorId', 'customerId', 'type', 'startDttmUTC', 'endDttmUTC', 'status' ], 'required' ],
			[ [ 'tutorId', 'customerId', 'type', 'status', 'subscriptionId' ], 'integer' ],
			[ [ 'startDttmUTC', 'endDttmUTC' ], 'safe' ],
			[ [ 'notes' ], 'string', 'max' => 200 ],
			[ [ 'customerId' ], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => [ 'customerId' => 'id' ] ],
			[ [ 'tutorId' ], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => [ 'tutorId' => 'id' ] ],
			[ [ 'subscriptionId' ], 'exist', 'skipOnError' => true, 'targetClass' => Subscription::className(), 'targetAttribute' => [ 'subscriptionId' => 'id' ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'customerId' => Yii::t( 'app', 'Customer ID' ),
			'type' => Yii::t( 'app', 'Type' ),
			'startDttmUTC' => Yii::t( 'app', 'Start Dttm Utc' ),
			'endDttmUTC' => Yii::t( 'app', 'End Dttm Utc' ),
			'status' => Yii::t( 'app', 'Status' ),
			'notes' => Yii::t( 'app', 'Notes' ),
			'subscriptionId' => Yii::t( 'app', 'Subscription ID' )
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer()
	{
		return $this->hasOne( Customer::className(), [ 'id' => 'customerId' ] );
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTutor()
	{
		return $this->hasOne( Tutor::className(), [ 'id' => 'tutorId' ] );
	}

	public function getSubscription()
	{
		return $this->hasOne( Subscription::className(), [ 'id' => 'subscriptionId' ] );
	}

	public function isConflict()
	{
		//TODO: Need to implement this function
		return false;
	}

	/**
	 * @inheritdoc
	 * @return ScheduleQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new ScheduleQuery( get_called_class() );
	}
}
