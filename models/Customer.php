<?php

namespace app\models;

use Yii;
use app\models\Purchase;
use app\models\helpers\PurchaseStatus;
use app\models\helpers\PurchaseType;
use app\models\Inventory;
use app\models\InventoryTransaction;
use app\models\Schedule;
use app\models\helpers\TimeType;

use yii\helpers\VarDumper;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $timezone
 * @property integer $deleted
 * @property integer $active
 * @property string $howFound
 * @property string $howFoundOther
 * @property integer $emailConfirmed
 */
class Customer extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'customer';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[ [ 'id' ], 'required' ],
			[ [ 'id', 'emailConfirmed' ], 'integer' ],
			[ [ 'howFound', 'howFoundOther' ], 'string', 'max' => 30 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t( 'app', 'ID' ),
			'howFound' => Yii::t( 'app', 'How Found' ),
			'howFoundOther' => Yii::t( 'app', 'How Found Other' ),
			'emailConfirmed' => Yii::t( 'app', 'Email Confirmed' ),
		];
	}

	public function getTimeZoneOffset()
	{
		$tz = new \DateTimeZone( $this->user->timezone );
		return $tz->getOffset( new \DateTime() ) / 60; // we need in minutes, not seconds
	}

	/**
	 * Returns the user record associated with this customer
	 */
	public function getUser()
	{
		return $this->hasOne( User::classname(), [ 'id' => 'id' ] );
	}

	/**
	 * Returns the purchase records associated with this customer
	 */
	public function getSubscriptions()
	{
		return $this->hasMany( Subscription::classname(), [ 'customerId' => 'id' ] );
	}

	public function isNew()
	{
		return !Purchase::find()
					->where( [ 'customerId' => $this->id ] )
					->andWhere( [ 'not', [ 'type' => PurchaseType::FREE ] ] )
					->exists();
	}

	public function getOutstandingBalance()
	{
		return Purchase::find()->where( [ 'customerId' => $this->id, 'status' => PurchaseStatus::UNPAID ] )->sum( 'totalPrice' );
	}

	public function getRecurringSubscriptionsHtml()
	{
		$subs = Subscription::find()->where( [ 'customerId' => $this->id ] )->andWhere( [ 'status' => STATUS_ACTIVE ] )->all();
		$html = '';

		// if the customer has some subscriptions
		if ( count( $subs ) > 0 )
		{
			// loop through the subscriptions
			foreach ( $subs as $sub )
			{
				$html .= '<h4>Tutor - '. $sub->tutor->user->name . '</h4>';
				$html .= '<p>Minutes Per Week - ' . $sub->minsPerWeek . '</p><br />';
			} // foreach ( $subs as $sub )
		} // if ( count( $subs ) > 0 )

		// else no subscriptions
		else
		{
			$html = '<h4>You currently do not have any subscriptions</h4>';
		} // else

		return $html;
	}

	public function getFlexBalancesHtml()
	{
		$html = '';
		$inventories = Inventory::find()->where( [ 'customerId' => $this->id ] )->andWhere( [ 'availableAmount > 0' ] )->all();
		
		if ( $inventories.count() == 0 )
		{
			return '<h4>You currently do not have any flex-time inventory</h4>';			
		}

		foreach ( $inventories as $inv )
		{
			$html = '<h4>Tutor: ' . $inv->tutor->user->name . ' ' . $inv->availableAmount . '</h4>';
		}
	}

	public function getInventoryRecord( $tutorId, $type )
	{
		$inv = Inventory::find()->where ( [ 'customerId' => $this->id, 'tutorId' => $tutorId, 'type' => $type ] )->one();

		// if there was no record returned, we need to creat one
		if ( $inv == false )
		{
			$inv = new Inventory();
			$inv->customerId = $this->id;
			$inv->tutorId = $tutorId;
			$inv->type = $type;
			$inv->availableAmount = 0;
Yii::trace( VarDumper::dumpAsString( $inv ) );

			// if we can't save it, we have a major problem
			if ( !$inv->save() )
			{
				throw new \yii\base\Exception( 'Cannot save inventory record' );
			} // if ( !$inv->save() )
		} // if ( $inv === false )

		return $inv;
	}

	public function updateInventory( $tutorId, $quantity, $add, $reason, $purchaseId = null )
	{
		$inventory = $this->getInventoryRecord( $tutorId, TimeType::FLEX );

		// we need to add an inventory transaction
		$invTrans = new InventoryTransaction();
		$invTrans->inventoryId = $inventory->id;
		$invTrans->amount = $quantity;
		$invTrans->isAdded = $add;
		$invTrans->reason = $reason;
		$invTrans->purchaseId = $purchaseId;

		// if the transaction is saved
		if ( $invTrans->save() )
		{

			// we need to update the inventory item's available amount
			// if we are to add
			if ( $add )
			{
				$inventory->availableAmount += $quantity;
			} // if ( $add )

			// else we're going to substract
			else
			{
				$inventory->availableAmount -= $quantity;
			} // else

			// if the inventory is saved
			if ( $inventory->save() )
			{
				return true;
			} // if ( $inventory->save() )
		} // if ( $invTrans->save() )

		return false;
	}

	public function resetSubscriptions( $tutorId )
	{
		Yii::$app->db->createCommand( 'UPDATE subscription' .
												' SET minsAvailable = minsPerWeek' .
												' WHERE customerId = ' . $this->id . ' AND tutorId = ' . $tutorId )->execute();
	}

	public function updateSubscription( $id, $duration )
	{
		$subscription = Subscription::findOne( $id );
		$subscription->minsAvailable -= $duration;

		// if we saved successfully
		if ( $subscription->save() )
		{
			return true;
		} // if ( $subscription->save() )

		return false;
	}

	public function hasConflicts( $startUTC, $endUTC )
	{
		$week = new \DateInterval( 'P7D' );

		// loop thru 4 weeks for now
		// TODO:  need to probably get subscription end date
		for ( $i = 0; $i < 4; $i++ )
		{

			if ( Schedule::find()
				->where( [ 'customerId' => Yii::$app->user->id ] )
				->andWhere( [ 'not', [ 'type' => TimeType::RECURRING ] ] )
				->andWhere( [ 'or', '\''. $startUTC->format( 'Y-m-d h:i' ) . '\' between `startDttmUTC` AND `endDttmUTC`', '\'' . $endUTC->format( 'Y-m-d h:i' ) . '\' between `startDttmUTC` AND `endDttmUTC`' ] )
				->exists() )
			{
				return true;
			}

			$startUTC->add( $week );
			$endUTC->add( $week );
		}

		return false;
	}

	public function getAvailableRecurring( $tutorId )
	{
		return Subscription::find()->where( [ 'customerId' => Yii::$app->user->id, 'tutorId' => $tutorId, 'status' => Subscription::STATUS_ACTIVE ] )->sum( 'minsAvailable' );
	}

	public function getAvailableSubscriptions( $tutorId )
	{
		return Subscription::find()->where( [ 'customerId' => Yii::$app->user->id, 'tutorId' => $tutorId, 'status' => Subscription::STATUS_ACTIVE ] )->all();
	}

	public function getAvailableFlex( $tutorId )
	{
		$invFlex = $this->getInventoryRecord( $tutorId, TimeType::FLEX );
//		$invMakeup = $this->getInventoryRecord( $tutorId, TimeType::MAKEUP );

		return $invFlex->availableAmount + $invMakeup->availableAmount;
	}

	public function getAvailableFree( $tutorId )
	{
		// if we have already scheduled an event with this tutor
		if ( Schedule::find()->where( [ 'customerId' => $this->id, 'tutorId' => $tutorId ] )->exists() )
		{
			return 0;
		} // if ( Schedule::find()->where( [ 'customerId' => $this->id, 'tutorId' => $tutorId ] )->exists() )

		return 1;
	}

	public function getSubscriptionEndDateForTutor( $tutorId, $custTZ )
	{
		$subs = Subscription::find()->where( [ 'customerId' => $this->id, 'tutorId' => $tutorId ] )->all();
		$endDate = new \DateTime( 'now', $custTZ );

		// loop through the subscriptions for this customer for this tutor (ideally, there is only one)
		foreach ( $subs as $sub )
		{
			$startDate = new \DateTime( $sub->startDate );
			$duration = new \DateTimeInterval( 'P' . $sub->months . 'M' );
			$newEndDate = $startDate->add( $duration );

			// if this subscription is longer than the previous ones
			if ( $newEndDate > $endDate )
			{
				$endDate = $newEndDate;
			} // if ( $newEndDate > $endDate )
		} // foreach ( $subs as $sub )

		return $endDate;
	}

	/**
	 * @inheritdoc
	 * @return CustomerQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new CustomerQuery( get_called_class() );
	}
}
