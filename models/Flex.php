<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $tutorId
 * @property integer $customerId
 * @property integer $hours
 *
 * @property Customer $customer
 * @property Tutor $tutor
 */
class Flex extends Model
{
	public $hours = 0;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'hours', 'tutorId', 'customerId' ], 'required' ],
			[ [ 'customerId', 'tutorId', 'hours' ], 'integer' ],
			[ [ 'customerId' ], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => [ 'customerId' => 'id' ] ],
			[ [ 'tutorId' ], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => [ 'tutorId' => 'id' ] ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'customerId' => Yii::t( 'app', 'Customer ID' ),
			'tutorId' => Yii::t( 'app', 'Tutor ID' ),
			'hours' => Yii::t( 'app', 'Enter Hours to Purchase' ),
		];
	}

	public function getMinutes()
	{
		return $hours * 60;
	}

	public function getPricingHtml()
	{
		return '<p>Less than 5 hours:  $00.00/hour</p><p>5 hours or more: $00.00/hour</p><p>10 hours or more: $00.00/hour</p>';
	}
}
