<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class CustomPurchase extends Model
{
	public $amount;
	public $description;
	public $tutorId;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[ [ 'amount', 'description' ], 'required' ],
			[ [ 'amount' ], 'number' ],
			[ [ 'tutorId' ], 'integer' ],
			[ [ 'description' ], 'string', 'max' => 250 ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'amount' => Yii::t( 'app', 'Amount' ),
			'description' => Yii::t( 'app', 'Description' ),
		];
	}
}