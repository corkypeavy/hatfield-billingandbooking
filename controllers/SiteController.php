<?php
namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => [ 'logout', 'signup' ],
				'rules' => [
					[
						'actions' => [ 'signup' ],
						'allow' => true,
						'roles' => [ '?' ],
					],
					[
						'actions' => [ 'logout' ],
						'allow' => true,
						'roles' => [ '@' ],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
					'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		// if the user is logged in
		if ( !Yii::$app->user->isGuest )
		{
			$this->goToUserHome();
		} // if ( !Yii::$app->user->isGuest )
		
		return $this->render( 'index' );
	}

	public function goToUserHome( $user = null )
	{
		// if the user is not set
		if ( empty( $user ) )
		{
			$user = User::findOne( Yii::$app->user->id );
		} // if ( empty( $user ) )

		// if the user is an admin/superAdmin
		if ( $user->isAdmin() )
		{
			return $this->redirect( [ 'admin/index' ] );
		} // if ( $user->isAdmin() )

		// else if the user is a tutor
		else if ( $user->isTutor() )
		{
			return $this->redirect( [ 'tutor/tasks' ] );
		} // else if ( $user->isTutor() )

		// else the user is a customer
		else
		{
			return $this->redirect( [ 'customer/tasks' ] );
		} // else
	}

	/**
	 * Logs in a user.
	 *
	 * @return mixed
	 */
	public function actionLogin()
	{
		// if the user is already logged in
		if ( !Yii::$app->user->isGuest )
		{
			return $this->goToUserHome();
		} // if ( !Yii::$app->user->isGuest )

		$model = new LoginForm();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) && $model->login() )
		{
			$user = User::findByUsername( $model->username );

			$this->goToUserHome( $user );
		} // if ( $model->load( Yii::$app->request->post() ) && $model->login() )

		return $this->render( 'login', [ 'model' => $model ] );
	}

	/**
	 * Logs out the current user.
	 *
	 * @return mixed
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * Displays contact page.
	 *
	 * @return mixed
	 */
	public function actionContact()
	{
		$model = new ContactForm();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() )
		{
			// if the mail was sent successfully
			if ( $model->sendEmail( Yii::$app->params[ 'adminEmail' ] ) )
			{
				Yii::$app->session->setFlash( 'success', 'Thank you for contacting us. We will respond to you as soon as possible.' );
			} // if ( $model->sendEmail( Yii::$app->params[ 'adminEmail' ] ) )

			// else there was a problem sending the email
			else
			{
				Yii::$app->session->setFlash( 'error', 'There was an error sending email.' );
			} // else

			return $this->refresh();
		} // if ( $model->load( Yii::$app->request->post() ) && $model->validate() )

		return $this->render( 'contact', [ 'model' => $model ] );
	}

	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionSignup()
	{
		$model = new SignupForm();

		if ( $model->load( Yii::$app->request->post() ) )
		{
			if ( $user = $model->signup() )
			{
				if ( Yii::$app->getUser()->login( $user ) )
				{
					return $this->goHome();
				}
			}
		}

		return $this->render( 'signup', [ 'model' => $model ] );
	}

	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() )
		{
			if ( $model->sendEmail() )
			{
				Yii::$app->session->setFlash( 'success', 'Check your email for further instructions.' );

				return $this->goHome(); 
			}
			else
			{
				Yii::$app->session->setFlash( 'error', 'Sorry, we are unable to reset password for email provided.' );
			}
		}

		return $this->render( 'requestPasswordResetToken', [ 'model' => $model ] );
	}

	/**
	 * Resets password.
	 *
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token)
	{
		try
		{
			$model = new ResetPasswordForm($token);
		}
		catch ( InvalidParamException $e )
		{
			throw new BadRequestHttpException( $e->getMessage() );
		}

		if ( $model->load( Yii::$app->request->post() ) && $model->validate() && $model->resetPassword() )
		{
			Yii::$app->session->setFlash( 'success', 'New password was saved.' );

			return $this->goHome();
		}

		return $this->render( 'resetPassword', [ 'model' => $model ] );
	}
}
