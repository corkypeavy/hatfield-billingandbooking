<?php
namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\models\UserSearch;
use app\models\Phone;
use app\models\Address;
use app\models\VideoChat;
use app\models\Email;

/**
 * User controller
 */
class UserController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'index' ],
						'allow' => true,
						'roles' => [ 'superAdmin' ],
					],
					[
						'actions' => [ 'create' ],
						'allow' => true,
						'roles' => [ 'createAdmin' ],
					],
					[
						'actions' => [ 'update' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'updateAdmin', [ 'user' => $model ] );
						}
					],
					[
						'actions' => [ 'view' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'viewAdmin', [ 'user' => $model ] );
						}
					],
					[
						'actions' => [ 'delete' ],
						'allow' => true,
						'roles' => [ 'deleteAdmin' ],
					],
					[
						'actions' => [ 'delete-phone' ],
						'allow' => true,
						'roles' => '@',
					],
					[
						'actions' => [ 'delete-video-chat' ],
						'allow' => true,
						'roles' => '@',
					],
					[
						'actions' => [ 'delete-email' ],
						'allow' => true,
						'roles' => '@',
					],
					[
						'actions' => [ 'delete-address' ],
						'allow' => true,
						'roles' => '@',
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
/*	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
					'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
*/
	/**
	 * Displays list of admins
	 */
	public function actionIndex()
	{
		Yii::$app->session[ 'MyAccount' ] = FALSE;
		$searchModel = new UserSearch();
		$params = Yii::$app->request->queryParams;
		$dataProvider = $searchModel->search( $params );

		return $this->render( 'index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}

	/**
	 * Displays create admin page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new User();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{
			// by default the admin use is active and not a superAdmin
			$model->status = User::STATUS_ACTIVE;
			$model->type = User::TYPE_ADMIN;
			$model->setPassword( $model->password );
			$model->generateAuthKey();

			// if the save to the db is successful
			if ( $model->save() )
			{
				// assign the admin role to the user
				$auth = Yii::$app->authManager;
				$role = $auth->getRole( 'admin' );
				$auth->assign( $role, $model->id );
				return $this->redirect( [ 'view', 'id' => $model->id ] );
			} // if ( $model->save() )
		} // if ( $model->load( Yii::$app->request->post() ) )

		// else we are to load the create form
		else
		{
			return $this->render( 'create', [ 'model' => $model ] );
		} // else
	}

	/**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView( $id )
	{
		return $this->render( 'view', [ 'model' => $this->findModel( $id ) ] );
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate( $id )
	{
		$model = $this->findModel( $id );

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{

			// if a new password has been entered
			if ( !empty( $model->password ) )
			{
				$model->setPassword( $model->password );
			} // if ( !empty( $model->password ) )

			// if the record is saved
			if ( $model->save() )
			{

				// if the user is a super admin, we need to see if he has been assigned that role
				if ( $model->type == TYPE_SUPER_ADMIN )
				{
					$auth = Yii::$app->authManager;

					// if the superadmin assignment has not been made for this user
					if ( $auth->getAssignment( 'superAdmin', $model->id ) == null )
					{
						$role = $auth->getRole( 'superAdmin' );
						$auth->assign( $role, $model->id );
					} // if ( $auth->getAssignment( 'superAdmin', $model->id ) == null )
				} // if ( $model->type == TYPE_SUPER_ADMIN )
				return $this->redirect( [ 'view', 'id' => $model->id ] );
			} // if ( $model->save() )
		} // if ( $model->load( Yii::$app->request->post() ) )

		// else we are to load the form with the current data
		else
		{
			return $this->render( 'update', [ 'model' => $model ] );
		} // else
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete( $id )
	{
		$user = $this->findModel( $id );

		// if we found the user
		if ( !empty( $user ) )
		{
			$user->status = User::STATUS_DELETED;
			$user->save();
		} // if ( !empty( $user ) )

		return $this->redirect( [ 'index' ] );
	}

	/**
	 * Deletes a phone number from the Tutor model
	 * @return mixed
	 */
	public function actionDeletePhone( $id, $returnUrl )
	{
		$phone = Phone::findOne( $id );
		$userId = $phone->userId;
		$phone->delete();
		return $this->redirect( urldecode( $returnUrl ) );
	} 

	/**
	 * Deletes a phone number from the Tutor model
	 * @return mixed
	 */
	public function actionDeleteVideoChat( $id, $returnUrl )
	{
		$videoChat = VideoChat::findOne( $id );
		$userId = $videoChat->userId;
		$videoChat->delete();
		return $this->redirect( urldecode( $returnUrl ) );
	} 

	/**
	 * Deletes a phone number from the Tutor model
	 * @return mixed
	 */
	public function actionDeleteEmail( $id, $returnUrl )
	{
		$email = Email::findOne( $id );
		$userId = $email->userId;
		$email->delete();
		return $this->redirect( urldecode( $returnUrl ) );
	} 

	/**
	 * Deletes a phone number from the Tutor model
	 * @return mixed
	 */
	public function actionDeleteAddress( $id, $returnUrl )
	{
		$address = Address::findOne( $id );
		$userId = $address->userId;
		$address->delete();
		return $this->redirect( urldecode( $returnUrl ) );
	} 

	/**
	* Finds the User model based on its primary key value.
	* If the model is not found, a 404 HTTP exception will be thrown.
	* @param integer $id
	* @return User the loaded model
	* @throws NotFoundHttpException if the model cannot be found
	*/
	protected function findModel( $id )
	{
		// if we found a user
		if ( ( $model = User::findOne( $id ) ) !== null )
		{
			return $model;
		} // if ( ( $model = User::findOne( $id ) ) !== null )

		// else we didn't find a user with that id
		else
		{
			throw new NotFoundHttpException( 'The requested page does not exist.' );
		} // else
	}
}
