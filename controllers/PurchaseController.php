<?php

namespace app\controllers;

use Yii;
use app\models\Purchase;
use app\models\PurchaseSearch;
use app\models\helpers\TimeType;
use app\models\helpers\PurchaseType;
use app\models\helpers\PurchaseStatus;
use app\models\PaymentForm;
use app\models\Payment;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PurchaseController implements the CRUD actions for Purchase model.
 */
class PurchaseController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'pay' ],
						'allow' => true,
						'roles' => [ 'customer' ]
					],
					[
						'actions' => [ 'delete' ],
						'allow' => true,
						'roles' => [ 'admin' ],
					],
					[
						'actions' => [ 'create', 'index', 'view-payment' ],
						'allow' => true,
						'roles' => [ 'admin', 'tutor' ]
					],
					[
						'actions' => [ 'view' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'viewPurchase', [ 'tutor' => $model ] );
						}
					],
					[
						'actions' => [ 'update' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'updatePurchase', [ 'tutor' => $model ] );
						}
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}

	/**
	* Lists all Purchase models.
	* @return mixed
	*/
	public function actionIndex()
	{
		$curUser = User::findOne( Yii::$app->user->id );
		$searchModel = new PurchaseSearch();

		// if the current user is a tutor, we can only load his/her invoices
		if ( $curUser->isTutor() )
		{
			$searchModel->tutorId = $curUser->id;
		} // if ( $curUser->isTutor() )

		$dataProvider = $searchModel->searchPurchases( Yii::$app->request->queryParams );

		return $this->render( 'index', [ 'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'curUser' => $curUser ] );
	}

	protected function calcRecurringPrice( $tutorId, $minsPerWeek )
	{
		$tutor = Tutor::findOne( $tutorId );

		$minsPerMonth = $minsPerWeek * 4.35;

		// if we don't reach the minimum value for discount, use max price
		if ( $minsPerWeek < $tutor->minDiscountMinsRecurring )
		{
			return round( $tutor->maxPPMRecurring * $minsPerMonth, 2 );
		}

		// else if we exceed the maximum discount hours, use min price
		else if ( $minsPerWeek > $tutor->maxDiscountMinsRecurring )
		{
			return round( $tutor->minPPMRecurring * $minsPerMonth, 2 );		
		}

		// else somewhere in the middle
		else
		{
			$slope = ( $tutor->maxPPMRecurring - $tutor->minPPMRecurring ) / ( $tutor->minDiscountMinsRecurring - $tutor->maxDiscountMinsRecurring );

			// slope = (y2 - y1) / (x2 - x1)
			// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
			// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
			// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
			// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
			$ppm = $slope * ( $minsPerWeek - $tutor->minDiscountMinsRecurring ) + $tutor->maxPPMRecurring;
			return round( $ppm * $minsPerMonth, 2 );
		}
	}

	protected function calcFlexPrice( $tutorId, $hours )
	{
		$tutor = Tutor::findOne( $tutorId );

		if ( $hours < $tutor->minDiscountHoursFlex )
		{
			return round( $tutor->maxPPHFlex * $hours, 2 );
		}

		else if ( $hours > $tutor->maxDiscountHoursFlex )
		{
			return round( $tutor->minPPHFlex * $hours, 2 );
		}

		else
		{
			$slope = ( $tutor->maxPPHFlex - $tutor->minPPHFlex ) / ( $tutor->minDiscountHoursFlex - $tutor->maxDiscountHoursFlex );

			// slope = (y2 - y1) / (x2 - x1)
			// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
			// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
			// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
			// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
			$pph = $slope * ( $hours - $tutor->minDiscountHoursFlex ) + $tutor->maxPPHFlex;
			return round( $pph * $hours, 2 );
		}
	}

	/**
	 * Displays a single Purchase model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView( $id )
	{
		return $this->render( 'view', [ 'model' => $this->findModel( $id ) ] );
	}

	public function actionViewPayment( $paymentId )
	{
		$model = Payment::findOne( $paymentId );

		return $this->renderAjax( 'viewPayment', [ 'model' => $model ] );
	}

	/**
	 * Creates a new Purchase model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Purchase();
		$model->quantity = 1;
		$today = new \DateTime();
		$model->createdDttmUTC = $today->format( 'Y-m-d' );
		$model->status = PurchaseStatus::UNPAID;

		$subscription = new Subscription();
		$subscription->startDate = $today->format( 'Y-m-d' );
		$subscription->minsPerWeek = 0;
		$post = Yii::$app->request->post();

		// if this is a post back
		if ( $model->load( $post ) && $subscription->load( $post ) )
		{
			$bContinue = true;
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				// if the invoice is a subscription invoice
				if ( $model->type == Invoice::TYPE_RECURRING )
				{
					$subscription->customerId = $model->customerId;
					$subscription->tutorId = $model->tutorId;
					$subscription->status = Subscription::STATUS_ACTIVE;
					$model->quantity = $subscription->minsPerWeek * 4.35;

					// if we're able to save the subscription record
					if ( $subscription->save() )
					{
//TODO: maybe add a connection of the subscription to the payment
//						$model->subscriptionId = $subscription->id;

						// we need to calculate again on the server side just to make sure
						$model->totalPrice = $this->calcRecurringPrice( $model->tutorId, $subscription->minsPerWeek );//$post[ 'priceRecurring' ];
					} // if ( $subscription->save() )

					// else we can't continue
					else
					{
						$bContinue = false;
					} // else
				} // if ( $model->type == Invoice::TYPE_RECURRING )

				// else if this is a flex invoice
				else if ( $model->type == Invoice::TYPE_FLEX )
				{
					// we need to calculate again on the server side just to make sure
					$model->totalPrice = $this->calcFlexPrice( $model->tutorId, $model->quantity ); //$post[ 'priceFlex' ];
				} // else if ( $model->type == Invoice::TYPE_FLEX )

				// else this is an "other" invoice
				else
				{
					$model->quantity = 1;
					$model->totalPrice = $post[ 'priceOther' ];
				} // else

				// if we were able to save the invoice
				if ( $bContinue && $model->save() )
				{

					// if the user chose to send the invoice now
					if ( $post[ 'sendNow' ] == true )
					{
						$this->sendInvoice( $model );
					} // if ( $post[ 'sendNow' ] == true )

					$trans->commit();
					return $this->redirect( [ 'view', 'id' => $model->id ] );
				} // if ( $model->save() )

				$trans->rollback();
			} // try

			// catch all exceptions
			catch ( \Exception $e )
			{
				$trans->rollback();
			} // catch ( \Exception $e ) 
		} // if ( $model->load( Yii::$app->request->post() ) && $subscription->load( Yii::$app->request->post() )

		return $this->render( 'create', [ 'model' => $model, 'subscription' => $subscription ] );
	}

	protected function sendInvoice( $purchase )
	{
		Yii::$app->mailer->compose( 'invoice', [ 'invoice' => $purchase ] )
			->setFrom( Yii::$app->params[ 'adminEmail' ] )
			->setTo( 'evhatfield@yahoo.com' )
			->setSubject( 'Invoice #' . $purchase->id )
			->send();
	}

	/**
	 * Updates an existing Purchase model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate( $id )
	{
		$model = $this->findModel( $id );

		if ( $model->load( Yii::$app->request->post() ) && $model->save() )
		{
			return $this->redirect( [ 'view', 'id' => $model->id ] );
		}
		else
		{
			return $this->render( 'update', [ 'model' => $model ] );
		}
	}

	/**
	 * Deletes an existing Purchase model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete( $id )
	{
		$purchase = $this->findModel( $id );
		$purchase->status = PurchaseStatus::DELETED;
		$purchase->save();

		return $this->redirect( [ 'index' ] );
	}

	public function actionChoose()
	{
		return $this->render( 'choose' );
	}

	public function actionTutor( $type )
	{
		$searchModel = new TutorSearch();
		$dataProvider = $searchModel->search();

		return $this->render( 'tutor', [ 'dataProvider' => $dataProvider, 'type' => $type ] );
	}
/*
	public function actionViewTutor( $id )
	{
		$tutor = Tutor::findOne( $id );
		return $this->renderAjax( 'viewTutor', [ 'model' => $tutor ] );
	}

	public function actionRecurring( $tutorId )
	{
		$model = new Subscription();

		if ( $model->load( Yii::$app->request->post() ) )
		{
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				$model->tutorId = $tutorId;
				$model->customerId = Yii::$app->user->id;

				if ( $model->save() )
				{
					$quantity = $model->minsPerWeek * 4.35;
					$product = Product::find()
						->where( [ 'tutorId' => $tutorId, 'type' => TimeType::RECURRING ] )
						->andWhere( [ '<', 'quantityInMinutes', $quantity ] )
						->max( 'quantityInMinutes' );
					$purchase = new Purchase();
					$purchase->customerId = $model->customerId;
					$purchase->productId = $product->id;
					$purchase->tutorId = $tutorId;
					$purchase->quantity = $quantity;
					$purchase->totalPrice = $quantity * $product->pricePerMinute;
					$purchase->dateCreated = date( 'Y-m-d' );
					$purchase->status = PurchaseStatus::UNPAID;

					if ( $purchase->save() )
					{
						$trans->commit();
						return $this->redirect( [ 'pay', 'purchase' => $purchase ] );
					}
				}

				$trans->rollback();
			}
			catch ( Exception $e )
			{
				$trans->rollback();
			}
		}

		return $this->render( 'recurring', [ 'model' => $model ] );
	}

	public function actionFlex( $tutorId )
	{
		$tutor = Tutor::findOne( $tutorId );
		$model = new Flex();

		if ( $model->load( Yii::$app->request->post() ) )
		{
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				$model->tutorId = $tutorId;
				$model->customerId = Yii::$app->user->id;

				if ( $model->save() )
				{
					$quantity = $model->hours * 60;
					$product = Product::find()
						->where( [ 'tutorId' => $tutorId, 'type' => TimeType::FLEX ] )
						->andWhere( [ '<', 'quantityInMinutes', $quantity ] )
						->max( 'quantityInMinutes' );
					$purchase = new Purchase();
					$purchase->customerId = $model->customerId;
					$purchase->productId = $product->id;
					$purchase->tutorId = $tutorId;
					$purchase->quantity = $quantity;
					$purchase->totalPrice = $quantity * $product->pricePerMinute;
					$purchase->dateCreated = date( 'Y-m-d' );
					$purchase->status = PurchaseStatus::UNPAID;

					if ( $purchase->save() )
					{
						$trans->commit();
						return $this->redirect( [ 'pay', 'purchase' => $purchase ] );
					}
				}
			}
			catch ( Exception $e )
			{
				$trans->rollback();
			}
		}

		return $this->render( 'flex', [ 'model' => $model ] );
	}
*/
	public function actionPay( $id, $returnUrl = null )
	{
		$purchase = $this->findModel( $id );
		$model = new PaymentForm();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{
			$nonce = $_POST[ "payment_method_nonce" ];
			$braintree = Yii::$app->braintree;

			// let's let braintree do it's thing
			$response = $braintree->call( 'Transaction', 'sale', [
				'amount' => $purchase->totalPrice,
				'paymentMethodNonce' => $nonce,
				'options' => [
					'submitForSettlement' => true
				]
			] );

			// if braintree payment was made successfully
			if ( $response->success )
			{
				$trans = Yii::$app->db->beginTransaction();

				// try to save payment and update purchase
				try
				{
					// we first create a payment
					$payment = new Payment();
					$payment->transactionId = $response->transaction->id;
					$payment->amount = $purchase->totalPrice;
					$date = new \DateTime();
					$payment->datetimeUTC = $date->format( 'Y-m-d h:i:s' );

					// if we can save the payment
					if ( $payment->save() )
					{
						// we need to set the paymentid of the purchase
						$purchase->paymentId = $payment->id;
						$purchase->status = PurchaseStatus::PAID;
						$purchase->paidDttmUTC = $payment->datetimeUTC;

						// if we can update the purchase
						if ( $purchase->save() )
						{
							$error = false;

							// if this is a flex time purchase, we can update inventory
							if ( $purchase->type == PurchaseType::FLEX )
							{
								// if we can NOT update the inventory of the customer
								if ( !$purchase->customer->updateInventory( $purchase->tutorId, $purchase->quantity, true, InventoryTransaction::REASON_PURCHASE, $purchase->id ) )
								{
									$error = true;
								} // if ( !$purchase->customer->updateInventory( $purchase->tutorId, $purchase->quantity, true, InventoryTransaction::REASON_PURCHASE, $purchase->id ) )
							} // if ( $purchase->type == PurchaseType::FLEX )

							// if there is no error
							if ( !$error )
							{
								$trans->commit();

								// if the returnUrl value is null
								if ( $returnUrl === null )
								{
									return $this->redirect( [ 'customer/tasks' ] );
								} // if ( $returnUrl === null )

								// else there is a valid returnUrl
								else
								{
									return $this->redirect( [ Html::decode( $returnUrl ) ] );
								} // else
							} // if ( !$error )
						} // if ( $purchase->save() )
					} // if ( $payment->save() )

					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // try

				// catch all exceptions
				catch ( Exception $e )
				{
					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // catch ( Exception $e )
			} // if ( $response->success )
		} // if ( $payment->load( Yii::$app->request->post() ) )

		return $this->render( 'pay', [ 'model' => $model, 'purchase' => $purchase ] );
	}

	/**
	 * Finds the Purchase model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Purchase the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id )
	{
		if ( ( $model = Purchase::findOne( $id ) ) !== null )
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException( 'The requested page does not exist.' );
		}
	}
}
