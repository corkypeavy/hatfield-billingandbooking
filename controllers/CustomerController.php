<?php

namespace app\controllers;

use Yii;
use app\models\Customer;
use app\models\CustomerSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\TutorSearch;
use app\models\Tutor;
use app\models\User;
use app\models\Phone;
use app\models\Email;
use app\models\Address;
use app\models\VideoChat;
use app\models\SignupForm;
use app\models\Payment;
use app\models\Purchase;
use app\models\PurchaseSearch;
use app\models\CustomPurchase;
use app\models\Subscription;
use app\models\Flex;
use app\models\Recurring;
use app\models\Schedule;
use app\models\InventoryTransaction;
use app\models\Inventory;
use app\models\helpers\TimeType;
use app\models\helpers\PurchaseType;
use app\models\helpers\PurchaseStatus;
use app\models\helpers\ScheduleEvent;

use yii\helpers\Json;

use yii\helpers\VarDumper;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'index', 'create', 'emails' ],
						'allow' => true,
						'roles' => [ 'tutor' ],
					],
					[
						'actions' => [ 'tutor-scheduled', 'view-available', 'purchases', 'check-conflicts', 'history', 'view-payment', 'subscriptions', 'my-events', 'tutor-events', 'scheduled-events', 'scheduled-list' ],
						'allow' => true,
						'roles' => [ 'customer' ],
					],
					[
						'actions' => [ 'delete' ],
						'allow' => true,
						'roles' => [ 'admin' ],
					],
					[
						'actions' => [ 'add-phone', 'add-video-chat', 'add-email' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'userId' ) );
							return Yii::$app->getUser()->can( 'updateCustomer', [ 'customer' => $model ] );
						}
					],
					[
						'actions' => [ 'update' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'updateCustomer', [ 'customer' => $model ] );
						}
					],
					[
						'actions' => [ 'view' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'viewCustomer', [ 'customer' => $model ] );
						}
					],
					[
						'actions' => [ 'tasks', 'view-tutor', 'schedule', 'free-trial', 'free', 'purchase', 'tutor', 'flex', 'recurring', 'custom' ],
						'allow' => true,
						'roles' => [ 'customer' ],
					],
					[
						'actions' => [ 'signup', 'verify-email' ],
						'allow' => true,
						'roles' => [ '?' ],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
					'check-conflicts' => [ 'POST' ],
				],
			],
		];
	}

	/**
	 * Lists all Customer models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new CustomerSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );

		return $this->render( 'index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}

	/**
	 * Lists the customer's available tasks
	 */
	public function actionTasks()
	{
		$model = $this->findModel( Yii::$app->user->id );

		return $this->render( 'tasks', [ 'model' => $model ] );
	}

	/**
	 * Displays a single Customer model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView( $id )
	{
		return $this->render( 'view', [ 'model' => $this->findModel( $id ) ] );
	}

	/**
	 * Creates a new Customer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Customer();
		$user = new User();
		$phone = new Phone();
		$address = new Address();
		$videoChat = new VideoChat();

		if ( $model->load( Yii::$app->request->post() ) &&
			  $user->load( Yii::$app->request->post() ) &&
			  $phone->load( Yii::$app->request->post() ) &&
			  $address->load( Yii::$app->request->post() ) &&
			  $videoChat->load( Yii::$app->request->post() ) )
		{
			$transaction = Yii::$app->db->beginTransaction();

			try
			{
				if ( $user->save() )
				{
					$model->id = $user->id;

					if ( $model->save() )
					{
						$phone->userId = $user->id;

						if ( $phone->save() )
						{
							$address->userId = $user->id;

							if ( $address->save() )
							{
								$email = new Email();
								$email->userId = $user->id;
								$email->type = Email::$types[ 'Personal' ];
								$email->address = $user->email;

								if ( $email->save() )
								{
									$videoChat->userId = $user->id;

									if ( $videoChat->save() )
									{
										$transaction->commit();
										return $this->redirect( [ 'view', 'id' => $model->id ] );
									}
								}
							}
						}
					}
				}

				$transaction->rollback();
			}
			catch ( Exception $e )
			{
				$transaction->rollback();
			}
		}

		return $this->render( 'create', [
			'model' => $model,
			'user' => $user,
			'phone' => $phone,
			'address' => $address,
			'videoChat' => $videoChat,
		] );
	}

	/**
	 * Allows a customer to sign up for themselves
	 * @return mixed
	 */
	public function actionSignup()
	{
		$customer = new Customer();
		$user = new User();
		$phone = new Phone();
		$address = new Address();
		$videoChat = new VideoChat();

		// if we got the info from the user
		if ( $user->load( Yii::$app->request->post() ) &&
			  $customer->load( Yii::$app->request->post() ) &&
			  $phone->load( Yii::$app->request->post() ) &&
			  $address->load( Yii::$app->request->post() ) &&
			  $videoChat->load( Yii::$app->request->post() ) )
		{
			$transaction = Yii::$app->db->beginTransaction();

			try
			{
				$user->status = User::STATUS_ACTIVE;
				$user->type = User::TYPE_STUDENT;
				$user->setPassword( $user->password );
				$user->generateAuthKey();

				// if the user record was saved
				if ( $user->save() )
				{
					$customer->id = $user->id;

					// if the customer record was saved
					if ( $customer->save() )
					{
						$phone->userId = $user->id;

						// if the phone record was saved
						if ( $phone->save() )
						{
							$address->userId = $user->id;

							// if the address record was saved
							if ( $address->save() )
							{
								$email = new Email();
								$email->userId = $user->id;
								$email->type = Email::$types[ 'Personal' ];
								$email->address = $user->email;

								// if the email record was saved
								if ( $email->save() )
								{
									$videoChat->userId = $user->id;

									// if the videoChat record was saved
									if ( $videoChat->save() )
									{
										$auth = Yii::$app->authManager;
										$custRole = $auth->getRole( 'customer' );
										$auth->assign( $custRole, $user->id );
										
										$transaction->commit();
										Yii::$app->user->login( $user, 0 );
										$this->sendVerifyEmail( $user );
										return $this->redirect( [ 'tasks', 'id' => $user->id ] );
									} // if ( $videoChat->save() )
								} // if ( $email->save() )
							} // if ( $address->save() )
						} // if ( $phone->save() )
					} // if ( $customer->save() )
				} // if ( $user->save() )

				$transaction->rollback();
			} // try

			// catch all exceptions
			catch ( Exception $e )
			{
				$transaction->rollback();
			} // catch ( Exception $e )
		} // if ( $user->load( Yii::$app->request->post() ) && ....

		return $this->render( 'signup', [
			'user' => $user,
			'customer' => $customer,
			'phone' => $phone,
			'address' => $address,
			'videoChat' => $videoChat
		] );
	}

	public function sendVerifyEmail( $user )
	{
		Yii::$app->mailer->compose( 'verify-email', [ 'user' => $user ] )
			->setFrom( Yii::$app->params[ 'adminEmail' ] )
			->setTo( $user->email )
			->setSubject( 'Email Verification' )
			->send();
	}

	public function actionVerifyEmail( $email )
	{
		$user = User::find()->where( [ 'email' => urldecode( $email ) ] )->one();

		// if we found the user, log them in
		if ( !empty( $user ) )
		{
			$user->emailConfirmed = true;

			// if we can save that the email was confirmed
			if ( $user->save() )
			{
				Yii::$app->user->login( $user );
				return $this->redirect( [ 'tasks' ] );
			} // ( $user->save() )
		} // if ( !empty( $user ) )

		return $this->redirect( [ 'site/login' ] );
	}

	/**
	 * Offers a free trial to a user.
	 *
	 * @return mixed
	 */
	public function actionFreeTrial()
	{
		// if the user is already logged in
		if ( !Yii::$app->user->isGuest )
		{
			$user = User::findOne( Yii::$app->user->id );
			$searchModel = new TutorSearch();

			// make sure we only grab those tutors that are available for free
			$dataProvider = $searchModel->searchFree( $user );

			return $this->render( 'tutor', [ 'dataProvider' => $dataProvider, 'type' => 'free' ] );
		} // if ( !Yii::$app->user->isGuest )

		return $this->render( 'freeTrial' );
	}

	public function actionPurchase()
	{
		return $this->render( 'purchase' );
	}

	public function actionTutor( $type )
	{
		$searchModel = new TutorSearch();
		$dataProvider = $searchModel->search( [] );

		return $this->render( 'tutor', [ 'dataProvider' => $dataProvider, 'type' => $type ] );
	}

	public function actionFree( $tutorId )
	{
		$purchase = Purchase::find()->where( [ 'customerId' => Yii::$app->user->id, 'tutorId' => $tutorId ] )->one();

		// if we not have already made the 'free' purchase
		if ( !$purchase )
		{
			$purchase = new Purchase();
			$purchase->customerId = Yii::$app->user->id;
			$purchase->tutorId = $tutorId;
			$purchase->quantity = 1;
			$purchase->totalPrice = 0;
			$purchase->status = PurchaseStatus::FREE;
			$purchase->type = PurchaseType::FREE;
			$purchase->createdDttmUTC = date( 'Y-m-d h:i:s' );
			$purchase->notes = 'Free Trial with Tutor '. $tutorId;
			$purchase->hasViewed = true;

			// if we can save the purchase
			if ( $purchase->save() )
			{
				return $this->redirect( [ 'schedule', 'tutorId' => $tutorId, 'type' => TimeType::FREE ] );
			} // if ( $purchase->save() )
		} // if ( !$purchase )

		// else we just need to redirect to scheduling
		else
		{
			return $this->redirect( [ 'schedule', 'tutorId' => $tutorId, 'type' => TimeType::FREE ] );
		} // else

		return $this->redirect( Yii::$app->request->referrer );
	}

	public function actionFlex( $tutorId )
	{
		$tutor = Tutor::findOne( $tutorId );
		$model = new Flex();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{
			// calculate on server side just to make sure
			$price = $this->calcFlexPrice( $tutor, $model->hours );

			$nonce = $_POST[ "payment_method_nonce" ];
			$braintree = Yii::$app->braintree;

			// let's let braintree do it's thing
			$response = $braintree->call( 'Transaction', 'sale', [
				'amount' => $price,
				'paymentMethodNonce' => $nonce,
				'options' => [
					'submitForSettlement' => true
				]
			] );

			// if braintree payment was made successfully
			if ( $response->success )
			{
				$trans = Yii::$app->db->beginTransaction();

				// try to create a payment and a purchase record
				try
				{
					$payment = new Payment();
					$payment->datetimeUTC = date( 'Y-m-d h:i:s' );
					$payment->amount = $price;
					$payment->transactionId = $response->transaction->id;

					// if we can save the payment
					if ( $payment->save() )
					{
						$purchase = new Purchase();
						$purchase->customerId = Yii::$app->user->id;
						$purchase->tutorId = $tutorId;
						$purchase->quantity = $model->hours;
						$purchase->totalPrice = $price;
						$purchase->createdDttmUTC = date( 'Y-m-d h:i:s' );
						$purchase->type = PurchaseType::FLEX;
						$purchase->notes = 'Flex time purchase';
						$purchase->hasViewed = true;
						$purchase->status = PurchaseStatus::PAID;
						$purchase->paymentId = $payment->id;
						$purchase->paidDttmUTC = date( 'Y-m-d h:i:s' );

						// if we can save this purchase, let's go pay for it
						if ( $purchase->save() )
						{
							// if we can update the inventory of the customer
							if ( $purchase->customer->updateInventory( $tutorId, $purchase->quantity, true, InventoryTransaction::REASON_PURCHASE, $purchase->id ) )
							{
								$trans->commit();
								return $this->redirect( [ 'schedule', 'tutorId' => $tutorId, 'type' => TimeType::FLEX ] );
							} // if ( !$purchase->customer->updateInventory( $tutorId, $purchase->quantity, true, InventoryTransaction::REASON_PURCHASE, $purchase->id ) )
						} // if ( $purchase->save() )
					} // if ( $payment->save() )

					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // try

				// catch all exceptions
				catch ( Exception $e )
				{
					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // catch ( Exception $e )
			} // if ( $response->success )
		} // if ( $model->load( Yii::$app->request->post() ) )

		return $this->render( 'flex', [ 'model' => $model, 'tutor' => $tutor ] );
	}

	protected function calcFlexPrice( $tutor, $hours )
	{
		// if we do not reach the minimum hours
		if ( $hours < $tutor->minDiscountHoursFlex )
		{
			return round( $tutor->maxPPHFlex * $hours, 2 );
		} // if ( $hours < $tutor->minDiscountHoursFlex )

		// else we exceed the maximum discount hours
		else if ( $hours >= $tutor->maxDiscountHoursFlex )
		{
			return round( $tutor->minPPHFlex * $hours, 2 );
		} // else if ( $hours >= $tutor->maxDiscountHoursFlex )

		// else we're somewhere in the middle
		else
		{
			$slope = ( $tutor->maxPPHFlex - $tutor->minPPHFlex ) / ( $tutor->minDiscountHoursFlex - $tutor->maxDiscountHoursFlex );

			// slope = (y2 - y1) / (x2 - x1)
			// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
			// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
			// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
			// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
			$pph = $slope * ( $hours - $tutor->minDiscountHoursFlex ) + $tutor->maxPPHFlex;
			return round( $pph * $hours, 2 );
		} // else
	}

	public function actionRecurring( $tutorId )
	{
		$tutor = Tutor::findOne( $tutorId );
		$model = new Subscription();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{
			// calculate on server side just to make sure
			$price = $this->calcRecurringPrice( $tutor, $model->minsPerWeek );

			$nonce = $_POST[ "payment_method_nonce" ];
			$braintree = Yii::$app->braintree;

			// let's let braintree do it's thing
			$response = $braintree->call( 'Transaction', 'sale', [
				'amount' => $price,
				'paymentMethodNonce' => $nonce,
				'options' => [
					'submitForSettlement' => true
				]
			] );

			// if braintree payment was made successfully
			if ( $response->success )
			{
				$trans = Yii::$app->db->beginTransaction();

				// try to create a payment, a subscription, and a purchase
				try
				{
					$payment = new Payment();
					$payment->datetimeUTC = date( 'Y-m-d h:i:s' );
					$payment->amount = $price;
					$payment->transactionId = $response->transaction->id;

					// if we can save the payment
					if ( $payment->save() )
					{
						$purchase = new Purchase();
						$purchase->customerId = Yii::$app->user->id;
						$purchase->tutorId = $tutorId;
						$purchase->quantity = $model->minsPerWeek * 4.35;
						$purchase->totalPrice = $price;
						$purchase->createdDttmUTC = date( 'Y-m-d h:i:s' );
						$purchase->type = PurchaseType::RECURRING;
						$purchase->notes = 'Recurring purchase';
						$purchase->hasViewed = true;
						$purchase->status = PurchaseStatus::PAID;
						$purchase->paymentId = $payment->id;
						$purchase->paidDttmUTC = date( 'Y-m-d h:i:s' );

						// if we can save this purchase, let's go pay for it
						if ( $purchase->save() )
						{
							$model->tutorId = $tutorId;
							$model->customerId = Yii::$app->user->id;
							$model->minsAvailable = $model->minsPerWeek;

							// if the subscription is saved, we need to create an invoice
							if ( $model->save() )
							{
								$trans->commit();
								return $this->redirect( [ 'schedule', 'tutorId' => $purchase->id, 'type' => TimeType::RECURRING ] );
							} // if ( $model->save() )
						} // if ( $purchase->save() )
					} // if ( $payment->save() )

					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // try

				// catch all exceptions
				catch ( Exception $e )
				{
					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // catch ( Exception $e )
			} // if ( $response->success )
		} // if ( $model->load( Yii::$app->request->post() ) )

		return $this->render( 'recurring', [ 'model' => $model, 'tutor' => $tutor ] );
	}

	protected function calcRecurringPrice( $tutor, $minsPerWeek )
	{
		$minsPerMonth = $minsPerWeek * 4.35;

		// if we don't reach the minimum value for discount, use max price
		if ( $minsPerWeek < $tutor->minDiscountMinsRecurring )
		{
			return round( $tutor->maxPPMRecurring * $minsPerMonth, 2 );
		}

		// else if we exceed the maximum discount hours, use min price
		else if ( $minsPerWeek > $tutor->maxDiscountMinsRecurring )
		{
			return round( $tutor->minPPMRecurring * $minsPerMonth, 2 );		
		}

		// else somewhere in the middle
		else
		{
			$slope = ( $tutor->maxPPMRecurring - $tutor->minPPMRecurring ) / ( $tutor->minDiscountMinsRecurring - $tutor->maxDiscountMinsRecurring );

			// slope = (y2 - y1) / (x2 - x1)
			// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
			// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
			// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
			// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
			$ppm = $slope * ( $minsPerWeek - $tutor->minDiscountMinsRecurring ) + $tutor->maxPPMRecurring;
			return round( $ppm * $minsPerMonth, 2 );
		}
	}

	public function actionCustom()
	{
		$model = new CustomPurchase();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{
			$nonce = $_POST[ "payment_method_nonce" ];
			$braintree = Yii::$app->braintree;

			$response = $braintree->call( 'Transaction', 'sale', [
				'amount' => $model->amount,
				'paymentMethodNonce' => $nonce,
				'options' => [
					'submitForSettlement' => true
				]
			] );

			// if the braintree response was a success
			if ( $response->success )
			{
				$trans = Yii::$app->db->beginTransaction();

				// try to save payment/purchase records
				try
				{
					$today = new \DateTime();

					$payment = new Payment();
					$payment->transactionId = $response->transaction->id;
					$payment->amount = $model->amount;
					$payment->datetimeUTC = $today->format( 'Y-m-d h:i:s' );

					// if we can save the payment successfully
					if ( $payment->save() )
					{
						$purchase = new Purchase();
						$purchase->customerId = Yii::$app->user->id;
						$purchase->tutorId = $model->tutorId;
						$purchase->type = PurchaseType::OTHER;
						$purchase->quantity = 1;
						$purchase->totalPrice = floatval( $model->amount );
						$purchase->createdDttmUTC = $payment->datetimeUTC;
						$purchase->paidDttmUTC = $payment->datetimeUTC;
						$purchase->paymentId = $payment->id;
						$purchase->notes = $model->description;
						$purchase->hasViewed = true;
						$purchase->status = PurchaseStatus::PAID;

						// if we can save the purchase successfully
						if ( $purchase->save() )
						{
							$trans->commit();
							return $this->redirect( [ 'customer/tasks' ] );
						} // if ( $purchase->save() )
					} // if ( $payment->save() )

					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // try

				// catch all exceptions
				catch ( Exception $e )
				{
					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // catch ( Exception $e )
			} // if ( $response->success )
		} // if ( $model->load( Yii::$app->request->post() ) )

		return $this->render( 'custom', [ 'model' => $model ] );
	}

	/**
	 * Updates an existing Customer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate( $id )
	{
		$model = $this->findModel( $id );
		$user = User::findOne( $id );

		if ( $model->load( Yii::$app->request->post() ) && 
			  $user->load( Yii::$app->request->post() ) )
		{
			if ( $model->save() )
			{
				return $this->redirect( [ 'view', 'id' => $model->id ] );
			}
		}
		else 
		{
			return $this->render( 'update', [ 'model' => $model, 'user' => $user ] );
		}
	}

	/**
	 * Deletes an existing Customer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete( $id )
	{
		$user = User::findOne( $id );
		$user->status = User::STATUS_DELETED;
		$user->save();

		return $this->redirect( [ 'index' ] );
	}

	/**
	 * Displays a list of tutor email addresses
	 *
	 */
	public function actionEmails()
	{
		$searchModel = new CustomerSearch();
		$dataProvider = $searchModel->searchEmail( Yii::$app->request->queryParams );

		return $this->render( 'emails', [ 'searchModel' => $searchModel, 'dataProvider' => $dataProvider ] );
	}

	public function actionPurchases()
	{
		$searchModel = new PurchaseSearch();
		$dataProvider = $searchModel->searchUnpaid( Yii::$app->user->id );

		return $this->render( 'purchases', [ 'dataProvider' => $dataProvider ] );		
	}

	public function actionHistory()
	{
		$searchModel = new PurchaseSearch();
		$dataProvider = $searchModel->searchHistory( Yii::$app->user->id );

		return $this->render( 'history', [ 'dataProvider' => $dataProvider ] );
	}

	public function actionViewPayment( $paymentId )
	{
		$payment = Payment::findOne( $paymentId );

		return $this->renderAjax( 'payment', [ 'model' => $payment ] );
	}

	public function actionSubscriptions()
	{
		$query = Subscription::find()->where( [ 'customerId' => Yii::$app->user->id ] );
		$query->andWhere( [ 'not', [ 'status' => Subscription::STATUS_DELETED ] ] );
		$dataProvider = new ActiveDataProvider( [ 'query' => $query ] );
		
		return $this->render( 'subscriptions', [ 'dataProvider' => $dataProvider ] );
	}

	/* recurring events are handled differently.  If we are scheduling recurring events,
		we only show the current week's events */
	protected function getCustomerRecurringEvents( $custId, $start, $end )
	{
		$cust = $this->findModel( $custId );
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$utcTZ = new \DateTimeZone( 'UTC' );

		$startOfWeek = new \DateTime( $start, $custTZ );
		$endOfWeek = new \DateTime( $end, $custTZ );

		$startOfWeekUTC = $startOfWeek->setTimezone( $utcTZ );
		$endOfWeekUTC = $endOfWeek->setTimezone( $utcTZ );

		return Schedule::find()
			->where( [ 'customerId' => $custId, 'type' => TimeType::RECURRING ] )
			->andWhere( [ 'between', 'startDttmUTC', $startOfWeekUTC->format( 'Y-m-d h:i' ), $endOfWeekUTC->format( 'Y-m-d h:i' ) ] )
			->all();
	}

	protected function getCustomerNonRecurringEvents( $custId )
	{
		return Schedule::find()
			->where( [ 'customerId' => $custId, 'status' => Schedule::STATUS_SCHEDULED ] )
			->andWhere( [ 'not', [ 'type' => TimeType::RECURRING ] ] )
			->all();
	}

	protected function getTutorScheduledEvents( $tutorId, $custId )
	{
		return Schedule::find()
				->where( [ 'tutorId' => $tutorId, 'status' => Schedule::STATUS_SCHEDULED ] )
				->andWhere( [ 'not', [ 'customerId' => $custId ] ] )
				->all();
	} 

	protected function loadEvents( $schedules, $tutorId, $custId, $type )
	{
		$utcTZ = new \DateTimeZone( 'UTC' );
		$cust = $this->findModel( $custId );
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$events = array();

		// loop through all the schedule records
		foreach ( $schedules as $schedule )
		{
			$event = new ScheduleEvent();
			$event->id = $schedule->id;
			$event->title = $schedule->typeString();
			$start = new \DateTime( $schedule->startDttmUTC, $utcTZ );
			$start->setTimezone( $custTZ );
			$end = new \DateTime( $schedule->endDttmUTC, $utcTZ );
			$end->setTimezone( $custTZ );
			$event->start = $start->format( 'Y-m-d H:i' ); 
			$event->end = $end->format( 'Y-m-d H:i' );
			$event->subId = $schedule->subscriptionId;
			$event->alreadyScheduled = true;

			// if the event is for this tutor and for this customer and of the current type, we want to be able to edit it
			if ( $schedule->tutorId == $tutorId
					&& $schedule->customerId == $custId
					&& $schedule->type == $type
					&& $type != TimeType::FREE )
			{
				// allow the user to edit his own events
				$event->editable = true;
				$event->startEditable = true;
				$event->durationEditable = true;
			} // if ( $schedule->tutorId == $tutorId && $schedule->custId == $custId && $schedule->type == $type )

			// else we do not want to edit it
			else
			{
				$event->editable = false;
				$event->color = 'pink';
			} // else

			$events[] = $event;
		} // foreach ( $schedules as $schedule )

		return $events;
	}

	public function actionTutorScheduled( $tutorId )
	{
		// try to get the events for this tutor
		try
		{
			$schedules = Schedule::find()->where( [ 'tutorId' => $tutorId ] )->all();
			$custId = Yii::$app->user->id;
			$events = $this->loadEvents( $schedules, $tutorId, $custId, -1 );

			header( 'Content-type: application/json' );
			echo Json::encode( $events );
			Yii::$app->end();
		} // try

		// catch all exceptions
		catch ( \Exception $e )
		{
			return $this->renderContent( 'error' );
		} // catch ( \Exception $e )		
	}

	public function actionScheduledEvents( $custId, $tutorId, $type, $start, $end )
	{
		// try to load the events
		try
		{
			$schedules = $this->getCustomerRecurringEvents( $custId, $start, $end );
			$events = $this->loadEvents( $schedules, $tutorId, $custId, $type );

			$schedules = $this->getCustomerNonRecurringEvents( $custId );
			$events = array_merge( $events, $this->loadEvents( $schedules, $tutorId, $custId, $type ) );

			$schedules = $this->getTutorScheduledEvents( $tutorId, $custId );
			$events = array_merge( $events, $this->loadEvents( $schedules, $tutorId, $custId, $type ) );

			header( 'Content-type: application/json' );
			echo Json::encode( $events );
			Yii::$app->end();
		} // try

		// catch all exceptions
		catch ( \Exception $e )
		{
			return $this->renderContent( 'error' );
		} // catch ( \Exception $e )
	}


	protected function loadEventList( $schedules, $custId )
	{
		$utcTZ = new \DateTimeZone( 'UTC' );
		$cust = $this->findModel( $custId );
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$events = array();

		// loop through all the schedule records
		foreach ( $schedules as $schedule )
		{
			$event = new ScheduleEvent();
			$event->id = $schedule->id;
			$event->title = $schedule->typeString();
			$start = new \DateTime( $schedule->startDttmUTC, $utcTZ );
			$start->setTimezone( $custTZ );
			$end = new \DateTime( $schedule->endDttmUTC, $utcTZ );
			$end->setTimezone( $custTZ );
			$event->start = $start->format( 'Y-m-d H:i' ); 
			$event->end = $end->format( 'Y-m-d H:i' );
			$event->subId = $schedule->subscriptionId;
			$event->alreadyScheduled = true;

			$events[] = $event;
		} // foreach ( $schedules as $schedule )

		return $events;
	}

	public function actionScheduledList( $custId )
	{
		// try to load the events
		try
		{
			$schedules = $this->getCustomerEvents( $custId );
			$events = $this->loadEventList( $schedules, $custId );

			header( 'Content-type: application/json' );
			echo Json::encode( $events );
			Yii::$app->end();
		} // try

		// catch all exceptions
		catch ( \Exception $e )
		{
			return $this->renderContent( 'error' );
		} // catch ( \Exception $e )
	}

	public function actionSchedule( $tutorId, $type )
	{
		$tutor = Tutor::findOne( $tutorId );
		$customer = $this->findModel( Yii::$app->user->id );
		$post = Yii::$app->request->post();

		// if user clicked 'save'
		if ( !empty( $post ) )
		{
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				// delete all records for this customer and tutor and type
				Schedule::deleteAll( 'customerId = ' . $customer->id . ' AND tutorId = ' . $tutorId . ' AND Type = ' . $type );
				$cust->resetSubscriptions( $tutorId );
				$error = false;
				$warning = '';

				$numOfEvents = count( $post );

				// if we have events
				if ( $numOfEvents > 0 )
				{

					// loop thru all the items in the post
					for ( $i = 0; $i < count( $post ); $i++ )
					{

						// if this is an item for this tutor
						if ( $post[ $i ][ 'color' ] != 'pink' )
						{

							// if we're saving recurring types
							if ( $type == TimeType::RECURRING )
							{
								$result = $this->saveRecurring( $post[ $i ], $customer, $tutorId );

								// if there was an error
								if ( $result == 'error' )
								{
									$error = true;
									break;
								} // if ( $result == 'error' )

								// else if there were warnings
								else if ( $result != 'success' )
								{
									$warning = $result;
									break;
								} // else if ( $result != 'success' )
							} // if ( $type == TimeType::RECURRING )

							// else if we're scheduling makeup time
							else if ( $type == TimeType::MAKEUP )
							{
								// TODO: Need to implement makeup time
							} // else if ( $type == TimeType::MAKEUP )

							// else if we're scheduling free time
							else if ( $type == TimeType::FREE )
							{
								$result = $this->saveFree( $post[ $i ], $customer, $tutorId );

								// if we had an error saving the free trial
								if ( $result == 'error' )
								{
									$error = true;
									break;
								} // if ( $result == 'error' )
							} // else if ( $type == TimeType::FREE )

							// else we're saving flex types
							else
							{
								$result = $this->saveFlex( $post[ $i ], $customer, $tutorId );

								// if there was an error
								if ( $result == 'error' )
								{
									$error = true;
									break;
								} // if ( $result == 'error' )
							} // else
						} // if ( $post[ $i ][ 'title' ] == $tutur->user->name )
					} // for ( $i = 0; $i < count( $post ); $i++ )
				} // if ( $numOfEvents > 0 )

				// else there are no events to save - let's return a warning
				else
				{
					$warning = 'No Events Scheduled';
				} // else

				// if there was an error saving the scheduled items
				if ( $error )
				{
					$trans->rollback();
				} // if ( $error )

				else if ( !empty( $warning ) )
				{
					$trans->rollback();
					header( 'Content-type: application/json' );
					echo Json::encode( $warning );
					Yii::$app->end();
				} // else if ( !empty( $warning ) )

				// else everything was successful
				else
				{
					$trans->commit();
					header( 'Content-type: application/json' );
					echo Json::encode( 'success' );
					Yii::$app->end();
				} // else
			} // try

			// catch any excpetions
			catch ( Exception $e )
			{
				$trans->rollback();
			} // catch ( Exception $e )

			header( 'Content-type: application/json' );
			echo Json::encode( 'error' );
			Yii::$app->end();
		}

		// else this is just the initial load
		else
		{
			return $this->render( 'schedule', [
				'cust' => $customer,
				'tutor' => $tutor,
				'type' => $type,
			] );
		}
	}

	protected function saveFree( $event, $cust, $tutorId )
	{
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$utcTZ = new \DateTimeZone( 'UTC' );

		// get utc start/end date/time
		$startDate = new \DateTime( $event[ 'start' ], $custTZ );
		$startUTC = $startDate->setTimezone( $utcTZ );
		$endDate = new \DateTime( $event[ 'end' ], $custTZ );
		$endUTC = $endDate->setTimezone( $utcTZ );

		$schedule = new Schedule();
		$schedule->tutorId = $tutorId;
		$schedule->customerId = $cust->id;
		$schedule->type = TimeType::FREE;
		$schedule->status = Schedule::STATUS_SCHEDULED;
		$schedule->startDttmUTC = $startUTC->format( 'Y-m-d H:i' );
		$schedule->endDttmUTC = $endUTC->format( 'Y-m-d H:i' );

		// if save was not successful
		if ( $schedule->save() )
		{
			return 'success';
		} // if ( !$at->save() )

		return 'error';
	}

	protected function saveFlex( $event, $cust, $tutorId )
	{
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$utcTZ = new \DateTimeZone( 'UTC' );

		// get utc start/end date/time
		$startDate = new \DateTime( $event[ 'start' ], $custTZ );
		$startUTC = $startDate->setTimezone( $utcTZ );
		$endDate = new \DateTime( $event[ 'end' ], $custTZ );
		$endUTC = $endDate->setTimezone( $utcTZ );

		$schedule = new Schedule();
		$schedule->tutorId = $tutorId;
		$schedule->customerId = $cust->id;
		$schedule->type = TimeType::FLEX;
		$schedule->status = Schedule::STATUS_SCHEDULED;
		$schedule->startDttmUTC = $startUTC->format( 'Y-m-d H:i' );
		$schedule->endDttmUTC = $endUTC->format( 'Y-m-d H:i' );

		// if save was not successful
		if ( $schedule->save() )
		{

			// if we can update the inventory successfully
			if ( $cust->updateInventory( $tutorId, 1, false, InventoryTransaction::REASON_SCHEDULED ) )
			{
				return 'success';
			} // if ( $cust->updateInventory( $tutorId, 1, false, null, InventoryTransaction::REASON_SCHEDULED ) )
		} // if ( !$at->save() )

		return 'error';
	}

	protected function saveRecurring( $event, $cust, $tutorId )
	{
		$error = false;
		$warningMsg = '';
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$utcTZ = new \DateTimeZone( 'UTC' );

		// get utc start/end date/time
		$startDate = new \DateTime( $event[ 'start' ], $custTZ );
		$startUTC = $startDate->setTimezone( $utcTZ );
		$endDate = new \DateTime( $event[ 'end' ], $custTZ );
		$endUTC = $endDate->setTimezone( $utcTZ );

		$duration = $endUTC->diff( $startUTC, true ) - 5; // we subtract 5 due to the 5-minute break between classes

		// if we are NOT able to update the subscription
		if ( !$cust->updateSubscription( $event[ 'subId' ], $duration ) )
		{
			return 'error';
		} // if ( !$cust->updateSubscription( $event[ 'subId' ], $duration ) )

		// get ending date for subscription
		$endRecurringDate = $cust->getSubscriptionEndDateForTutor( $tutorId, $custTZ );

		$today = new \DateTime( 'now', $utcTZ );
		$week = new \DateInterval( 'P7D' );

		// loop until we reach the end date of the subscription
		while ( !$error && $startDate < $endRecurringDate )
		{
			// if the starting date is less than today, we need to add a week and start next week
			if ( $startUTC < $today )
			{
				$startUTC->add( $week );
				$endUTC->add( $week );
				continue;
			} // if ( $startUTC < $today )

			$schedule = new Schedule();
			$schedule->tutorId = $tutorId;
			$schedule->customerId = $cust->id;
			$schedule->type = TimeType::RECURRING;
			$schedule->status = Schedule::STATUS_SCHEDULED;
			$schedule->startDttmUTC = $startUTC->format( 'Y-m-d H:i' );
			$schedule->endDttmUTC = $endUTC->format( 'Y-m-d H:i' );
			$schedule->subscriptionId = $event[ 'subId' ];

			// if there is a scheduling conflict
			if ( $schedule->isConflict() )
			{
				$warning .= 'Warning:  Your ' . $startDate->format( 'l, H:i' ) . ' class may have some future conflicts.  If you want to continue, click the "Force Save" button.\n';
			} // if ( $schedule->isConflict() )

			// if save was not successful
			if ( !$schedule->save() )
			{
				$error = true;
			} // if ( !$at->save() )

			$startUTC->add( $week );
			$endUTC->add( $week );
		} // while ( !$error && $startDate < $endRecurringDate )

		// if an error has occurred
		if ( $error )
		{
			return 'error';
		} // if ( $error )

		// else if there has been a warning
		else if ( !empty( $warningMsg ) )
		{
			return $warningMsg;
		} // else if ( !empty( $warning ) )

		// else no problems
		else
		{
			return 'success';
		} // else
	}

	protected function actionScheduled( $tutorId, $type )
	{
		// if we just saved a free trial
		if ( $type == TimeType::FREE )
		{
			$cust = $this->findModel( Yii::$app->user->id );

			// if this is a new customer
			if ( $cust->isNew() )
			{
				$tutor = Tutor::findOne( $tutorId );

				// to customer
				Yii::$app->mailer->compose( 'scheduled-new', [ 'cust' => $cust, 'tutor' => $tutor, 'type' => 'customer' ] )
					->setFrom( Yii::$app->params[ 'adminEmail' ] )
					->setTo( [ $cust->user->email => $cust->user->name ] )
					->setSubject( 'Welcome to Spanish for Good' )
					->send();

				// to tutor
				Yii::$app->mailer->compose( 'scheduled-new', [ 'cust' => $cust, 'tutor' => $tutor, 'type' => 'tutor' ] )
					->setFrom( Yii::$app->params[ 'adminEmail' ] )
					->setTo( [ $tutor->user->email => $tutor->user->name ] )
					->setSubject( 'New Student' )
					->send();

				// to admin
				Yii::$app->mailer->compose( 'scheduled-new', [ 'cust' => $cust, 'tutor' => $tutor, 'type' => 'admin' ] )
					->setFrom( Yii::$app->params[ 'adminEmail' ] )
					->setTo( [ Yii::$app->params[ 'adminEmail' ] => 'Admin' ] )
					->setSubject( 'New Student' )
					->send();
			} // if ( !Purchase::find()->where( [ 'customerId' => $cust->id ] )->andWhere( [ 'not', [ 'type' => PurchaseType::FREE ] ] )->exists() )

			return $this->redirect( 'tasks' );
		} // if ( $type == TimeType::FREE )
	}

	protected function actionCalendarList()
	{
		$cust = $this->findModel( Yii::$app->user->id );

		return $this->render( 'calendarList', [ 'cust' => $cust ] );
	}

	protected function actionCancel( $id )
	{
		$schedule = Schedule::findOne( $id );
		$cust = Customer::findOne( Yii::$app->user->id );

		$now = new DateTime( 'now', new DateTimeZone( 'UTC' ) );

		// if the current time is past the cancel time
		if ( $schedule->datetimeUTC->sub( new \DateInterval( 'P1H' ) ) < $now  )
		{
			return $this->renderContent( 'error: too late to cancel' );
		} // if ( $schedule->datetimeUTC->sub( new \DateInterval( 'P10I' ) ) < $now  )

		// if we can't find the schedule
		if ( !isset( $schedule ) )
		{
			throw new Exception( 'Invalid Schedule Id' );
		} // if ( !isset( $schedule ) )

		// what is the type
		switch ( $schedule->type )
		{
			case TimeType::RECURRING:
				// TODO: need to handle each a little differently
				//$this->updateFlexInventory( $cust, $schedule->tutorId, true, InventoryTransaction::REASON_CANCELLED );
				break;

			case TimeType::FLEX:
				$this->updateFlexInventory( $cust, $schedule->tutorId, true, InventoryTransaction::REASON_CANCELLED );
				break;

			case TimeType::FREE:
				$schedule->delete();
				break;

			case TimeType::MAKEUP:
				$this->updateFlexInventory( $cust, $schedule->tutorId, true, InventoryTransaction::REASON_CANCELLED );
				break;

			default:
				throw new Exception( 'Invalid Schedule Type' );
		} // switch ( $schedule->type )

		// send email to tutor 
		Yii::$app->mailer->compose( 'cancel', [ 'schedule' => $schedule, 'type' => 'tutor'  ] )
			->setTo( $schedule->tutor->user->email )
			->setFrom( $cust->user->email )
			->setSubject( 'Class Cancellation' )
			->send();

		return $this->renderContent( 'success' );
	}

	protected function updateFlexInventory( $cust, $tutorId, $add, $reason, $purchaseId = null)
	{
		$cust->updateInventory( $tutorId, $quantity, $add, $reason, $purchaseId );
	}

	public function actionCheckConflicts()
	{
		$post = Yii::$app->request->post();
		$cust = $this->findModel( Yii::$app->user->id );
		$custTZ = new \DateTimeZone( $cust->user->timezone );
		$utcTZ = new \DateTimeZone( 'UTC' );

		if ( !empty( $post ) )
		{
			$start = new \DateTime( $post[ 'start' ], $custTZ );
			$end = new \DateTime( $post[ 'end' ], $custTZ );

			if ( $cust->hasConflicts( $start->setTimezone( $utcTZ ), $end->setTimezone( $utcTZ ) ) )
			{
				header( 'Content-type: application/json' );
				echo Json::encode( 'warning' );
				Yii::$app->end();
			}

			else
			{
				header( 'Content-type: application/json' );
				echo Json::encode( 'success' );
				Yii::$app->end();
			}
		}
		else
		{
			header( 'Content-type: application/json' );
			echo Json::encode( 'error' );
			Yii::$app->end();
		}
	}

	public function actionViewTutor( $id )
	{
		$tutor = Tutor::findOne( $id );
		return $this->renderAjax( 'viewTutor', [ 'model' => $tutor ] );
	}

	public function actionAddEmail( $userId, $returnUrl )
	{
		$email = new Email();

		if ( $email->load( Yii::$app->request->post() ) )
		{
			$email->userId = $userId;
			$email->save();
			return $this->redirect( $returnUrl ? urldecode( $returnUrl ) : [ 'view', 'id' => $userId ] );
		}
		else
		{
			return $this->renderAjax( 'email', [ 'model' => $email ] );
		}
	}

	public function actionAddPhone( $userId, $returnUrl )
	{
		$phone = new Phone();

		if ( $phone->load( Yii::$app->request->post() ) )
		{
			$phone->userId = $userId;
			$phone->save();
			return $this->redirect( $returnUrl ? urldecode( $returnUrl ) : [ 'view', 'id' => $userId ] );
		}
		else //if ( Yii::$app->request->isAjax )
		{
			return $this->renderAjax( 'phone', [ 'model' => $phone ] );
		}
	}

	public function actionAddVideoChat( $userId )
	{
		$videoChat = new VideoChat();

		if ( $videoChat->load( Yii::$app->request->post() ) )
		{
			$videoChat->userId = $userId;
			$videoChat->save();
			return $this->redirect( $returnUrl ? urldecode( $returnUrl ) : [ 'view', 'id' => $userId ] );
		}
		else //if ( Yii::$app->request->isAjax )
		{
			return $this->renderAjax( 'videoChat', [ 'model' => $videoChat ] );
		}
	}

	public function actionAddAddress( $userId )
	{
		$address = new Address();

		if ( $address->load( Yii::$app->request->post() ) )
		{
			$address->userId = $userId;
			$address->save();
			return $this->redirect( $returnUrl ? urldecode( $returnUrl ) : [ 'view', 'id' => $userId ] );
		}
		else //if ( Yii::$app->request->isAjax )
		{
			return $this->renderAjax( 'address', [ 'model' => $address ] );
		}
	}

	public function actionViewAvailable( $id )
	{
		$tutor = Tutor::findOne( $id );

		$this->renderAjax( 'viewAvailable', [ 'tutor' => $tutor ] );
	}

	/**
	 * Finds the Customer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Customer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id )
	{
		if ( ( $model = Customer::findOne( $id ) ) !== null )
		{
			return $model;
		}
		else 
		{
			throw new NotFoundHttpException( 'The requested page does not exist.' );
		}
	}
}
