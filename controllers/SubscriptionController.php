<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Subscription;

class SubscriptionController extends Controller
{
 	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'pause', 'unpause', 'renew' ],
						'allow' => true,
						'roles' => [ 'customer' ]
					],
				],
			],
/*			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'pause' => [ 'POST' ],
					'unpause' => [ 'POST' ],
				],
			], */
		];
	}

	public function actionPause( $id )
	{
		$model = $this->findModel( $id );
		$model->status = Subscription::STATUS_PAUSED;
		$model->pauseDttmUTC = date( 'Y-m-d h:i:s' );
		$model->save();

		return $this->redirect( [ 'customer/subscriptions' ] );
	}

	public function actionUnpause( $id )
	{
		$model = $this->findModel( $id );
		$model->status = Subscription::STATUS_ACTIVE;
		$model->pauseDttmUTC = null;
		$model->save();

		return $this->redirect( [ 'customer/subscriptions' ] );
	}

	/**
	 * Finds the Subscription model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Customer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id )
	{
		if ( ( $model = Subscription::findOne( $id ) ) !== null )
		{
			return $model;
		}
		else 
		{
			throw new NotFoundHttpException( 'The requested page does not exist.' );
		}
	}
}
