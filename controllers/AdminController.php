<?php
namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\UserSearch;
use app\models\User;
use app\models\Dunning;

/**
 * Admin controller
 */
class AdminController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'index', 'dunning' ],
						'allow' => true,
						'roles' => [ 'admin' ],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Shows all the options for an admin user
	 *
	 * returns mixed
	 */
	public function actionIndex()
	{
		return $this->render( 'index' );
	}

	public function actionDunning()
	{
		$post = Yii::$app->request->post();

		// if this is a post back
		if ( !empty( $post ) )
		{
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				// delete all records for this customer and tutor and type
				Dunning::deleteAll();

				// loop thru all the items in the post
				for ( $i = 0; $i < count( $post ); $i++ )
				{
					$dunning = new Dunning();
					$dunning->step = $post[ $i ][ 'step' ];
					$dunning->days = $post[ $i ][ 'days' ];
					$dunning->message = $post[ $i ][ 'message' ];
					if ( !$dunning->save() )
					{
						$error = true;
						break;
					}
				} // for ( $i = 0; $i < count( $post ); $i++ )

				if ( $error )
				{
					$trans->rollback();
				}
				else
				{
					$trans->commit();
					return $this->renderContent( 'success' );
				}
			} // try

			// catch all exceptions
			catch ( Exception $e )
			{
				$trans->rollback();
			} // catch ( Exception $e )

			return $this->renderContent( 'error' );
		} // if ( !empty( $post ) )

		return $this->render( 'dunning' );
	}
}
