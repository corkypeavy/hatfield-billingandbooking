<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use app\models\Invoice;
use app\models\InvoiceSearch;
use app\models\Subscription;
use app\models\Tutor;
use app\models\User;
use app\models\PaymentForm;
use app\models\Payment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\helpers\VarDumper;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'pay' ],
						'allow' => true,
						'roles' => [ 'customer' ]
					],
					[
						'actions' => [ 'delete' ],
						'allow' => true,
						'roles' => [ 'admin' ],
					],
					[
						'actions' => [ 'create', 'index' ],
						'allow' => true,
						'roles' => [ 'admin', 'tutor' ]
					],
					[
						'actions' => [ 'view' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'viewInvoice', [ 'tutor' => $model ] );
						}
					],
					[
						'actions' => [ 'update' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'updateInvoice', [ 'tutor' => $model ] );
						}
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}

	/**
	 * Lists all Invoice models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$curUser = User::findOne( Yii::$app->user->id );
		$searchModel = new InvoiceSearch();

		// if the current user is a tutor, we can only load his/her invoices
		if ( $curUser->isTutor() )
		{
			$searchModel->tutorId = $curUser->id;
		} // if ( $curUser->isTutor() )

		$dataProvider = $searchModel->searchInvoices( Yii::$app->request->queryParams );

		return $this->render( 'index', [ 'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'curUser' => $curUser ] );
	}

	/**
	 * Displays a single Invoice model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView( $id )
	{
		return $this->render( 'view', [ 'model' => $this->findModel( $id ) ] );
	}

	protected function calcRecurringPrice( $tutorId, $minsPerWeek )
	{
		$tutor = Tutor::findOne( $tutorId );

		$minsPerMonth = $minsPerWeek * 4.35;

		// if we don't reach the minimum value for discount, use max price
		if ( $minsPerWeek < $tutor->minDiscountMinsRecurring )
		{
			return round( $tutor->maxPPMRecurring * $minsPerMonth, 2 );
		}

		// else if we exceed the maximum discount hours, use min price
		else if ( $minsPerWeek > $tutor->maxDiscountMinsRecurring )
		{
			return round( $tutor->minPPMRecurring * $minsPerMonth, 2 );		
		}

		// else somewhere in the middle
		else
		{
			$slope = ( $tutor->maxPPMRecurring - $tutor->minPPMRecurring ) / ( $tutor->minDiscountMinsRecurring - $tutor->maxDiscountMinsRecurring );

			// slope = (y2 - y1) / (x2 - x1)
			// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
			// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
			// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
			// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
			$ppm = $slope * ( $minsPerWeek - $tutor->minDiscountMinsRecurring ) + $tutor->maxPPMRecurring;
			return round( $ppm * $minsPerMonth, 2 );
		}
	}

	protected function calcFlexPrice( $tutorId, $hours )
	{
		$tutor = Tutor::findOne( $tutorId );

		if ( $hours < $tutor->minDiscountHoursFlex )
		{
			return round( $tutor->maxPPHFlex * $hours, 2 );
		}

		else if ( $hours > $tutor->maxDiscountHoursFlex )
		{
			return round( $tutor->minPPHFlex * $hours, 2 );
		}

		else
		{
			$slope = ( $tutor->maxPPHFlex - $tutor->minPPHFlex ) / ( $tutor->minDiscountHoursFlex - $tutor->maxDiscountHoursFlex );

			// slope = (y2 - y1) / (x2 - x1)
			// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
			// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
			// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
			// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
			$pph = $slope * ( $hours - $tutor->minDiscountHoursFlex ) + $tutor->maxPPHFlex;
			return round( $pph * $hours, 2 );
		}
	}

	/**
	 * Creates a new Invoice model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Invoice();
		$model->quantity = 1;
		$today = new \DateTime();
		$model->createdDate = $today->format( 'Y-m-d' );
		$model->status = Invoice::STATUS_UNPAID;

		$subscription = new Subscription();
		$subscription->startDate = $today->format( 'Y-m-d' );
		$subscription->minsPerWeek = 0;
		$post = Yii::$app->request->post();

		// if this is a post back
		if ( $model->load( $post ) && $subscription->load( $post ) )
		{
			$bContinue = true;
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				// if the invoice is a subscription invoice
				if ( $model->type == Invoice::TYPE_RECURRING )
				{
					$subscription->customerId = $model->customerId;
					$subscription->tutorId = $model->tutorId;
					$subscription->status = Subscription::STATUS_ACTIVE;
					$model->quantity = $subscription->minsPerWeek * 4.35;

					// if we're able to save the subscription record
					if ( $subscription->save() )
					{
						$model->subscriptionId = $subscription->id;

						// we need to calculate again on the server side just to make sure
						$model->price = $this->calcRecurringPrice( $model->tutorId, $subscription->minsPerWeek );//$post[ 'priceRecurring' ];
					} // if ( $subscription->save() )

					// else we can't continue
					else
					{
						$bContinue = false;
					} // else
				} // if ( $model->type == Invoice::TYPE_RECURRING )

				// else if this is a flex invoice
				else if ( $model->type == Invoice::TYPE_FLEX )
				{
					// we need to calculate again on the server side just to make sure
					$model->price = $this->calcFlexPrice( $model->tutorId, $model->quantity ); //$post[ 'priceFlex' ];
				} // else if ( $model->type == Invoice::TYPE_FLEX )

				// else this is an "other" invoice
				else
				{
					$model->quantity = 1;
					$model->price = $post[ 'priceOther' ];
				} // else

				// if we were able to save the invoice
				if ( $bContinue && $model->save() )
				{

					// if the user chose to send the invoice now
					if ( $post[ 'sendNow' ] == true )
					{
						$this->sendInvoice( $model );
					} // if ( $post[ 'sendNow' ] == true )

					$trans->commit();
					return $this->redirect( [ 'view', 'id' => $model->id ] );
				} // if ( $model->save() )

				$trans->rollback();
			} // try

			// catch all exceptions
			catch ( \Exception $e )
			{
				$trans->rollback();
			} // catch ( \Exception $e ) 
		} // if ( $model->load( Yii::$app->request->post() ) && $subscription->load( Yii::$app->request->post() )

		return $this->render( 'create', [ 'model' => $model, 'subscription' => $subscription ] );
	}

	protected function sendInvoice( $invoice )
	{
		Yii::$app->mailer->compose( 'invoice', [ 'invoice' => $invoice ] )
			->setFrom( Yii::$app->params[ 'adminEmail' ] )
			->setTo( 'evhatfield@yahoo.com' )
			->setSubject( 'Invoice #' . $invoice->id )
			->send();
	}

	/**
	 * Updates an existing Invoice model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate( $id )
	{
		$model = $this->findModel( $id );

		if ( $model->load( Yii::$app->request->post() ) && $model->save() )
		{
			return $this->redirect( [ 'view', 'id' => $model->id ] );
		}
		else
		{
			return $this->render( 'update', [ 'model' => $model ] );
		}
	}

	public function actionPay( $id, $returnUrl = null )
	{
		$invoice = $this->findModel( $id );
		$model = new PaymentForm();

		// if this is a post back
		if ( $model->load( Yii::$app->request->post() ) )
		{
			$nonce = $_POST[ "payment_method_nonce" ];
			$braintree = Yii::$app->braintree;

			$response = $braintree->call( 'Transaction', 'sale', [
				'amount' => $invoice->price,
				'paymentMethodNonce' => $nonce,
				'options' => [
					'submitForSettlement' => true
				]
			] );

			// if braintree payment was made successfully
			if ( $response->success )
			{
				$trans = Yii::$app->db->beginTransaction();

				// try to save payment and update invoice
				try
				{
					$payment = new Payment();
					$payment->transactionId = $response->transaction->id;
					$payment->amount = $invoice->price;
					$date = new \DateTime();
					$payment->datetimeUTC = $date->format( 'Y-m-d h:i:s' );

					// if we can save the payment
					if ( $payment->save() )
					{
						$invoice->paymentId = $payment->id;
						$invoice->status = Invoice::STATUS_PAID;

						// if we can update the invoice
						if ( $invoice->save() )
						{
							$trans->commit();

							// if the returnUrl value is null
							if ( $returnUrl === null )
							{
								return $this->redirect( [ 'customer/tasks' ] );
							} // if ( $returnUrl === null )

							// else there is a valid returnUrl
							else
							{
								return $this->redirect( [ Html::decode( $returnUrl ) ] );
							} // else
						} // if ( $invoice->save() )
					} // if ( $payment->save() )
					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // try

				// catch all exceptions
				catch ( Exception $e )
				{
					$trans->rollback();
					$braintree->call( 'Transaction', 'void', $response->transaction->id );
				} // catch ( Exception $e )
			} // if ( $response->success )
		} // if ( $payment->load( Yii::$app->request->post() ) )

		return $this->render( 'pay', [ 'model' => $model, 'invoice' => $invoice ] );
	}

	/**
	 * Deletes an existing Invoice model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete( $id )
	{
		$inv = $this->findModel( $id );
		$inv->status = STATUS_DELETED;
		$inv->save();

		return $this->redirect( [ 'index' ] );
	}

	/**
	 * Finds the Invoice model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Invoice the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id )
	{
		if ( ( $model = Invoice::findOne( $id ) ) !== null )
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException( 'The requested page does not exist.' );
		}
	}
}
