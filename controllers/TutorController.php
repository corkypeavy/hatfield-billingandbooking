<?php

namespace app\controllers;

use Yii;
use app\models\Tutor;
use app\models\TutorSearch;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Phone;
use app\models\VideoChat;
use app\models\AvailableTime;
use yii\web\UploadedFile;
use yii\web\Response;
use yii2fullcalendar\models\Event;

use yii\helpers\VarDumper;

/**
 * TutorController implements the CRUD actions for Tutor model.
 */
class TutorController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'prices' ],
						'allow' => true,
						'roles' => [ '@' ]
					],
					[
						'actions' => [ 'view' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'viewTutor', [ 'tutor' => $model ] );
						}
					],
					[
						'actions' => [ 'index', 'create', 'emails', 'delete' ],
						'allow' => true,
						'roles' => [ 'admin' ],
					],
					[
						'actions' => [ 'contact-info' ],
						'allow' => true,
						'roles' => [ 'customer' ]
					],
					[
						'actions' => [ 'add-phone', 'add-video-chat', 'add-email' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'userId' ) );
							return Yii::$app->getUser()->can( 'updateTutor', [ 'tutor' => $model ] );
						}
					],
					[
						'actions' => [ 'update', 'available' ],
						'allow' => true,
						'matchCallback' => function ( $rule, $action ) {
							$model = $this->findModel( Yii::$app->getRequest()->get( 'id' ) );
							return Yii::$app->getUser()->can( 'updateTutor', [ 'tutor' => $model ] );
						}
					],
					[
						'actions' => [ 'tasks' ],
						'allow' => true,
						'roles' => [ 'tutor' ],
					],
					[
						'actions' => [ 'emails' ],
						'allow' => true,
						'roles' => [ 'admin' ],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}

	/**
	 * Lists all Tutor models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new TutorSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );

		return $this->render( 'index', [ 'searchModel' => $searchModel, 'dataProvider' => $dataProvider ] );
	}

	// only accessible to logged in customers
	public function actionContactInfo()
	{
		$searchModel = new TutorSearch();
		$dataProvider = $searchModel->searchCustomerTutors( Yii::$app->user->id );

		return $this->render( 'contactInfo', [ 'dataProvider' => $dataProvider ] );
	}

	/**
	 * Displays a single Tutor model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView( $id )
	{
		return $this->render( 'view', [ 'model' => $this->findModel( $id ) ] );
	}

	/**
	 * Creates a new Tutor model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Tutor();
		$user = new User();
		$phone = new Phone();
		$videoChat = new VideoChat();

		// load the default prices
		$model->loadDefaultPrices();

		// if we loaded the information from the post back
		if ( $model->load( Yii::$app->request->post() ) &&
			  $user->load( Yii::$app->request->post() ) &&
			  $phone->load( Yii::$app->request->post() ) &&
			  $videoChat->load( Yii::$app->request->post() ) )
		{
			$user->setPassword( $user->password );
			$user->type = User::TYPE_TUTOR;

			$transaction = Yii::$app->db->beginTransaction();

			try
			{
				// if the user model was saved
				if ( $user->save() )
				{
					$model->id = $user->id;
					$model->imageFile = UploadedFile::getInstance( $model, 'imageFile' );

					// if the image file is not empty
					if ( !empty( $model->imageFile ) )
					{
						// if we successfully saved the image
						if ( !$model->upload() )
						{
							$transaction->rollback();
							//TODO: Probably need to show some kind of error
							return $this->refresh();
						} // if ( !$model->upload() )
					} // if ( !empty( $model->imageFile ) )

					// if the tutor model itself was saved
					if ( $model->save() )
					{
						$auth = Yii::$app->authManager;
						$tutorRole = $auth->getRole( 'tutor' );
						$auth->assign( $tutorRole, $model->id );
						$phone->userId = $user->id;

						// if the phone record was saved
						if ( $phone->save() )
						{
							$videoChat->userId = $user->id;
							$videoChat->isPrimary = true;

							// if the video chat was saved
							if ( $videoChat->save() )
							{
								$transaction->commit();
								return $this->redirect( [ 'view', 'id' => $model->id ] );
							} // if ( $videoChat->save() )
						} // if ( $phone->save() )
					} // if ( $model->save() )
				} // if ( $user->save() )

				$transaction->rollback();
				return $this->refresh();
			} // try
			// let's catch all exceptions and rollback the transaction
			catch ( Exception $e )
			{
				$transaction->rollback();
			} // catch ( Exception $e )
		} // if ( $model->load( Yii::$app->request->post() ) &&.......

		return $this->render( 'create', [ 'model' => $model, 'user' => $user, 'phone' => $phone, 'videoChat' => $videoChat ] );
	}

	/**
	 * Updates an existing Tutor model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate( $id )
	{
		$user = User::findOne( $id );
		$model = $this->findModel( $id );
//		$phones = Phone::find()->where( [ 'userId' => $id ] )->all();
//		$videoChats = VideoChat::find()->where( [ 'userId' => $id ] )->all();

		if ( $model->load( Yii::$app->request->post() ) &&
			  $user->load( Yii::$app->request->post() ) )
		{
			$transaction = Yii::$app->db->beginTransaction();

			try
			{
				if ( $user->save() )
				{
					$model->imageFile = UploadedFile::getInstance( $model, 'imageFile' );
					$canContinue = FALSE;

					if ( !empty( $model->imageFile ) )
					{
						if ( !$model->upload() )
						{
							$transaction->rollback();
							return $this->refresh();
						}
					}

					if ( $model->save() )
					{
						$transaction->commit();
						return $this->redirect( [ 'view', 'id' => $model->id ] );
					}
				}

				$transaction->rollback();
				return $this->refresh();
			}
			catch ( Exception $e )
			{
				$transaction->rollback();
			}
		}

		return $this->render( 'update', [ 'model' => $model, 'user' => $user ] );
	}

	public function actionTasks()
	{
		$model = $this->findModel( Yii::$app->user->id );

		return $this->render( 'tasks', [ 'model' => $model ] );
	}

	public function actionAddPhone( $userId, $returnUrl )
	{
		$phone = new Phone();

		if ( $phone->load( Yii::$app->request->post() ) )
		{
			$phone->userId = $userId;
			$phone->save();
			return $this->redirect( $returnUrl ? urldecode( $returnUrl ) : [ 'view', 'id' => $userId ] );
		}
		else //if ( Yii::$app->request->isAjax )
		{
			return $this->renderAjax( 'phone', [ 'model' => $phone ] );
		}
	}

	public function actionAddVideoChat( $userId )
	{
		$videoChat = new VideoChat();

		if ( $videoChat->load( Yii::$app->request->post() ) )
		{
			$videoChat->userId = $userId;
			$videoChat->save();
			return $this->redirect( [ 'view', 'id' => $userId ] );
		}
		else //if ( Yii::$app->request->isAjax )
		{
			return $this->renderAjax( 'videoChat', [ 'model' => $videoChat ] );
		}
	}

	/**
	 * Deletes an existing Tutor model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete( $id )
	{
		$user = User::findOne( $id );
		$user->status = User::STATUS_DELETED;
		$user->save();

		return $this->redirect( [ 'index' ] );
	}

	/**
	 * Displays a list of tutor email addresses
	 *
	 */
	public function actionEmails()
	{
		$searchModel = new TutorSearch();
		$dataProvider = $searchModel->searchEmail( Yii::$app->request->queryParams );

		return $this->render( 'emails', [ 'searchModel' => $searchModel, 'dataProvider' => $dataProvider ] );
	}

	private function convertDOWToNum( $dow )
	{
		switch ( $dow )
		{
			case 'Sunday':
				return 0;

			case 'Monday':
				return 1;

			case 'Tuesday':
				return 2;

			case 'Wednesday':
				return 3;

			case 'Thursday':
				return 4;

			case 'Friday':
				return 5;

			case 'Saturday':
				return 6;

			default:
				throw new \Exception;
		}
	}

	private function getLocalDateTime( $time, $dayOfWeek, $tutorTimezone )
	{
		// get current date/time of tutor
		$curDate = new \DateTime( "now", new \DateTimeZone( $tutorTimezone ) );

		// get current day of week
		$curDOW = $this->convertDOWToNum( $curDate->format( 'l' ) );

		// find the difference between the available time day of week and the current day of week 
		$diff = $dayOfWeek - $curDOW;
		$interval = new \DateInterval( 'P' . abs( $diff ) . 'D' );

		// if the difference is negative, we will need to invert the interval
		if ( $diff < 0 )
		{
			$interval->invert = true;
		} // if ( $diff < 0 )

		// calculate the new date
		$newDate = $curDate->add( $interval );

		// set the time of th new date
		$arr = explode( ':', $time );
		$newDate->setTime( intval( $arr[ 0 ] ), intval( $arr[ 1 ] ) );

		return $newDate;
	}

	public function actionPrices( $id )
	{
		$tutor = $this->findModel( $id );

		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'minPPMRecurring' => $tutor->minPPMRecurring,
			'maxPPMRecurring' => $tutor->maxPPMRecurring,
			'minMinsForDiscount' => $tutor->minDiscountMinsRecurring,
			'maxMinsForDiscount' => $tutor->maxDiscountMinsRecurring,
			'minPPHFlex' => $tutor->minPPHFlex,
			'maxPPHFlex' => $tutor->maxPPHFlex,
			'minHoursForDiscount' => $tutor->minDiscountHoursFlex,
			'maxHoursForDiscount' => $tutor->maxDiscountHoursFlex,
		];
	}

	public function actionAvailable( $id, $reset = null )
	{
		$post = Yii::$app->request->post();

		// if this is a save post (user clicked 'Update')
		if ( !empty( $post ) )
		{
			$trans = Yii::$app->db->beginTransaction();

			try
			{
				// we need to delete all current records for this tutor
				AvailableTime::deleteAll( 'tutorId = ' . $id );
				$error = false;

				// loop thru all the items in the post
				for ( $i = 0; $i < count( $post ); $i++ )
				{
					$at = new AvailableTime();

					$at->tutorId = $id;

					$start = new \DateTime( $post[ $i ][ 'start' ] );
//					$start->setTimezone( new \DateTimeZone( 'UTC' ) );
					$at->dayOfWeek = $this->convertDOWToNum( $start->format( 'l' ) );
					$at->startTime = $start->format( 'H:i' );

					$end = new \DateTime( $post[ $i ][ 'end' ] );
//					$end->setTimezone( new \DateTimeZone( 'UTC' ) );
					$at->endTime = $end->format( 'H:i' );

					// if save was not successful
					if ( !$at->save() )
					{
						$error = true;
						break;
					} // if ( !$at->save() )
				} // for ( $i = 0; $i < count( $post ); $i++ )

				if ( $error )
				{
					$trans->rollback();
				}
				else
				{
					$trans->commit();
					return $this->renderContent( 'success' );
				}
			}
			catch ( Exception $e )
			{
				$trans->rollback();
			}
			return 'error';
		}
		else if ( isset( $reset ) )
		{
			AvailableTime::deleteAll( 'tutorId = ' . $id );
			return $this->renderContent( 'success' );
//			$events = array();
//			return $this->render( 'available', [ 'events' => $events, 'tutorId' => $id ] );
		}
		else
		{
			$tutor = Tutor::findOne( $id );
			$times = AvailableTime::find()->where( [ 'tutorId' => $id ] )->all();
			$events = array();
			$i = 1;

			// loop thru all of the avaailable times
			foreach ( $times as $time )
			{
				$event = new Event();
				$event->id = $time->id;
				$event->title = 'Available';
				$dt = $this->getLocalDateTime( $time->startTime, $time->dayOfWeek, $tutor->user->timezone );
				$event->start = $dt->format( 'Y-m-d h:i:s' );
				$dt = $this->getLocalDateTime( $time->endTime, $time->dayOfWeek, $tutor->user->timezone );
				$event->end = $dt->format( 'Y-m-d h:i:s' );
				$events[] = $event;
			} // foreach ( $times as $time )

			return $this->render( 'available', [ 'events' => $events, 'tutorId' => $id ] );
		}
	}

	protected function getTutorScheduledEvents( $tutorId )
	{
		return Schedule::find()
				->where( [ 'tutorId' => $tutorId, 'status' => Schedule::STATUS_SCHEDULED ] )
				->all();
	} 

	protected function loadEvents( $schedules, $tutorId )
	{
		$utcTZ = new \DateTimeZone( 'UTC' );
		$tutor = $this->findModel( $tutorId );
		$tutorTZ = new \DateTimeZone( $tutor->user->timezone );
		$events = array();

		// loop through all the schedule records
		foreach ( $schedules as $schedule )
		{
			$event = new ScheduleEvent();
			$event->id = $schedule->id;
			$event->title = $schedule->customer->user->name;
			$start = new \DateTime( $schedule->startDttmUTC, $utcTZ );
			$start->setTimezone( $tutorTZ );
			$end = new \DateTime( $schedule->endDttmUTC, $utcTZ );
			$end->setTimezone( $tutorTZ );
			$event->start = $start->format( 'Y-m-d H:i' ); 
			$event->end = $end->format( 'Y-m-d H:i' );
			$event->subId = $schedule->subscriptionId;
			$event->alreadyScheduled = true;
			$events[] = $event;
		} // foreach ( $schedules as $schedule )

		return $events;
	}

	public function actionScheduledEvents( $start, $end )
	{
		// try to load the events
		try
		{
			$tutorId = Yii::$app->user->id;
			$schedules = $this->getTutorScheduledEvents( $tutorId );
			$events = array_merge( $events, $this->loadEvents( $schedules, $tutorId ) );

			header( 'Content-type: application/json' );
			echo Json::encode( $events );
			Yii::$app->end();
		} // try

		// catch all exceptions
		catch ( \Exception $e )
		{
			return $this->renderContent( 'error' );
		} // catch ( \Exception $e )
	}

	public function actionCalendar()
	{
		$tutor = Tutor::findOne( Yii::$app->user->id );

		return $this->render( 'calendar', [ 'tutor' => $tutor ] );
	}

	/**
	* Finds the Tutor model based on its primary key value.
	* If the model is not found, a 404 HTTP exception will be thrown.
	* @param integer $id
	* @return Tutor the loaded model
	* @throws NotFoundHttpException if the model cannot be found
	*/
	protected function findModel( $id )
	{
		if ( ( $model = Tutor::findOne( $id ) ) !== null )
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException( 'The requested page does not exist.' );
		}
	}
/*
	public function actionSomeAction($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string) $model->_id]);
        }elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                        'model' => $model
            ]);
        } else {
            return $this->render('_form', [
                        'model' => $model
            ]);
        }
    }
	 
	 The same action as above with just ajax and would never use direct access via url would be

public function actionSomeAction($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string) $model->_id]);
        }else (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                        'model' => $model
            ]);
        }
    }
	  */
}
