
function calcRecurringPrice( minsPerWeek, minPPM, maxPPM, minMinsForDiscount, maxMinsForDiscount )
{
	var minsPerMonth = minsPerWeek * 4.35;

	// if we don't reach the minimum value for discount, use max price
	if ( minsPerWeek < minMinsForDiscount )
	{
		return ( maxPPM * minsPerMonth );
	} // if ( minsPerWeek < minMinsForDiscount )

	// else if we exceed the maximum discount hours, use min price
	else if ( minsPerWeek > maxMinsForDiscount )
	{
		return ( minPPM * minsPerMonth );		
	} // else if ( minsPerWeek > maxMinsForDiscount )

	// else somewhere in the middle
	else
	{
		slope = ( maxPPM - minPPM ) / ( minMinsForDiscount - maxMinsForDiscount );

		// slope = (y2 - y1) / (x2 - x1)
		// let (x1, y1) = ( minMinsForDiscount, maxPPM )  and   x2 = minsPerWeek
		// so slope = (y2 - maxPPM) / ( minsPerWeek - minMinsForDiscount )
		// so slope * ( minsPerWeek - minMinsForDiscount ) = y2 - maxPPM
		// so y2 = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM
		ppm = slope * ( minsPerWeek - minMinsForDiscount ) + maxPPM;
		return ( ppm * minsPerMonth );
	} // else
}

function calcFlexPrice( hours, minPPH, maxPPH, minHoursForDiscount, maxHoursForDiscount )
{
	// if we don't reach the minimum value for discount, use max price
	if ( hours < minHoursForDiscount )
	{
		return ( maxPPH * hours );
	} // if ( hours < minHoursForDiscount )

	// else we exceed the maximum discount hours, use min price
	else if ( hours >= maxHoursForDiscount )
	{
		return ( minPPH * hours );
	} // else if ( hours >= maxHoursForDiscount )

	// else somewhere in the middle
	else
	{
		var slope = ( maxPPH - minPPH ) / ( minHoursForDiscount - maxHoursForDiscount );

		// slope = (y2 - y1) / (x2 - x1)
		// let (x1, y1) = ( minHoursForDiscount, maxPPH )  and   x2 = hours
		// so slope = (y2 - maxPPH) / ( hours - minHoursForDiscount )
		// so slope * ( hours - minHoursForDiscount ) = y2 - maxPPH
		// so y2 = slope * ( hours - minHoursForDiscount ) + maxPPH
		var pph = slope * ( hours - minHoursForDiscount ) + maxPPH;
		return ( pph * hours );
	} // else
}
