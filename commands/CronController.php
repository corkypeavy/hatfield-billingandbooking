<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Schedule;

class CronController extends Controller
{
	public function actionRunJobs()
	{
		$this->markEventsAsComplete();
		$this->sendInvoices();
		$this->sendLateNotices();
	}

	protected function sendInvoices()
	{
		$subscriptions = Subscription::GetExpired();

		foreach ( $subscriptions as $sub )
		{
			sendRenewalInvoice();
		}

		$subscripts = Subscription::GetInvoicesToSend();

		foreach ( $subscriptions as $sub )
		{
			sendMonthlyInvoice();
		}
	}

	protected function sendRenewalInvoice()
	{
		Yii::$app->mailer->compose( 'renewal-invoice', [ 'subscription' => $sub ] )
			->setTo( $sub->customer->user->email )
			->setFrom( Yii::$app->params[ 'adminEmail' ] )
			->setSubject( 'Spanish for Good Renewal Invoice' )
			->send();
	}

	protected function sendMonthlyInvoice()
	{
		Yii::$app->mailer->compose( 'monthly-invoice', [ 'subscription' => $sub ] )
			->setTo( $sub->customer->user->email )
			->setFrom( Yii::$app->params[ 'adminEmail' ] )
			->setSubject( 'Spanish for Good Invoice' )
			->send();
	}

	protected function sendLateNotices()
	{
		//TODO: Implement this
	}

	protected function sendPauseNotices()
	{
		//TODO: Need to update the logic here
		$subscriptions = Subscription::find()
								->where( [ 'status' => Subscription::STATUS_PAUSED, 'noticesSent' => 0 ] )
								->all();

		// loop through all the subscriptions
		foreach ( $subscriptions as $sub )
		{
			sendPauseNoticeEmail( $sub );
			$sub->noticesSent++;
			$sub->save();
		} // foreach ( $subscriptions as $sub )
	}

	protected function sendPauseNoticeEmail( $sub )
	{
		Yii::$app->mailer->compose( 'pause-notice', [ 'subscription' => $sub ] )
			->setTo( $sub->customer->user->email )
			->setFrom( Yii::$app->params[ 'adminEmail' ] )
			->setSubject( 'Your Spanish for Good Subsciption is Still Paused' )
			->send();
	}

	protected function markEventsAsComplete()
	{
/*		Yii::$app->db->createCommand( 'UPDATE schedule SET status = :newStatus WHERE status = :oldStatus AND endDttmUTC < now()' )
			->bindValue( ':newStatus', Schedule::STATUS_COMPLETED )
			->bindValue( ':oldStatus', Schedule::STATUS_SCHEDULED )
			->execute(); */
		Yii::$app->db->createCommand()->update( 'schedule', [ 'status' => Schedule::STATUS_COMPLETED ],
																'status = ' . Schedule::STATUS_SCHEDULED . ' AND endDttmUTC < now()' )
																->execute();
	}
}