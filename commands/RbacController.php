<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
//use app\rbac\MyCustomerAccountRule;
//use app\rbac\MyTutorAccountRule;
//use app\rbac\MyUserAccountRule;

class RbacController extends Controller
{
	public function actionCustom()
	{
		$auth = Yii::$app->authManager;
		$custRole = $auth->getRole( 'customer' );
		$auth->assign( $custRole, 25 );
	}

	public function actionReset()
	{
		Yii::$app->authManager->removeAll();
	}

	public function actionInit()
	{
		$trans = Yii::$app->db->beginTransaction();

		try
		{
			$auth = Yii::$app->authManager;

			/******************************************************************
			* Tutor Permissions                                               *
			******************************************************************/
			// add "createTutor" permission
			$createTutor = $auth->createPermission( 'createTutor' );
			$auth->add( $createTutor );

			// add "updateTutor" permission
			$updateTutor = $auth->createPermission( 'updateTutor' );
			$auth->add( $updateTutor );

			// add "viewTutor" permssion
			$viewTutor = $auth->createPermission( 'viewTutor' );
			$auth->add( $viewTutor );

			// add 'deleteTutor" permission
			$deleteTutor = $auth->createPermission( 'deleteTutor' );
			$auth->add( $deleteTutor );

			// add 'availableTimes' permission
			$availableTimes = $auth->createPermission( 'availableTimes' );
			$auth->add( $availableTimes );

			/******************************************************************
			* Customer Permissions                                            *
			******************************************************************/
			// add "createCustomer" permission
			$createCustomer = $auth->createPermission( 'createCustomer' );
			$auth->add( $createCustomer );

			// add "updateCustomer" permission
			$updateCustomer = $auth->createPermission( 'updateCustomer' );
			$auth->add( $updateCustomer );

			// add "viewCustomer" permssion
			$viewCustomer = $auth->createPermission( 'viewCustomer' );
			$auth->add( $viewCustomer );

			// add 'deleteCustomer" permission
			$deleteCustomer = $auth->createPermission( 'deleteCustomer' );
			$auth->add( $deleteCustomer );

			/******************************************************************
			* Admin Permissions                                               *
			******************************************************************/
			$createAdmin = $auth->createPermission( 'createAdmin' );
			$auth->add( $createAdmin );

			$updateAdmin = $auth->createPermission( 'updateAdmin' );
			$auth->add( $updateAdmin );

			$viewAdmin = $auth->createPermission( 'viewAdmin' );
			$auth->add( $viewAdmin );

			$deleteAdmin = $auth->createPermission( 'deleteAdmin' );
			$auth->add( $deleteAdmin );

			/******************************************************************
			* Referrer Permissions                                            *
			******************************************************************/
			$createReferrer = $auth->createPermission( 'createReferrer' );
			$auth->add( $createReferrer );

			$updateReferrer = $auth->createPermission( 'updateReferrer' );
			$auth->add( $updateReferrer );

			$viewReferrer = $auth->createPermission( 'viewReferrer' );
			$auth->add( $viewReferrer );

			$deleteReferrer = $auth->createPermission( 'deleteReferrer' );
			$auth->add( $deleteReferrer );

			/******************************************************************
			* Settings Permissions                                            *
			******************************************************************/
			$createSetting = $auth->createPermission( 'createSetting' );
			$auth->add( $createSetting );

			$updateSetting = $auth->createPermission( 'updateSetting' );
			$auth->add( $updateSetting );

			$viewSetting = $auth->createPermission( 'viewSetting' );
			$auth->add( $viewSetting );

			$deleteSetting = $auth->createPermission( 'deleteSetting' );
			$auth->add( $deleteSetting );

			/******************************************************************
			* Purchase Permissions                                            *
			******************************************************************/
			$createPurchasee = $auth->createPermission( 'createPurchase' );
			$auth->add( $createPurchase );

			$updatePurchase = $auth->createPermission( 'updatePurchase' );
			$auth->add( $updatePurchase );

			$viewPurchase = $auth->createPermission( 'viewPurchase' );
			$auth->add( $viewPurchase );

			$deletePurchase = $auth->createPermission( 'deletePurchase' );
			$auth->add( $deletePurchase );
			
			// create roles
			$customer = $auth->createRole( 'customer' );
			$auth->add( $customer );

			$tutor = $auth->createRole( 'tutor' );
			$auth->add( $tutor );

			$admin = $auth->createRole( 'admin' );
			$auth->add( $admin );

			$superAdmin = $auth->createRole( 'superAdmin' );
			$auth->add( $superAdmin );

			// add permissions to customer

			// add rule to view/update own account
			$myCustAcctRule = new \app\rbac\MyCustomerAccountRule;
			$auth->add( $myCustAcctRule );

			// add permission to view own acct
			$viewOwnCustAcct = $auth->createPermission( 'viewOwnCustAcct' );
			$viewOwnCustAcct->ruleName = $myCustAcctRule->name;
			$auth->add( $viewOwnCustAcct );
			$auth->addChild( $viewOwnCustAcct, $viewCustomer );
			$auth->addChild( $customer, $viewOwnCustAcct );

			// add permission to update own acct
			$updateOwnCustAcct = $auth->createPermission( 'updateOwnCustAcct' );
			$updateOwnCustAcct->ruleName = $myCustAcctRule->name;
			$auth->add( $updateOwnCustAcct );
			$auth->addChild( $updateOwnCustAcct, $updateCustomer );
			$auth->addChild( $customer, $updateOwnCustAcct );

			// add permissions to tutor
			$auth->addChild( $tutor, $createCustomer );
			$auth->addChild( $tutor, $viewCustomer );
			$auth->addChild( $tutor, $updateCustomer );
			$auth->addChild( $tutor, $availableTimes );

			// add rule to view/update own account/purchase
			$myTutorAcctRule = new \app\rbac\MyTutorAccountRule;
			$auth->add( $myTutorAcctRule );

			// add permission to view own purchases
			$viewOwnTutorPurchase = $auth->createPermission( 'viewOwnTutorPurchase' );
			$viewOwnTutorPurchase->ruleName = $myTutorAcctRule->name;
			$auth->add( $viewOwnTutorPurchase );
			$auth->addChild( $viewOwnTutorPurchase, $viewPurchase );
			$auth->addChild( $tutor, $viewOwnTutorPurchase );			

			// add rule to update own purchases
			$updateOwnTutorPurchase = $auth->createPermission( 'updateOwnTutorPurchase' );
			$updateOwnTutorPurchase->ruleName = $myTutorAcctRule->name;
			$auth->add( $updateOwnTutorPurchase );
			$auth->addChild( $updateOwnTutorPurchase, $updatePurchase );
			$auth->addChild( $tutor, $updateOwnTutorPurchase );

			// add permission to view own acct
			$viewOwnTutorAcct = $auth->createPermission( 'viewOwnTutorAcct' );
			$viewOwnTutorAcct->ruleName = $myTutorAcctRule->name;
			$auth->add( $viewOwnTutorAcct );
			$auth->addChild( $viewOwnTutorAcct, $viewTutor );
			$auth->addChild( $tutor, $viewOwnTutorAcct );

			// add rule to update own account
			$updateOwnTutorAcct = $auth->createPermission( 'updateOwnTutorAcct' );
			$updateOwnTutorAcct->ruleName = $myTutorAcctRule->name;
			$auth->add( $updateOwnTutorAcct );
			$auth->addChild( $updateOwnTutorAcct, $updateTutor );
			$auth->addChild( $tutor, $updateOwnTutorAcct );

			// add roles and permissions to admin
			$auth->addChild( $admin, $viewTutor );
			$auth->addChild( $admin, $updateTutor );
			$auth->addChild( $admin, $tutor );
			$auth->addChild( $admin, $customer );
			$auth->addChild( $admin, $deleteCustomer );
			$auth->addChild( $admin, $deleteTutor );
			$auth->addChild( $admin, $viewPurchase );
			$auth->addChild( $admin, $updatePurchase );

			// add rule to update/view own account
			$myUserAcctRule = new \app\rbac\MyUserAccountRule;
			$auth->add( $myUserAcctRule );

			// add permission to view own acct
			$viewOwnUserAcct = $auth->createPermission( 'viewOwnUserAcct' );
			$viewOwnUserAcct->ruleName = $myUserAcctRule->name;
			$auth->add( $viewOwnUserAcct );
			$auth->addChild( $viewOwnUserAcct, $viewAdmin );
			$auth->addChild( $admin, $viewOwnUserAcct );

			// add permission to update own acct
			$updateOwnUserAcct = $auth->createPermission( 'updateOwnUserAcct' );
			$updateOwnUserAcct->ruleName = $myUserAcctRule->name;
			$auth->add( $updateOwnUserAcct );
			$auth->addChild( $updateOwnUserAcct, $updateAdmin );
			$auth->addChild( $admin, $updateOwnUserAcct );

			// add roles and permissions to superAdmin
			$auth->addChild( $superAdmin, $admin );
			$auth->addChild( $superAdmin, $createAdmin );
			$auth->addChild( $superAdmin, $deleteAdmin );
			$auth->addChild( $superAdmin, $viewAdmin );
			$auth->addChild( $superAdmin, $updateAdmin );

			// assign 
			$auth->assign( $superAdmin, 1 );
			$auth->assign( $superAdmin, 2 );
			$auth->assign( $superAdmin, 22 );
			$auth->assign( $superAdmin, 24 );
			$auth->assign( $admin, 23 );
			$auth->assign( $tutor, 9 );
			$auth->assign( $tutor, 10 );
			$auth->assign( $tutor, 21 );
			$auth->assign( $customer, 20 );
			$trans->commit();
		}
		catch ( Exception $e )
		{
			$trans->rollback();
		}
	}
}