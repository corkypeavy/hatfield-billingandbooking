<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2>Welcome <?= $user->name ?></h2>
<p>In order to verify your email address, please click the 'Verify' link below to verify that this is your correct email address. ?></p>

<?= Html::a( 'Verify', Url::to( [ 'customer/verify-email', 'email' => $user->email ], true ) ) ?>