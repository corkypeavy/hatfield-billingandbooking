<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2>Invoice #<?= $purchase->id ?></h2>
<p>Name: <?= $purchase->customer->user->name ?></p>
<p>Tutor: <?= $purchase->tutor->user->name ?></p>
<p>Type: <?= $purchase->typeString() ?></p>
<p>Amount: <?= $purchase->totalPrice ?></p>

<?= Html::a( 'Click here to pay', Url::to( [ 'customer/pay', 'purchaseId' => $purchase->id ], true ) ) ?>