<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Schedule;
use app\models\helpers\TimeType;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

$schedule = Schedule::find()->where( [ 'customerId' => $cust->id, 'tutorId' => $tutorId ] )->one();

if ( $type == 'customer' )
{
	$startDttm = $schedule->startDttmUTC->setTimezone( new \DateTimeZone( $cust->user->timezone ) );
}
else
{
	$startDttm = $schedule->startDttmUTC->setTimezone( new \DateTimeZone( $tutor->user->timezone ) );
}
?>

<?php if ( $type == 'customer' ) : ?>
	<h2>Welcome, <?= $cust->user->name ?></h2>
	<p>You have just scheduled your first class with <span style="font-size: 150%"><?= $tutor->user->name ?></span></p>
	<p>Your class will be at <span style="font-size: 150%"><?= $startDttm->format( 'h:i' ) ?> on <?= $startDttm->format( 'Y-m-d' ) ?></span>.</p>
	<p>Your free class last for 30 minutes.</p>
	<br/>
	<p>Your Spanish For Good Team
<?php else : ?>
	<p>A new student has just scheduled his first class with <?= $type == 'tutor' ? 'you' : '<span style="font-size: 150%">' . $tutor->user->name . '</span>' ?>. </p>
	<p>The class will be at <span style="font-size: 150%"><?= $startDttm->format( 'h:i' ) ?> on <?= $startDttm->format( 'Y-m-d' ) ?><?= $type == 'admin' ? '(tutor\'s time)' : '' ?></span>.</p>
	<p>It last for 30 minutes.</p>
<?php endif; ?>