<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Schedule;
use app\models\helpers\TimeType;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

if ( $type == 'customer' )
{
	$startDttm = $schedule->startDttmUTC->setTimezone( new \DateTimeZone( $schedule->customer->user->timezone ) );
}
else
{
	$startDttm = $schedule->startDttmUTC->setTimezone( new \DateTimeZone( $schedule->tutor->user->timezone ) );
}
?>

<?php if ( $type == 'customer' ) : ?>
	<h2>Dear <?= $cust->user->name ?></h2>
	<p>We are notifying you that your class scheduled at</p>
	<span style="font-size: 150%"><?= $startDttm->format( 'h:i' ) ?> on <?= $startDttm->format( 'Y-m-d' ) ?></span>
	<p>has been cancelled.</p>
	<br/>
	<p>Your Spanish For Good Team
<?php else : ?>
	<p>Your student, <?= $schedule->customer->user->name ?>, has just cancelled his/her class with <?= $type == 'tutor' ? 'you' : '<span style="font-size: 150%">' . $tutor->user->name . '</span>' ?>. </p>
	<p>The class was scheduled at <span style="font-size: 150%"><?= $schedule->startDttmUTC->format( 'h:i' ) ?> on <?= $schedule->startDttmUTC->format( 'Y-m-d' ) ?><?= $type == 'admin' ? '(tutor\'s time)' : '' ?></span>.</p>
<?php endif; ?>