<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2>Subscription #<?= $subscription->id ?></h2>
<p>Name: <?= $sub->customer->user->name ?></p>
<p>Tutor: <?= $sub->tutor->user->name ?></p>

The above subscription is still paused, but will expire shortly.  Please click the following link to resume your subscription.
<br/>
<?= Html::a( 'Resume Subscription', Url::to( [ 'customer/subscriptions' ], true ) ) ?>