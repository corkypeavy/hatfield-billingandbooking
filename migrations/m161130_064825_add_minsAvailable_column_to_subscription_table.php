<?php

use yii\db\Migration;

/**
 * Handles adding minsAvailable to table `subscription`.
 */
class m161130_064825_add_minsAvailable_column_to_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->addColumn( 'subscription', 'minsAvailable', $this->integer() );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->dropColumn( 'subscription', 'minsAvailable' );
    }
}
