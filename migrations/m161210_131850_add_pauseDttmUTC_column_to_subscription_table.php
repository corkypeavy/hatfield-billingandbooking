<?php

use yii\db\Migration;

/**
 * Handles adding pauseDttmUTC to table `subscription`.
 */
class m161210_131850_add_pauseDttmUTC_column_to_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn( 'subscription', 'pauseDttmUTC', $this->datetime() );
		  $this->addColumn( 'subscription', 'noticesSent', $this->integer(), 0 );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn( 'subscription', 'pauseDttmUTC' );
		  $this->dropColumn( 'subscription', 'noticesSent' );
    }
}
