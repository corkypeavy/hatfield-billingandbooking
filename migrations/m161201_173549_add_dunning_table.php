<?php

use yii\db\Migration;

class m161201_173549_add_dunning_table extends Migration
{
	public function up()
	{
		$this->createTable( 'dunning', [
			'step' => $this->integer()->unique()->notNull(),
			'message' => $this->text()->notNull(),
			'days' => $this->integer()->notNull()
		] );
	}

	public function down()
	{
		$this->dropTable( 'dunning' );
	}
}
