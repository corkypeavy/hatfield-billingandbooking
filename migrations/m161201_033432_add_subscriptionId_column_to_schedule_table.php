<?php

use yii\db\Migration;

/**
 * Handles adding subscriptionId to table `schedule`.
 */
class m161201_033432_add_subscriptionId_column_to_schedule_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->addColumn( 'schedule', 'subscriptionId', $this->Integer() );
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropColumn( 'schedule', 'subscriptionId' );
	}
}
